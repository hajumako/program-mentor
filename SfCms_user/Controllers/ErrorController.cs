﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//
using SfCms_model;
using System.IO;
using System.Web.UI;

namespace SfCms_user.Controllers
{
    public class ErrorController : Controller
    {
		public ActionResult HttpError403(string error)
		{
            ViewBag.Description = error;
            return this.View();
		}

		public ActionResult HttpError404(string error)
		{
            ViewBag.Description = error;
            return this.View();
		}

		public ActionResult HttpError500(string error)
		{
            ViewBag.Description = error;
            return this.View();
		}
    }
}
