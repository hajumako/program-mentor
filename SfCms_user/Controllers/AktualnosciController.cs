﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//
using SfCms_model;

namespace SfCms_user.Controllers
{
    public class AktualnosciController : Controller
    {
        //pattern
        private class_pattern pattern = new class_pattern();

        //Index
        public ActionResult Index()
        {
            //menu
            class_menu menu = new class_menu();
            menu.generate_list();

            //news
            class_news news = new class_news();
            news.generate_list(class_news.search_type.active, string.Empty);

            //news_file
            class_news_file news_file = new class_news_file();

            //pattern - begin
            this.pattern.menu = menu;
            this.pattern.news = news;
            this.pattern.news_file = news_file;
            //patern - end

            return View(this.pattern);
        }
    }
}
