﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//
using SfCms_model;
using Facebook;

namespace SfCms_user.Controllers
{
    public class AccountUserController : Controller
    {
		//pattern
		private class_pattern pattern = new class_pattern();
		//nav
		private class_nav nav = new class_nav();
		//validation
		private class_validation validation = new class_validation();
        //settings
        private class_settings settings = new class_settings();
        //tool
        private class_tool tool = new class_tool();
		//sql
		private class_sql sql = new class_sql();

		//menu_user
		[AllowAnonymous]
		public ActionResult menu_user()
		{
            //menu
            class_menu menu = new class_menu();
            menu.generate_list();

			//nav
			this.nav.post = false;

			//user
			class_user user = new class_user();

			//session
			if (session.user_id != null)
			{
				user.id = int.Parse(session.user_id);
				user.get_data();
			}

			//pattern - begin
            this.pattern.menu = menu;
			this.pattern.nav = this.nav;
			this.pattern.user = user;
			//patern - end

			return PartialView("_partial_menu_user", this.pattern);
		}

		//menu_user
		[HttpPost]
		[AllowAnonymous]
		public ActionResult menu_user(FormCollection form)
		{
            //menu
            class_menu menu = new class_menu();
            menu.generate_list();

			//nav
			this.nav.post = true;

			//user
			class_user user = new class_user();

			//session
			if (session.user_id != null)
			{
				user.id = int.Parse(session.user_id);
				user.get_data();
			}

			//pattern - begin
            this.pattern.menu = menu;
			this.pattern.nav = this.nav;
			this.pattern.user = user;
			//patern - end

			return PartialView("_partial_menu_user", this.pattern);
		}

		//form_login
		[AllowAnonymous]
		public ActionResult form_login()
		{
			//nav
			this.nav.post = false;

			//user
			class_user user = new class_user();

            //id
            string id = string.Empty;

            //project
            class_project project = new class_project();

            if (this.validation.czy_string_not_empty(RouteData.Values["id"]))
            {
                id = RouteData.Values["id"].ToString();

                project.generate_list(class_project.search_type.name, id);

                if (project.list.Count == 1)
                {
                    //project
                    project = project.list[0];
                }
            }
            else
            {

            }

			//pattern - begin
			this.pattern.nav = this.nav;
			this.pattern.user = user;
            this.pattern.project = project;
			//patern - end

			return PartialView("_partial_form_login", this.pattern);
		}

        //form_login_brick
        [AllowAnonymous]
        public ActionResult form_login_brick()
        {
            //nav
            this.nav.post = false;

            //user
            class_user user = new class_user();

            //id
            string id = string.Empty;

            //project
            class_project project = new class_project();

            if (this.validation.czy_string_not_empty(RouteData.Values["id"]))
            {
                id = RouteData.Values["id"].ToString();

                project.generate_list(class_project.search_type.name, id);

                if (project.list.Count == 1)
                {
                    //project
                    project = project.list[0];
                }
            }
            else
            {

            }

            //pattern - begin
            this.pattern.nav = this.nav;
            this.pattern.user = user;
            this.pattern.project = project;
            //patern - end

            return PartialView("_partial_form_login_brick", this.pattern);
        }

		//form_login_info
		[AllowAnonymous]
		public ActionResult form_login_info()
		{
			//nav
			this.nav.post = false;

			//user
			class_user user = new class_user();

			//pattern - begin
			this.pattern.nav = this.nav;
			this.pattern.user = user;
			//patern - end

			return PartialView("_partial_form_login_info", this.pattern);
		}

		//LogIn
		[HttpPost]
		[AllowAnonymous]
		public ActionResult LogIn(FormCollection form)
		{
            string form_project_id = form["form_login_project_id"];
			string form_email = form["form_login_email"];
			string form_password = form["form_login_password"];

            //user
            class_user user = new class_user();
			user.email = form_email;
			user.password = form_password;

            //validation
            if (this.validation.czy_email(user.email) && this.validation.czy_string_not_empty(user.password))
            {
			    //md5
			    class_md5 md5 = new class_md5(user.password, class_md5.encoding.UTF8);

			    if (user.log_in(user.email, md5.password_md5hash) == true)
			    {
				    //session
				    session.user_id = user.id.ToString();

				    //nav
					this.nav.success = true;
				    this.nav.post = true;
				    this.nav.validation_login = true;
				    this.nav.modal_login = false;
			    }
			    else
			    {
				    //nav
					this.nav.success = false;
				    this.nav.post = true;
				    this.nav.validation_login = true;
				    this.nav.modal_login = true;
			    }
            }
            else
            {
                //nav
				this.nav.success = false;
                this.nav.post = true;
                this.nav.validation_login = false;
                this.nav.modal_login = true;
            }

            //project
            int id = int.Parse(form_project_id);

            class_project project = new class_project(id);

			//pattern - begin
			this.pattern.nav = this.nav;
			this.pattern.user = user;
            this.pattern.project = project;
			this.pattern.validation = validation;
			//patern - end

			return PartialView("_partial_form_login", this.pattern);
		}

        //LogInBrick
        [HttpPost]
        [AllowAnonymous]
        public ActionResult LogInBrick(FormCollection form)
        {
            string form_project_id = form["form_login_project_id"];
            string form_email = form["form_login_brick_email"];
            string form_password = form["form_login_brick_password"];

            //user
            class_user user = new class_user();
            user.email = form_email;
            user.password = form_password;

            //validation
            if (this.validation.czy_email(user.email) && this.validation.czy_string_not_empty(user.password))
            {
                //md5
                class_md5 md5 = new class_md5(user.password, class_md5.encoding.UTF8);

                if (user.log_in(user.email, md5.password_md5hash) == true)
                {
                    //session
                    session.user_id = user.id.ToString();

                    //nav
                    this.nav.success = true;
                    this.nav.post = true;
                    this.nav.validation_login_brick = true;
                    this.nav.modal_login_brick = false;
                }
                else
                {
                    //nav
                    this.nav.success = false;
                    this.nav.post = true;
                    this.nav.validation_login_brick = true;
                    this.nav.modal_login_brick = true;
                }
            }
            else
            {
                //nav
                this.nav.success = false;
                this.nav.post = true;
                this.nav.validation_login_brick = false;
                this.nav.modal_login_brick = true;
            }

            //project
            int id = int.Parse(form_project_id);

            class_project project = new class_project(id);

            //pattern - begin
            this.pattern.nav = this.nav;
            this.pattern.user = user;
            this.pattern.project = project;
            this.pattern.validation = validation;
            //patern - end

            return PartialView("_partial_form_login_brick", this.pattern);
        }

		//LogOff
		[HttpPost]
		[AllowAnonymous]
		public ActionResult LogOff()
		{
            //menu
            class_menu menu = new class_menu();
            menu.generate_list();

			//session
			session.user_id = null;

			//nav
			this.nav.post = true;

			//user
			class_user user = new class_user();

			//pattern - begin
            this.pattern.menu = menu;
			this.pattern.nav = this.nav;
			this.pattern.user = user;
			//patern - end

			return PartialView("_partial_menu_user", this.pattern);
		}

		//form_register
		[AllowAnonymous]
		public ActionResult form_register()
		{
			//nav
			this.nav.post = false;

			//user
			class_user user = new class_user();

			//pattern - begin
			this.pattern.nav = this.nav;
			this.pattern.user = user;
			//patern - end

			return PartialView("_partial_form_register", this.pattern);
		}

		//form_register_info
		[AllowAnonymous]
		public ActionResult form_register_info()
		{
			//nav
			this.nav.post = false;

			//user
			class_user user = new class_user();

			//pattern - begin
			this.pattern.nav = this.nav;
			this.pattern.user = user;
			//patern - end

			return PartialView("_partial_form_register_info", this.pattern);
		}

		//RegisterIn
		[HttpPost]
		[AllowAnonymous]
		public ActionResult RegisterIn(FormCollection form)
		{
			string form_name = form["form_register_name"];
			string form_surname = form["form_register_surname"];
			string form_email = form["form_register_email"];
			string form_password = form["form_register_password"];
			string form_password_repeat = form["form_register_password_repeat"];
			bool form_regulations = true;
            //if (form["form_register_regulations"] == "true")
            //{
            //    form_regulations = true;
            //}
			bool form_policy = true;
            //if (form["form_register_policy"] == "true")
            //{
            //    form_policy = true;
            //}

			//user
			class_user user = new class_user();
			user.name = form_name;
			user.surname = form_surname;
			user.email = form_email;
			user.password_no_md5 = form_password;
			user.password_repeat = form_password_repeat;
			user.regulations = form_regulations;
			user.policy = form_policy;

			//validation
			if (
				this.validation.czy_string_not_empty(user.name)
				&& this.validation.czy_string_not_empty(user.surname)
				&& this.validation.czy_email(user.email)
				&& !user.exists(class_user.search_type.email,user.email)
				&& this.validation.czy_string_not_empty(user.password_no_md5)
				&& this.validation.czy_string_not_empty(user.password_repeat)
				&& (user.password_no_md5 == user.password_repeat)
				&& user.regulations
				&& user.policy
				)
			{
				//md5
				class_md5 md5 = new class_md5(user.password_no_md5, class_md5.encoding.UTF8);

				//user.insert()
				user.active = false;
				user.login = user.email;
				user.password = md5.password_md5hash;
				//user.name
				//user.surname
				//user.email
				user.date_create = DateTime.Now;
				user.date_password_change = this.sql.data_default;
				user.date_last_logged = this.sql.data_default;
				user.uid_password_reset = string.Empty;
				user.uid_register = user.generate_uid();
				user.facebook_id = string.Empty;
				user.insert(class_user.insert_type.user);

				if (user.id != 0)
				{
                    //link_register
                    user.link_register = settings.host_user + "/AccountUser/Register/" + user.uid_register;

                    //email
                    class_email email = new class_email();

                    string msg_template = System.IO.File.ReadAllText(Server.MapPath("/assets/email_templates/register_email_template.html"));

                    if (email.send(user.email, user.subject_register, user.body_register(msg_template)))
                    {
                        //nav
                        this.nav.success = true;
                        this.nav.validation_register = true;
                        this.nav.modal_register = false;
						this.nav.info = string.Empty;
                    }
                    else
                    {
                        user.delete();

                        //nav
                        this.nav.success = false;
                        this.nav.validation_register = true;
                        this.nav.modal_register = true;
                        this.nav.info = "Nie można wysłać maila, sprawdź poprawność wpisanego adresu";
                    }
				}
				else
				{
					if (user.insert_email_exists)
					{
						//nav
						this.nav.success = false;
						this.nav.validation_register = true;
						this.nav.modal_register = true;
						this.nav.info = "Konto już istnieje";
					}
					else if (user.insert_uid_register_exists)
					{
						//nav
						this.nav.success = false;
						this.nav.validation_register = true;
						this.nav.modal_register = true;
						this.nav.info = "Błąd uid";
					}
					else
					{
						//nav
						this.nav.success = false;
						this.nav.validation_register = true;
						this.nav.modal_register = true;
						this.nav.info = "Błąd";
					}
				}
			}
			else
			{
				//nav
				this.nav.success = false;
				this.nav.validation_register = false;
				this.nav.modal_register = true;
				this.nav.info = string.Empty;
			}

			//nav
			this.nav.post = true;

			//pattern - begin
			this.pattern.nav = this.nav;
			this.pattern.user = user;
			this.pattern.validation = this.validation;
			//patern - end

			return PartialView("_partial_form_register", this.pattern);
		}

        //Register
		[AllowAnonymous]
		public ActionResult Register()
		{
            //id
            if (!this.validation.czy_string_not_empty(RouteData.Values["id"]))
            {
                return RedirectToAction("HttpError404", "Error");
            }
            else
            {
                string uid_register = RouteData.Values["id"].ToString();

                //user
                class_user user = new class_user();

                if (user.exists(class_user.search_type.uid_register, uid_register))
                {
                    user.get_data();

                    if (!user.active)
                    {
                        user.active = true;
                        user.update();

                        //nav
                        this.nav.register = true;
                    }
                    else
                    {
                        //nav
                        this.nav.register = false;
                    }
                }
                else
                {
                    //nav
                    this.nav.register = false;
                }

                //menu
                class_menu menu = new class_menu();
                menu.generate_list();

                //pattern - begin
                this.pattern.menu = menu;
                this.pattern.nav = this.nav;
                this.pattern.user = user;
                //patern - end

                return View(this.pattern);
            }
		}

		//form_password_reset
		[AllowAnonymous]
		public ActionResult form_password_reset()
		{
			//nav
			this.nav.post = false;

			//user
			class_user user = new class_user();

			//pattern - begin
			this.pattern.nav = this.nav;
			this.pattern.user = user;
			//patern - end

			return PartialView("_partial_form_password_reset", this.pattern);
		}

		//form_password_reset_info
		[AllowAnonymous]
		public ActionResult form_password_reset_info()
		{
			//nav
			this.nav.post = false;

			//user
			class_user user = new class_user();

			//pattern - begin
			this.pattern.nav = this.nav;
			this.pattern.user = user;
			//patern - end

			return PartialView("_partial_form_password_reset_info", this.pattern);
		}

		//PasswordResetIn
		[HttpPost]
		[AllowAnonymous]
		public ActionResult PasswordResetIn(FormCollection form)
		{
			string form_email = form["form_password_reset_email"];

			//id
			int id = 0;

			//user
			class_user user = new class_user();
			user.email = form_email;
			
			//validation
			if (this.validation.czy_email(user.email))
			{
                if (user.exists(class_user.search_type.email, user.email))
                {
                    user.get_data();
				
					//id
					id = user.id;

                    if (user.active)
                    {
						user.uid_password_reset = user.generate_uid();

						if(!user.exists(class_user.search_type.uid_password_reset,user.uid_password_reset))
						{
							user.id = id;
							user.update();

							//link_password_reset
							user.link_password_reset = settings.host_user + "/AccountUser/PasswordReset/" + user.uid_password_reset;

							//email
							class_email email = new class_email();

                            string msg_template = System.IO.File.ReadAllText(Server.MapPath("/assets/email_templates/password_reset_email_template.html"));

							if (email.send(user.email, user.subject_password_reset, user.body_password_reset(msg_template)))
							{
								//nav
								this.nav.success = true;
								this.nav.validation_password_reset = true;
								this.nav.modal_password_reset = false;
								this.nav.info = string.Empty;
							}
							else
							{
								user.uid_password_reset = string.Empty;
								user.update();

								//nav
								this.nav.success = false;
								this.nav.validation_password_reset = true;
								this.nav.modal_password_reset = true;
								this.nav.info = "Nie można wysłać maila, sprawdź poprawność wpisanego adresu";
							}
						}
						else
						{
							//nav
							this.nav.success = false;
							this.nav.validation_password_reset = true;
							this.nav.modal_password_reset = true;
							this.nav.info = "Błąd uid";
						}
                    }
                    else
                    {
                        //nav
                        this.nav.success = false;
                        this.nav.validation_password_reset = true;
                        this.nav.modal_password_reset = true;
						this.nav.info = "Konto nieaktywne";
                    }
                }
                else
                {
                    //nav
                    this.nav.success = false;
                    this.nav.validation_password_reset = true;
                    this.nav.modal_password_reset = true;
					this.nav.info = "Użytkownik o podanym adresie email nie istnieje";
                }
				
			}
			else
			{
				//nav
				this.nav.success = false;
				this.nav.validation_password_reset = false;
				this.nav.modal_password_reset = true;
				this.nav.info = string.Empty;
			}

			//nav
			this.nav.post = true;

			//pattern - begin
			this.pattern.nav = this.nav;
			this.pattern.user = user;
			this.pattern.validation = validation;
			//patern - end

			return PartialView("_partial_form_password_reset", this.pattern);
		}

		//PasswordReset
		[AllowAnonymous]
		public ActionResult PasswordReset()
		{
			//id
			if (!this.validation.czy_string_not_empty(RouteData.Values["id"]) && RouteData.Values["id"].ToString().Length != 32)
			{
				return RedirectToAction("HttpError404", "Error");
			}
			else
			{
				string uid_password_reset = RouteData.Values["id"].ToString();

				//user
				class_user user = new class_user();

				if (user.exists(class_user.search_type.uid_password_reset, uid_password_reset))
				{
					user.get_data();

					if (!user.active)
					{
						//nav
						this.nav.password_reset = false;
					}
					else
					{
						//nav
						this.nav.password_reset = true;
					}
				}
				else
				{
					//nav
					this.nav.password_reset = false;
				}

				//menu
				class_menu menu = new class_menu();
				menu.generate_list();

				//nav
				this.nav.post = false;

				//pattern - begin
				this.pattern.menu = menu;
				this.pattern.nav = this.nav;
				this.pattern.user = user;
				//patern - end

				return View(this.pattern);
			}
		}

		//PasswordReset
		[HttpPost]
		[AllowAnonymous]
		public ActionResult PasswordReset(FormCollection form)
		{
			int form_id = int.Parse(form["id"]);
			string form_uid_password_reset = form["uid_password_reset"];
			string form_password = form["password"];
			string form_password_repeat = form["password_repeat"];

			//user
			class_user user = new class_user(form_id);
			user.password_no_md5 = form_password;
			user.password_repeat = form_password_repeat;
			user.uid_password_reset = form_uid_password_reset;

			if (user.id != 0 && user.active && user.uid_password_reset != string.Empty && user.uid_password_reset == form_uid_password_reset)
			{
				if (this.validation.czy_string_not_empty(user.password_no_md5) && (user.password_no_md5 == user.password_repeat))
				{
					//md5
					class_md5 md5 = new class_md5(user.password_no_md5, class_md5.encoding.UTF8);

					//user.update()
					user.password = md5.password_md5hash;
					user.date_password_change = DateTime.Now;
					user.uid_password_reset = string.Empty;
					user.update();

					//nav
					this.nav.password_reset = true;
					this.nav.success = true;
					this.nav.validation_password_reset = true;
					this.nav.info = "Hasło zostało zmienione. Od teraz możesz się logować za pomoca nowego hasła.";
				}
				else
				{
					//nav
					this.nav.password_reset = true;
					this.nav.success = false;
					this.nav.validation_password_reset = false;
					this.nav.info = "Nie podałeś hasła lub powtórzone hasło nie zgadza się z pierwszym.<br>Spróbuj ponownie.";
				}
			}
			else
			{
				//nav
				this.nav.password_reset = false;
				this.nav.success = false;
				this.nav.validation_password_reset = false;
                this.nav.info = "Błąd przy zmianie hasła lub nieaktualny link.<br>Wygeneruj nowy link na stronie głównej.";
			}

			//menu
			class_menu menu = new class_menu();
			menu.generate_list();

			//nav
			this.nav.post = true;

			//pattern - begin
			this.pattern.menu = menu;
			this.pattern.nav = this.nav;
			this.pattern.user = user;
			//patern - end

			return View(this.pattern);
		}

        //form_brick
        [AllowAnonymous]
        public ActionResult form_brick()
        {
            //project
            class_project project = new class_project();

            //user
            class_user user = new class_user();

            //s_brick
            class_s_brick s_brick = new class_s_brick();

            //paylane
            class_paylane paylane = new class_paylane();

            //pattern - begin
            this.pattern.project = project;
            this.pattern.user = user;
            this.pattern.s_brick = s_brick;
            this.pattern.paylane = paylane;
            this.pattern.tool = this.tool;
            //patern - end

            return PartialView("_partial_form_brick", this.pattern);
        }

        //form_brick
        [HttpPost]
        [AllowAnonymous]
        public ActionResult form_brick(FormCollection form)
        {
            int form_project_id = int.Parse(form["form_brick_project_id"]);
            int form_s_brick_id = int.Parse(form["form_brick_s_brick_id"]);      

            //setting
            class_settings settings = new class_settings();

            //project
            class_project project = new class_project(form_project_id);

			//beneficiary
			class_beneficiary beneficiary = new class_beneficiary();
			beneficiary.generate_list(class_beneficiary.search_type.project_id, project.id.ToString());

            //user
            class_user user = new class_user();

            //session
            if (session.user_id != null)
            {
                user.id = int.Parse(session.user_id);
                user.get_data();
            }

            //s_brick
            class_s_brick s_brick = new class_s_brick(form_s_brick_id);

            //paylane
			string paylane_merchant_id = string.Empty;
			string paylane_salt = string.Empty;
			if (beneficiary.list.Count() > 0)
			{
				paylane_merchant_id = beneficiary.list[0].paylane_merchant_id;
				paylane_salt = beneficiary.list[0].paylane_salt;
			}

            class_paylane paylane = new class_paylane();
            paylane.project_id = project.id;
            paylane.user_id = user.id;
            paylane.date_request = DateTime.Now;

            paylane.request_merchant_id = paylane_merchant_id;
            //paylane.request_description = "TR001";
            paylane.request_transaction_description = "Program Mentor - " + project.name;
            paylane.request_amount = s_brick.amount_from;
            paylane.request_currency = "PLN";
            paylane.request_transaction_type = "S";
            paylane.request_back_url = settings.paylane_back_url;
            //paylane.request_hash = paylane.sha1(settings.paylane_salt, paylane.request_description, paylane.request_amount, paylane.request_currency, paylane.request_transaction_type);
            paylane.request_language = "pl";
            paylane.request_customer_name = user.surname + " " + user.name;
            paylane.request_customer_email = user.email;
            paylane.request_customer_country = "PL";

            //insert
            paylane.insert();

            //request_description
            if (paylane.id < 10)
            {
                paylane.request_description = "0" + paylane.id.ToString();
            }
            else
            {
                paylane.request_description = paylane.id.ToString();
            }
            //request_hash
            paylane.request_hash = paylane.sha1(paylane_salt, paylane.request_description, paylane.request_amount, paylane.request_currency, paylane.request_transaction_type);

            //insert
            paylane.update();

            //pattern - begin
            this.pattern.project = project;
            this.pattern.user = user;
            this.pattern.s_brick = s_brick;
            this.pattern.paylane = paylane;
            this.pattern.tool = this.tool;
            //patern - end

            return PartialView("_partial_form_brick", this.pattern);
        }

        //[HttpPost]
        [AllowAnonymous]
        public ActionResult PayLane()
        {
            string status = string.Empty;
            decimal amount = 0;
            string currency = string.Empty;
            string description = string.Empty;
            string hash = string.Empty;
            int id_sale = 0;

            status = Request.QueryString["status"];

            if (status.ToLower() != "error")
            {
                amount = decimal.Parse(Request.QueryString["amount"].Replace(".", ","));
                currency = Request.QueryString["currency"];
                description = Request.QueryString["description"];
                hash = Request.QueryString["hash"];
                id_sale = int.Parse(Request.QueryString["id_sale"]);

                int paylane_id = int.Parse(description);

                //paylane
                class_paylane paylane = new class_paylane(paylane_id);
                paylane.date_response = DateTime.Now;
                paylane.czy_anonymous = Convert.ToBoolean(session.czy_anonymous);
                paylane.response_status = status;
                paylane.response_description = description;
                paylane.response_amount = amount;
                paylane.response_currency = currency;
                paylane.response_hash = hash;
                //paylane.response_id_authorization = int.Parse(form["id_authorization"]);
                paylane.response_id_sale = id_sale;
                //paylane.response_id_error = int.Parse(form["id_error"]);
                //paylane.response_error_code = int.Parse(form["error_code"]);
                //paylane.response_error_text = form["error_text"];
                //paylane.response_fraud_score = decimal.Parse(form["fraud_score"]);
                //paylane.response_avs_result = form["avs_result"];
                paylane.update();

                //project
                class_project project = new class_project(paylane.project_id);
                project.generate_list_beneficiary();

                //commented out - 'email send' moved to post notification
                ////user
                //class_user user = new class_user(paylane.user_id);

                ////email
                //class_email email = new class_email();

                //string msg_template = System.IO.File.ReadAllText(Server.MapPath("/assets/email_templates/thankyou_email_template.html"));

                //if (email.send(user.email, user.subject_thankyou, user.body_thankyou(msg_template, paylane, project)))
                //{
                //    //success
                //}
                //else
                //{
                //    //error
                //}

                session.paylane_result = "success";

                return Redirect("~/Projekty/" + project.name);
            }
            else
            {
                session.paylane_result = "error";

                description = Request.QueryString["description"];
                if (description != null)
                {
                    return Redirect("~/Projekty/" + (new class_project((new class_paylane(int.Parse(description))).project_id)).name);
                }
                else
                {
                    return Redirect("~/Home/");
                }
            }
        }

        private Uri RedirectUri
        {
            get
            {
                var uriBuilder = new UriBuilder(Request.Url);
                uriBuilder.Query = null;
                uriBuilder.Fragment = null;
                uriBuilder.Path = Url.Action("FacebookCallback");
                return uriBuilder.Uri;
            }
        }

        [AllowAnonymous]
        public ActionResult Facebook()
        {
            //id
            string id = string.Empty;

            if (this.validation.czy_string_not_empty(RouteData.Values["id"]))
            {
                id = RouteData.Values["id"].ToString();

                //session
                session.project_id = id;
            }
            else
            {

            }

            var fb = new FacebookClient();
            var loginUrl = fb.GetLoginUrl(new
            {
                client_id = this.settings.facebook_app_id,
                client_secret = this.settings.facebook_app_secret,
                redirect_uri = RedirectUri.AbsoluteUri,
                response_type = "code",
                scope = "email" // Add other permissions as needed
            });

            return Redirect(loginUrl.AbsoluteUri);
        }

        [AllowAnonymous]
        public ActionResult FacebookCallback(string code)
        {
            var fb = new FacebookClient();

            //kliknięcie ok w aplikacji FB
            try
            {
                dynamic result = fb.Post("oauth/access_token", new
                {
                    client_id = this.settings.facebook_app_id,
                    client_secret = this.settings.facebook_app_secret,
                    redirect_uri = RedirectUri.AbsoluteUri,
                    code = code
                });

                var accessToken = result.access_token;
                fb.AccessToken = accessToken;

                //facebook_client
                dynamic facebook_client = fb.Get("me?fields=first_name,last_name,id,email");

                if (facebook_client.email != null)
                {
                    //user
                    class_user user = new class_user();

                    if (user.exists(class_user.search_type.facebook_id, facebook_client.id))
                    {
                        //user_id ustawione w funkcji user.exists()
                    }
                    else
                    {
                        user.active = true;
                        user.login = string.Empty;
                        user.password = string.Empty;

                        user.name = facebook_client.first_name;
                        user.surname = facebook_client.last_name;
                        user.email = facebook_client.email;
                        user.date_create = DateTime.Now;
                        user.date_password_change = this.sql.data_default;
                        user.date_last_logged = this.sql.data_default;
                        user.uid_password_reset = string.Empty;
                        user.uid_register = user.generate_uid();
                        user.facebook_id = facebook_client.id;

                        //insert
                        user.insert(class_user.insert_type.facebook);
                    }

                    //session
                    session.user_id = user.id.ToString();
                    session.czy_fb_error = "false";
                }
                else
                {
                    //session
                    session.czy_fb_error = "true";
                }
            }
            //kliknięcie anuluj w aplikacji FB
            catch
            {
                //session
                session.czy_fb_error = "true";
            }

            //id
            int id = 0;

            //project
            class_project project;

            //session
            if (session.project_id != null)
            {
                id = int.Parse(session.project_id);
                project = new class_project(id);

                return RedirectToAction("Index", "Projekty", new { id = project.name });
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
    }
}
