﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//
using SfCms_model;

namespace SfCms_user.Controllers
{
    public class ProjektyController : Controller
    {
        //pattern
        private class_pattern pattern = new class_pattern();
        //validation
        private class_validation validation = new class_validation();
        //tool
        private class_tool tool = new class_tool();

        //Index
        public ActionResult Index()
        {
            //menu
            class_menu menu = new class_menu();
            menu.generate_list();

			//id
			string id = string.Empty;

			//project
			class_project project = new class_project();

            //beneficiary
            class_beneficiary beneficiary = new class_beneficiary();

            //beneficiary_file
            class_beneficiary_file beneficiary_file = new class_beneficiary_file();

            //mentor
            class_mentor mentor = new class_mentor();

            //mentor_file
            class_mentor_file mentor_file = new class_mentor_file();

            //paylane
            class_paylane paylane = new class_paylane();

            //blog
            class_blog blog = new class_blog();

            //blog_file
            class_blog_file blog_file = new class_blog_file();

            //s_post
            class_s_post s_post = new class_s_post();

			if (this.validation.czy_string_not_empty(RouteData.Values["id"]))
			{
				id = RouteData.Values["id"].ToString();

				project.generate_list(class_project.search_type.name, id);

                if (project.list.Count == 1)
                {
                    //project
                    project = project.list[0];

					//mentor
					mentor.generate_list(class_mentor.search_type.project_id, project.id.ToString());

                    //beneficiary
                    beneficiary.generate_list(class_beneficiary.search_type.project_id, project.id.ToString());

                    //paylane
                    paylane.generate_list(class_paylane.search_type.donor_all, project.id.ToString());
                }
			}
			else
			{
				
			}

            int support_tens = project.support % 100, support_singles = support_tens % 10;
            string str_osoby = "osób", str_wsparcie = "wsparło";

            if (project.support == 1)
            {
                str_osoby = "osoba";
                str_wsparcie = "wsparła";
            }
            else if (support_tens > 5 && support_tens < 21)
            {
                str_osoby = "osób";
                str_wsparcie = "wsparło";
            }
            else if (support_singles >= 2 && support_singles <= 4)
            {
                str_osoby = "osoby";
                str_wsparcie = "wsparły";
            }
            else if (support_singles == 1 || (support_singles >= 5 && support_singles <= 9))
            {
                str_osoby = "osób";
                str_wsparcie = "wsparło";
            }

            //pattern - begin
            this.pattern.menu = menu;
            this.pattern.project = project;
            this.pattern.beneficiary = beneficiary;
            this.pattern.mentor = mentor;
            this.pattern.paylane = paylane;
            this.pattern.beneficiary_file = beneficiary_file;
            this.pattern.mentor_file = mentor_file;
            this.pattern.blog = blog;
            this.pattern.blog_file = blog_file;
            this.pattern.s_post = s_post;
            this.pattern.tool = tool;

            this.pattern.str_osoby = str_osoby;
            this.pattern.str_wsparcie = str_wsparcie;
            //patern - end

            return View(this.pattern);
        }
    }
}
