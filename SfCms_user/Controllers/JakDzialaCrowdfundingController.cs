﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//
using SfCms_model;

namespace SfCms_user.Controllers
{
    public class JakDzialaCrowdfundingController : Controller
    {
        //pattern
        private class_pattern pattern = new class_pattern();

        //Index
        public ActionResult Index()
        {
            //menu
            class_menu menu = new class_menu();
            menu.generate_list();

            //pattern - begin
            this.pattern.menu = menu;
            //patern - end

            return View(this.pattern);
        }
    }
}
