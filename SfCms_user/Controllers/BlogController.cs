﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//
using SfCms_model;
using System.IO;
using System.Web.UI;
using ImageResizer;

namespace SfCms_user.Controllers
{
	[AuthorizeSfCms_beneficiary()]
    public class BlogController : Controller
    {
        //pattern
        private class_pattern pattern = new class_pattern();
        //validation
        private class_validation validation = new class_validation();
        //settings
        private class_settings settings = new class_settings();

        public ActionResult Index()
        {
            //menu
            class_menu menu = new class_menu();
            menu.generate_list();

			//beneficiary
			class_beneficiary beneficiary = new class_beneficiary(int.Parse(session.beneficiary_id));

			//beneficiary_file
			class_beneficiary_file beneficiary_file = new class_beneficiary_file();

			//project
			class_project project = new class_project(beneficiary.project_id);

            //blog
            class_blog blog = new class_blog();
            blog.generate_list(class_blog.search_type.beneficiary_id, beneficiary.id.ToString());

			//blog_file
			class_blog_file blog_file = new class_blog_file();

            //s_post
            class_s_post s_post = new class_s_post();          

            //pattern - begin
            this.pattern.menu = menu;
			this.pattern.beneficiary = beneficiary;
			this.pattern.beneficiary_file = beneficiary_file;
			this.pattern.project = project;
            this.pattern.blog = blog;
            this.pattern.blog_file = blog_file;
            this.pattern.s_post = s_post;
            //patern - end

            return View(this.pattern);
        }

        public ActionResult AddAsk()
        {
            //menu
            class_menu menu = new class_menu();
            menu.generate_list();

			//beneficiary
			class_beneficiary beneficiary = new class_beneficiary(int.Parse(session.beneficiary_id));

			//beneficiary_file
			class_beneficiary_file beneficiary_file = new class_beneficiary_file();

			//project
			class_project project = new class_project(beneficiary.project_id);

            //s
            class_s s = new class_s(class_s.dictionary.s_post);

            //pattern - begin
            this.pattern.menu = menu;
			this.pattern.beneficiary = beneficiary;
			this.pattern.beneficiary_file = beneficiary_file;
			this.pattern.project = project;
            this.pattern.s = s;
            //patern - end

            return View(this.pattern);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Add(FormCollection form, HttpPostedFileBase file_1, HttpPostedFileBase file_2, HttpPostedFileBase file_3, HttpPostedFileBase file_4, HttpPostedFileBase file_5)
        {
            class_sql sql = new class_sql();

            //form
            bool form_active = false;
            int form_s_post_id = 0;
            string form_title = string.Empty;
            string form_content = string.Empty;
            DateTime form_date_user = sql.data_default;
            string form_media_link = string.Empty;

            if (form["active"] == "true")
            {
                form_active = true;
            }
            form_s_post_id = int.Parse(form["s_post_id"]);
            form_title = form["title"];
            form_content = form["content"];
            form_date_user = Convert.ToDateTime(form["date_user"]);
            form_media_link = form["media_link"];

            //blog
            class_blog blog = new class_blog(0);
            blog.active = form_active;
            blog.beneficiary_id = int.Parse(session.beneficiary_id);
            blog.s_post_id = form_s_post_id;
            blog.title = form_title;
            blog.content = form_content;
            blog.date_user = form_date_user;
            blog.media_link = form_media_link;
            blog.insert();

            //file_1
            #region "file_1"
            if (file_1 != null)
            {
				string file_title = form["file_1_title"];
                int no = 1;

                //blog_file_1
                class_blog_file blog_file_1 = new class_blog_file();

                //image
                class_image image = new class_image(file_1.ContentType);

                //version
                foreach (var suffix in image.versions.Keys)
                {         
                    file_1.InputStream.Seek(0, SeekOrigin.Begin);
                    image.stream_out = new System.IO.MemoryStream();
                    ImageBuilder.Current.Build(new ImageJob(file_1.InputStream,image.stream_out,new Instructions(image.versions[suffix]), false, true));
                    image.stream_out.Seek(0, SeekOrigin.Begin);

                    if(suffix == image.large)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        blog_file_1.content_large = image.file_content;
                    }
                    else if (suffix == image.medium)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        blog_file_1.content_medium = image.file_content;
                    }
                    else if (suffix == image.small)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        blog_file_1.content_small = image.file_content;
                    }
                }

                blog_file_1.blog_id = blog.id;
                blog_file_1.no = no;
                blog_file_1.content_type = file_1.ContentType;
                blog_file_1.name = file_1.FileName;
                blog_file_1.title = file_title;
                blog_file_1.insert();
            }
            #endregion

            //file_2
            #region "file_2"
            if (file_2 != null)
            {
                string file_title = form["file_2_title"];
                int no = 2;

                //blog_file_2
                class_blog_file blog_file_2 = new class_blog_file();

                //image
                class_image image = new class_image(file_2.ContentType);

                //version
                foreach (var suffix in image.versions.Keys)
                {
                    file_2.InputStream.Seek(0, SeekOrigin.Begin);
                    image.stream_out = new System.IO.MemoryStream();
                    ImageBuilder.Current.Build(new ImageJob(file_2.InputStream, image.stream_out, new Instructions(image.versions[suffix]), false, true));
                    image.stream_out.Seek(0, SeekOrigin.Begin);

                    if (suffix == image.large)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        blog_file_2.content_large = image.file_content;
                    }
                    else if (suffix == image.medium)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        blog_file_2.content_medium = image.file_content;
                    }
                    else if (suffix == image.small)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        blog_file_2.content_small = image.file_content;
                    }
                }

                blog_file_2.blog_id = blog.id;
                blog_file_2.no = no;
                blog_file_2.content_type = file_1.ContentType;
                blog_file_2.name = file_2.FileName;
                blog_file_2.title = file_title;
                blog_file_2.insert();
            }
            #endregion

            //file_3
            #region "file_3"
            if (file_3 != null)
            {
                string file_title = form["file_3_title"];
                int no = 3;

                //blog_file_3
                class_blog_file blog_file_3 = new class_blog_file();

                //image
                class_image image = new class_image(file_3.ContentType);

                //version
                foreach (var suffix in image.versions.Keys)
                {
                    file_3.InputStream.Seek(0, SeekOrigin.Begin);
                    image.stream_out = new System.IO.MemoryStream();
                    ImageBuilder.Current.Build(new ImageJob(file_3.InputStream, image.stream_out, new Instructions(image.versions[suffix]), false, true));
                    image.stream_out.Seek(0, SeekOrigin.Begin);

                    if (suffix == image.large)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        blog_file_3.content_large = image.file_content;
                    }
                    else if (suffix == image.medium)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        blog_file_3.content_medium = image.file_content;
                    }
                    else if (suffix == image.small)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        blog_file_3.content_small = image.file_content;
                    }
                }

                blog_file_3.blog_id = blog.id;
                blog_file_3.no = no;
                blog_file_3.content_type = file_1.ContentType;
                blog_file_3.name = file_3.FileName;
                blog_file_3.title = file_title;
                blog_file_3.insert();
            }
            #endregion

            //file_4
            #region "file_4"
            if (file_4 != null)
            {
                string file_title = form["file_4_title"];
                int no = 4;

                //blog_file_4
                class_blog_file blog_file_4 = new class_blog_file();

                //image
                class_image image = new class_image(file_4.ContentType);

                //version
                foreach (var suffix in image.versions.Keys)
                {
                    file_4.InputStream.Seek(0, SeekOrigin.Begin);
                    image.stream_out = new System.IO.MemoryStream();
                    ImageBuilder.Current.Build(new ImageJob(file_4.InputStream, image.stream_out, new Instructions(image.versions[suffix]), false, true));
                    image.stream_out.Seek(0, SeekOrigin.Begin);

                    if (suffix == image.large)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        blog_file_4.content_large = image.file_content;
                    }
                    else if (suffix == image.medium)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        blog_file_4.content_medium = image.file_content;
                    }
                    else if (suffix == image.small)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        blog_file_4.content_small = image.file_content;
                    }
                }

                blog_file_4.blog_id = blog.id;
                blog_file_4.no = no;
                blog_file_4.content_type = file_1.ContentType;
                blog_file_4.name = file_4.FileName;
                blog_file_4.title = file_title;
                blog_file_4.insert();
            }
            #endregion

            //file_5
            #region "file_5"
            if (file_5 != null)
            {
                string file_title = form["file_5_title"];
                int no = 5;

                //blog_file_5
                class_blog_file blog_file_5 = new class_blog_file();

                //image
                class_image image = new class_image(file_5.ContentType);

                //version
                foreach (var suffix in image.versions.Keys)
                {
                    file_5.InputStream.Seek(0, SeekOrigin.Begin);
                    image.stream_out = new System.IO.MemoryStream();
                    ImageBuilder.Current.Build(new ImageJob(file_5.InputStream, image.stream_out, new Instructions(image.versions[suffix]), false, true));
                    image.stream_out.Seek(0, SeekOrigin.Begin);

                    if (suffix == image.large)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        blog_file_5.content_large = image.file_content;
                    }
                    else if (suffix == image.medium)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        blog_file_5.content_medium = image.file_content;
                    }
                    else if (suffix == image.small)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        blog_file_5.content_small = image.file_content;
                    }
                }

                blog_file_5.blog_id = blog.id;
                blog_file_5.no = no;
                blog_file_5.content_type = file_1.ContentType;
                blog_file_5.name = file_5.FileName;
                blog_file_5.title = file_title;
                blog_file_5.insert();
            }
            #endregion

            return RedirectToAction("Index");
        }

        public ActionResult EditAsk()
        {
            //id
            if (!this.validation.czy_int(RouteData.Values["id"]))
            {
                return RedirectToAction("HttpError404", "Error");
            }
            else
            {
                int id = int.Parse(RouteData.Values["id"].ToString());

                //menu
                class_menu menu = new class_menu();
                menu.generate_list();

				//beneficiary
				class_beneficiary beneficiary = new class_beneficiary(int.Parse(session.beneficiary_id));

				//beneficiary_file
				class_beneficiary_file beneficiary_file = new class_beneficiary_file();

				//project
				class_project project = new class_project(beneficiary.project_id);

                //blog
                class_blog blog = new class_blog(id);

                //kontrola dostępu do bloga
                if (blog.beneficiary_id != beneficiary.id)
                {
                    return RedirectToAction("HttpError404", "Error");
                }

                //blog_file
                class_blog_file blog_file = new class_blog_file();
                blog_file.generate_list(class_blog_file.search_type.blog_id, blog.id.ToString());

                //s
                class_s s = new class_s(class_s.dictionary.s_post);
                
                //pattern - begin
                this.pattern.menu = menu;
				this.pattern.beneficiary = beneficiary;
				this.pattern.beneficiary_file = beneficiary_file;
				this.pattern.project = project;
                this.pattern.blog = blog;
                this.pattern.blog_file = blog_file;
                this.pattern.s = s;
                //patern - end

                return View(this.pattern);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(FormCollection form, HttpPostedFileBase file_1, HttpPostedFileBase file_2, HttpPostedFileBase file_3, HttpPostedFileBase file_4, HttpPostedFileBase file_5)
        {
            class_sql sql = new class_sql();

            //form
            int form_id = 0;
            bool form_active = false;
            int form_s_post_id = 0;
            string form_title = string.Empty;
            string form_content = string.Empty;
            DateTime form_date_user = sql.data_default;
            string form_media_link = string.Empty;

            form_id = int.Parse(form["id"].ToString());
            if (form["active"] == "true")
            {
                form_active = true;
            }
            form_s_post_id = int.Parse(form["s_post_id"]);
            form_title = form["title"];
            form_content = form["content"];
            form_date_user = Convert.ToDateTime(form["date_user"]);
            form_media_link = form["media_link"];

            //blog
            class_blog blog = new class_blog(form_id);
            blog.active = form_active;
            blog.beneficiary_id = int.Parse(session.beneficiary_id);
            blog.s_post_id = form_s_post_id;
            blog.title = form_title;
            blog.content = form_content;
            blog.date_user = form_date_user;
            blog.media_link = form_media_link;
            blog.update();

            //blog_file
            class_blog_file blog_file = new class_blog_file();
            blog_file.generate_list(class_blog_file.search_type.blog_id, blog.id.ToString());

            //file_1
            #region "file_1"
			if (file_1 != null)
			{
                string file_title = form["file_1_title"];
                int no = 1;

                //blog_file_1
                class_blog_file blog_file_1 = new class_blog_file();

                if (blog_file.list.Count >= 1)
                {
                    blog_file_1.id = blog_file.list[0].id;
                    blog_file_1.delete();
                }

                //image
                class_image image = new class_image(file_1.ContentType);

                //version
                foreach (var suffix in image.versions.Keys)
                {
                    file_1.InputStream.Seek(0, SeekOrigin.Begin);
                    image.stream_out = new System.IO.MemoryStream();
                    ImageBuilder.Current.Build(new ImageJob(file_1.InputStream, image.stream_out, new Instructions(image.versions[suffix]), false, true));
                    image.stream_out.Seek(0, SeekOrigin.Begin);

                    if (suffix == image.large)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        blog_file_1.content_large = image.file_content;
                    }
                    else if (suffix == image.medium)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        blog_file_1.content_medium = image.file_content;
                    }
                    else if (suffix == image.small)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        blog_file_1.content_small = image.file_content;
                    }
                }

                blog_file_1.blog_id = blog.id;
                blog_file_1.no = no;
                blog_file_1.content_type = file_1.ContentType;
                blog_file_1.name = file_1.FileName;
                blog_file_1.title = file_title;
                blog_file_1.insert();
			}
			else
			{
				if (blog_file.list.Count >= 1)
				{
					var query_blog_file = from bf in blog_file.list where bf.no == 1 select bf;

					foreach (class_blog_file bf in query_blog_file)
					{
						string file_title = form["file_1_title"];

						//blog_file_1
						class_blog_file blog_file_1 = new class_blog_file();

						blog_file_1.id = bf.id;
						blog_file_1.title = file_title;
						blog_file_1.update();
					}
				}
			}
            #endregion

            //file_2
            #region "file_2"
            if (file_2 != null)
            {
                string file_title = form["file_2_title"];
                int no = 2;

                //blog_file_2
                class_blog_file blog_file_2 = new class_blog_file();

                if (blog_file.list.Count >= 2)
                {
                    blog_file_2.id = blog_file.list[1].id;
                    blog_file_2.delete();
                }

                //image
                class_image image = new class_image(file_2.ContentType);

                //version
                foreach (var suffix in image.versions.Keys)
                {
                    file_2.InputStream.Seek(0, SeekOrigin.Begin);
                    image.stream_out = new System.IO.MemoryStream();
                    ImageBuilder.Current.Build(new ImageJob(file_2.InputStream, image.stream_out, new Instructions(image.versions[suffix]), false, true));
                    image.stream_out.Seek(0, SeekOrigin.Begin);

                    if (suffix == image.large)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        blog_file_2.content_large = image.file_content;
                    }
                    else if (suffix == image.medium)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        blog_file_2.content_medium = image.file_content;
                    }
                    else if (suffix == image.small)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        blog_file_2.content_small = image.file_content;
                    }
                }

                blog_file_2.blog_id = blog.id;
                blog_file_2.no = no;
                blog_file_2.content_type = file_2.ContentType;
                blog_file_2.name = file_2.FileName;
                blog_file_2.title = file_title;
                blog_file_2.insert();
            }
			else
			{
				if (blog_file.list.Count >= 2)
				{
					var query_blog_file = from bf in blog_file.list where bf.no == 2 select bf;

					foreach (class_blog_file bf in query_blog_file)
					{
						string file_title = form["file_2_title"];

						//blog_file_2
						class_blog_file blog_file_2 = new class_blog_file();

						blog_file_2.id = bf.id;
						blog_file_2.title = file_title;
						blog_file_2.update();
					}
				}
			}
            #endregion

            //file_3
            #region "file_3"
            if (file_3 != null)
            {
                string file_title = form["file_3_title"];
                int no = 3;

                //blog_file_3
                class_blog_file blog_file_3 = new class_blog_file();

                if (blog_file.list.Count >= 3)
                {
                    blog_file_3.id = blog_file.list[2].id;
                    blog_file_3.delete();
                }

                //image
                class_image image = new class_image(file_3.ContentType);

                //version
                foreach (var suffix in image.versions.Keys)
                {
                    file_3.InputStream.Seek(0, SeekOrigin.Begin);
                    image.stream_out = new System.IO.MemoryStream();
                    ImageBuilder.Current.Build(new ImageJob(file_3.InputStream, image.stream_out, new Instructions(image.versions[suffix]), false, true));
                    image.stream_out.Seek(0, SeekOrigin.Begin);

                    if (suffix == image.large)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        blog_file_3.content_large = image.file_content;
                    }
                    else if (suffix == image.medium)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        blog_file_3.content_medium = image.file_content;
                    }
                    else if (suffix == image.small)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        blog_file_3.content_small = image.file_content;
                    }
                }

                blog_file_3.blog_id = blog.id;
                blog_file_3.no = no;
                blog_file_3.content_type = file_3.ContentType;
                blog_file_3.name = file_3.FileName;
                blog_file_3.title = file_title;
                blog_file_3.insert();
            }
			else
			{
				if (blog_file.list.Count >= 3)
				{
					var query_blog_file = from bf in blog_file.list where bf.no == 3 select bf;

					foreach (class_blog_file bf in query_blog_file)
					{
						string file_title = form["file_3_title"];

						//blog_file_3
						class_blog_file blog_file_3 = new class_blog_file();

						blog_file_3.id = bf.id;
						blog_file_3.title = file_title;
						blog_file_3.update();
					}
				}
			}
            #endregion

            //file_4
            #region "file_4"
            if (file_4 != null)
            {
                string file_title = form["file_4_title"];
                int no = 4;

                //blog_file_4
                class_blog_file blog_file_4 = new class_blog_file();

                if (blog_file.list.Count >= 4)
                {
                    blog_file_4.id = blog_file.list[3].id;
                    blog_file_4.delete();
                }

                //image
                class_image image = new class_image(file_4.ContentType);

                //version
                foreach (var suffix in image.versions.Keys)
                {
                    file_4.InputStream.Seek(0, SeekOrigin.Begin);
                    image.stream_out = new System.IO.MemoryStream();
                    ImageBuilder.Current.Build(new ImageJob(file_4.InputStream, image.stream_out, new Instructions(image.versions[suffix]), false, true));
                    image.stream_out.Seek(0, SeekOrigin.Begin);

                    if (suffix == image.large)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        blog_file_4.content_large = image.file_content;
                    }
                    else if (suffix == image.medium)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        blog_file_4.content_medium = image.file_content;
                    }
                    else if (suffix == image.small)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        blog_file_4.content_small = image.file_content;
                    }
                }

                blog_file_4.blog_id = blog.id;
                blog_file_4.no = no;
                blog_file_4.content_type = file_4.ContentType;
                blog_file_4.name = file_4.FileName;
                blog_file_4.title = file_title;
                blog_file_4.insert();
            }
			else
			{
				if (blog_file.list.Count >= 4)
				{
					var query_blog_file = from bf in blog_file.list where bf.no == 4 select bf;

					foreach (class_blog_file bf in query_blog_file)
					{
						string file_title = form["file_4_title"];

						//blog_file_4
						class_blog_file blog_file_4 = new class_blog_file();

						blog_file_4.id = bf.id;
						blog_file_4.title = file_title;
						blog_file_4.update();
					}
				}
			}
            #endregion

            //file_5
            #region "file_5"
            if (file_5 != null)
            {
                string file_title = form["file_5_title"];
                int no = 5;

                //blog_file_5
                class_blog_file blog_file_5 = new class_blog_file();

                if (blog_file.list.Count >= 5)
                {
                    blog_file_5.id = blog_file.list[4].id;
                    blog_file_5.delete();
                }

                //image
                class_image image = new class_image(file_5.ContentType);

                //version
                foreach (var suffix in image.versions.Keys)
                {
                    file_5.InputStream.Seek(0, SeekOrigin.Begin);
                    image.stream_out = new System.IO.MemoryStream();
                    ImageBuilder.Current.Build(new ImageJob(file_5.InputStream, image.stream_out, new Instructions(image.versions[suffix]), false, true));
                    image.stream_out.Seek(0, SeekOrigin.Begin);

                    if (suffix == image.large)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        blog_file_5.content_large = image.file_content;
                    }
                    else if (suffix == image.medium)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        blog_file_5.content_medium = image.file_content;
                    }
                    else if (suffix == image.small)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        blog_file_5.content_small = image.file_content;
                    }
                }

                blog_file_5.blog_id = blog.id;
                blog_file_5.no = no;
                blog_file_5.content_type = file_5.ContentType;
                blog_file_5.name = file_5.FileName;
                blog_file_5.title = file_title;
                blog_file_5.insert();
            }
			else
			{
				if (blog_file.list.Count >= 5)
				{
					var query_blog_file = from bf in blog_file.list where bf.no == 5 select bf;

					foreach (class_blog_file bf in query_blog_file)
					{
						string file_title = form["file_5_title"];

						//blog_file_5
						class_blog_file blog_file_5 = new class_blog_file();

						blog_file_5.id = bf.id;
						blog_file_5.title = file_title;
						blog_file_5.update();
					}
				}
			}
            #endregion

            return RedirectToAction("Index");
        }

        public ActionResult Show()
        {
            //id
            if (!this.validation.czy_int(RouteData.Values["id"]))
            {
                return RedirectToAction("HttpError404", "Error");
            }
            else
            {
                int id = int.Parse(RouteData.Values["id"].ToString());

                //menu
                class_menu menu = new class_menu();
                menu.generate_list();

				//beneficiary
				class_beneficiary beneficiary = new class_beneficiary(int.Parse(session.beneficiary_id));

				//beneficiary_file
				class_beneficiary_file beneficiary_file = new class_beneficiary_file();

				//project
				class_project project = new class_project(beneficiary.project_id);

                //blog
                class_blog blog = new class_blog(id);

                //kontrola dostępu do bloga
                if (blog.beneficiary_id != beneficiary.id)
                {
                    return RedirectToAction("HttpError404", "Error");
                }

                //blog_file
                class_blog_file blog_file = new class_blog_file();
                blog_file.generate_list(class_blog_file.search_type.blog_id, blog.id.ToString());

                //s_post
                class_s_post s_post = new class_s_post(blog.s_post_id);

                //pattern - begin
                this.pattern.menu = menu;
				this.pattern.beneficiary = beneficiary;
				this.pattern.beneficiary_file = beneficiary_file;
				this.pattern.project = project;
                this.pattern.blog = blog;
                this.pattern.blog_file = blog_file;
                this.pattern.s_post = s_post;
                //patern - end

                return View(this.pattern);
            }
        }

        public ActionResult DeleteAsk()
        {
            //id
            if (!this.validation.czy_int(RouteData.Values["id"]))
            {
                return RedirectToAction("HttpError404", "Error");
            }
            else
            {
                int id = int.Parse(RouteData.Values["id"].ToString());

                //menu
                class_menu menu = new class_menu();
                menu.generate_list();

				//beneficiary
				class_beneficiary beneficiary = new class_beneficiary(int.Parse(session.beneficiary_id));

				//beneficiary_file
				class_beneficiary_file beneficiary_file = new class_beneficiary_file();

				//project
				class_project project = new class_project(beneficiary.project_id);

                //blog
                class_blog blog = new class_blog(id);

                //kontrola dostępu do bloga
                if (blog.beneficiary_id != beneficiary.id)
                {
                    return RedirectToAction("HttpError404", "Error");
                }

                //blog_file
                class_blog_file blog_file = new class_blog_file();
                blog_file.generate_list(class_blog_file.search_type.blog_id, blog.id.ToString());

                //s_post
                class_s_post s_post = new class_s_post(blog.s_post_id);

                //pattern - begin
                this.pattern.menu = menu;
				this.pattern.beneficiary = beneficiary;
				this.pattern.beneficiary_file = beneficiary_file;
				this.pattern.project = project;
                this.pattern.blog = blog;
                this.pattern.blog_file = blog_file;
                this.pattern.s_post = s_post;
                //patern - end

                return View(this.pattern);
            }
        }

        [HttpPost]
        public ActionResult Delete(FormCollection form)
        {
            //form
            int form_id = 0;

            form_id = int.Parse(form["id"].ToString());

            //blog
            class_blog blog = new class_blog(form_id);
            blog.delete();

            //blog_file
            class_blog_file blog_file = new class_blog_file();
            blog_file.generate_list(class_blog_file.search_type.blog_id, blog.id.ToString());

            if (blog_file.list.Count > 0)
            {
                for (var i = 0; i < blog_file.list.Count; i++)
                {
                    blog_file.id = blog_file.list[i].id;
                    blog_file.delete();
                }
            }

            return RedirectToAction("Index");
        }
    }
}
