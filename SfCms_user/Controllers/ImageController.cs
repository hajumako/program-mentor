﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//
using SfCms_model;

namespace SfCms_user.Controllers
{
    public class ImageController : Controller
    {
        private class_validation validation = new class_validation();

        public FileContentResult BlogImageLarge()
        {
            //id
            if (!this.validation.czy_int(RouteData.Values["id"]))
            {
                return null;
            }
            else
            {
                int id = int.Parse(RouteData.Values["id"].ToString());

                class_blog_file blog_file = new class_blog_file(id, class_blog_file.image_type.large);

                return File(blog_file.content, blog_file.content_type);
            }
        }

        public FileContentResult BlogImageMedium()
        {
            //id
            if (!this.validation.czy_int(RouteData.Values["id"]))
            {
                return null;
            }
            else
            {
                int id = int.Parse(RouteData.Values["id"].ToString());

                class_blog_file blog_file = new class_blog_file(id, class_blog_file.image_type.medium);

                return File(blog_file.content, blog_file.content_type);
            }
        }

        public FileContentResult BlogImageSmall()
        {
            //id
            if (!this.validation.czy_int(RouteData.Values["id"]))
            {
                return null;
            }
            else
            {
                int id = int.Parse(RouteData.Values["id"].ToString());

                class_blog_file blog_file = new class_blog_file(id, class_blog_file.image_type.small);

                return File(blog_file.content, blog_file.content_type);
            }
        }

        public FileContentResult MentorImageLarge()
        {
            //id
            if (!this.validation.czy_int(RouteData.Values["id"]))
            {
                return null;
            }
            else
            {
                int id = int.Parse(RouteData.Values["id"].ToString());

                class_mentor_file mentor_file = new class_mentor_file(class_mentor_file.search_type.mentor_id, class_mentor_file.image_type.large, id.ToString());

                return File(mentor_file.content, mentor_file.content_type);
            }
        }

        public FileContentResult MentorImageMedium()
        {
            //id
            if (!this.validation.czy_int(RouteData.Values["id"]))
            {
                return null;
            }
            else
            {
                int id = int.Parse(RouteData.Values["id"].ToString());

                class_mentor_file mentor_file = new class_mentor_file(class_mentor_file.search_type.mentor_id, class_mentor_file.image_type.medium, id.ToString());

                return File(mentor_file.content, mentor_file.content_type);
            }
        }

        public FileContentResult MentorImageSmall()
        {
            //id
            if (!this.validation.czy_int(RouteData.Values["id"]))
            {
                return null;
            }
            else
            {
                int id = int.Parse(RouteData.Values["id"].ToString());

                class_mentor_file mentor_file = new class_mentor_file(class_mentor_file.search_type.mentor_id, class_mentor_file.image_type.small, id.ToString());

                return File(mentor_file.content, mentor_file.content_type);
            }
        }

        public FileContentResult BeneficiaryImageLarge()
        {
            //id
            if (!this.validation.czy_int(RouteData.Values["id"]))
            {
                return null;
            }
            else
            {
                int id = int.Parse(RouteData.Values["id"].ToString());

                class_beneficiary_file beneficiary_file = new class_beneficiary_file(class_beneficiary_file.search_type.beneficiary_id, class_beneficiary_file.image_type.large, id.ToString());

                return File(beneficiary_file.content, beneficiary_file.content_type);
            }
        }

        public FileContentResult BeneficiaryImageMedium()
        {
            //id
            if (!this.validation.czy_int(RouteData.Values["id"]))
            {
                return null;
            }
            else
            {
                int id = int.Parse(RouteData.Values["id"].ToString());

                class_beneficiary_file beneficiary_file = new class_beneficiary_file(class_beneficiary_file.search_type.beneficiary_id, class_beneficiary_file.image_type.medium, id.ToString());

                return File(beneficiary_file.content, beneficiary_file.content_type);
            }
        }

        public FileContentResult BeneficiaryImageSmall()
        {
            //id
            if (!this.validation.czy_int(RouteData.Values["id"]))
            {
                return null;
            }
            else
            {
                int id = int.Parse(RouteData.Values["id"].ToString());

                class_beneficiary_file beneficiary_file = new class_beneficiary_file(class_beneficiary_file.search_type.beneficiary_id, class_beneficiary_file.image_type.small, id.ToString());

                return File(beneficiary_file.content, beneficiary_file.content_type);
            }
        }

        public FileContentResult NewsImageLarge()
        {
            //id
            if (!this.validation.czy_int(RouteData.Values["id"]))
            {
                return null;
            }
            else
            {
                int id = int.Parse(RouteData.Values["id"].ToString());

                class_news_file news_file = new class_news_file(class_news_file.search_type.news_id, class_news_file.image_type.large, id.ToString());

                return File(news_file.content, news_file.content_type);
            }
        }

        public FileContentResult NewsImageMedium()
        {
            //id
            if (!this.validation.czy_int(RouteData.Values["id"]))
            {
                return null;
            }
            else
            {
                int id = int.Parse(RouteData.Values["id"].ToString());

                class_news_file news_file = new class_news_file(class_news_file.search_type.news_id, class_news_file.image_type.medium, id.ToString());

                return File(news_file.content, news_file.content_type);
            }
        }

        public FileContentResult NewsImageSmall()
        {
            //id
            if (!this.validation.czy_int(RouteData.Values["id"]))
            {
                return null;
            }
            else
            {
                int id = int.Parse(RouteData.Values["id"].ToString());

                class_news_file news_file = new class_news_file(class_news_file.search_type.news_id, class_news_file.image_type.small, id.ToString());

                return File(news_file.content, news_file.content_type);
            }
        }
    }
}
