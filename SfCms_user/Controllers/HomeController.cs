﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//
using SfCms_model;

namespace SfCms_user.Controllers
{
    public class HomeController : Controller
    {
		//pattern
		private class_pattern pattern = new class_pattern();

		//Index
        public ActionResult Index()
        {
			//menu
			class_menu menu = new class_menu();
			menu.generate_list();

            //beneficiary
            class_beneficiary beneficiary = new class_beneficiary();
            beneficiary.generate_list(class_beneficiary.search_type.active, string.Empty);

            //beneficiary_file
            class_beneficiary_file beneficiary_file = new class_beneficiary_file();

            //project
            class_project project = new class_project();

            class_news news = new class_news();
            news.generate_list(class_news.search_type.active, string.Empty);

			//pattern - begin
			this.pattern.menu = menu;
            this.pattern.news = news;
            this.pattern.beneficiary = beneficiary;
            this.pattern.beneficiary_file = beneficiary_file;
            this.pattern.project = project;
			//patern - end

            return View(this.pattern);
        }
    }
}
