﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//
using SfCms_model;

namespace SfCms_user.Controllers
{
    public class AccountController : Controller
    {
		//pattern
		private class_pattern pattern = new class_pattern();

        [AuthorizeSfCms_beneficiary()]
        public ActionResult Index()
        {
			//menu
			class_menu menu = new class_menu();
			menu.generate_list();

			//beneficiary
			class_beneficiary beneficiary = new class_beneficiary(int.Parse(session.beneficiary_id));
			
			//pattern - begin
			this.pattern.menu = menu;
			//patern - end

			return View(this.pattern);
        }

        [AllowAnonymous]
        public ActionResult LogIn()
        {
            //menu
            class_menu menu = new class_menu();
            menu.generate_list();

            //pattern - begin
            this.pattern.menu = menu;
            //patern - end

            return View(this.pattern);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult LogIn(FormCollection form)
		{
			//menu
			class_menu menu = new class_menu();
			menu.generate_list();

            //form
            string form_login = string.Empty;
            string form_password = string.Empty;

            form_login = form["login"];
            form_password = form["password"];

            //beneficiary
            class_beneficiary beneficiary = new class_beneficiary();
            if (beneficiary.log_in(form_login, form_password))
            {
                session.beneficiary_id = beneficiary.id.ToString();
                return RedirectToAction("Index","Blog");
            }
            else
            {
                ViewBag.LogInInfo = "błędne logowanie";    
            }

			//pattern - begin
			this.pattern.menu = menu;
			//patern - end

			return View(this.pattern);
		}

        [AuthorizeSfCms_beneficiary()]
        public ActionResult LogOff()
        {
			//session
			session.beneficiary_id = null;

            return RedirectToAction("LogIn");
        }
    }
}
