var colRightContent,
    appended = false,
    mq,
    nav_icon,
    menu_mobile,
    shadowVisible = false,
    header;

var isMobile = {
    Android: function () {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function () {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function () {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function () {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function () {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function () {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

$(window).resize(function () {
    appendSections();
    if ($(window).width() >= 1024) {
        nav_icon.removeClass('open');
        menu_mobile.slideUp(200);
    }
});

$(window).scroll(toggleShadow);

$(document).ready(function(){
    colRightContent = $('.col-right'),
    nav_icon = $('#nav-icon'),
    menu_mobile = $('#menu-mobile'),
    mq = window.matchMedia('screen and (max-width:1024px)');

    if (isAndroid(2.2) || isWP7()) {
        $('.nav-icon-old').click(function () {
            $(this).toggleClass('nav-icon-close');
            $('#menu-mobile').slideToggle(200);
        });
        $('#nav-icon').hide();
        $('.nav-icon-old').show();
    }

    nav_icon.click(function (e) {
        $(this).toggleClass('open');
        menu_mobile.slideToggle(200);
        return e.preventDefault();
    });

    $(document).keyup(function (e) {
        if (e.keyCode == 27) {
            nav_icon.removeClass('open');
            menu_mobile.slideUp(200);
        };
    });

    appendSections();

    header = $("#header");
    $(document).keyup(function (e) {
        if (e.keyCode == 27) {
            $("#modal_fb_error").modal('hide');
            $("#modal_password_reset").modal('hide');
            $("#modal_password_reset_info").modal('hide');
            $("#modal_paylane_error").modal('hide');
        }
    });

    reintializeDonorSlider();

    var h = $('#row-mentor .col-right').innerHeight() - $('#row-mentor').innerHeight();
    $('#row-blog .col-right').css('margin-top', h + 'px');

    if (isMobile.any()) {
        applyUnslider();
        applySwipeEvent();
    } else {
        $('.fancybox').fancybox({
            type: 'image',
            openEffect: 'none',
            closeEffect: 'none',
            helpers: {
                title: { type: 'inside' }
            }
        });
    }

    $.fn.modal.Constructor.prototype.setScrollbar = function () {
        var bodyPad = parseInt((this.$body.css('padding-right') || 0), 10);
        if (this.bodyIsOverflowing) {
            header.css('padding-right', this.scrollbarWidth);
            $('.scrollbar-padding').css('padding-right', this.scrollbarWidth);
        }
    }
    $.fn.modal.Constructor.prototype.resetScrollbar = function () {
        header.css('padding-right', 0);
        $('.scrollbar-padding').css('padding-right', 0);
    }
});

function appendSections() {
    if (mq.matches && !appended) {
        $("<div id='row-info' class='scrollbar-padding'><div class='wrapper'></div></div>").insertAfter($('#row-mentor'));
        $(colRightContent[0]).clone(true, true).appendTo($('#row-info').find('.wrapper'));
        $("<div id='row-darczyncy' class='scrollbar-padding'><div class='wrapper'></div></div>").insertAfter($('#row-info'));
        $(colRightContent[1]).clone(true, true).appendTo($('#row-darczyncy').find('.wrapper'));
        $("<div id='row-nagrody' class='scrollbar-padding'><div class='wrapper'></div></div>").insertAfter($('#row-darczyncy'));
        $(colRightContent[2]).clone(true, true).appendTo($('#row-nagrody').find('.wrapper'));
        $('.banner')
        appended = true;
        reintializeDonorSlider();
    } else if (!mq.matches && appended) {
        $('#row-info').remove();
        $('#row-darczyncy').remove();
        $('#row-nagrody').remove();
        appended = false;
        reintializeDonorSlider();
    }
}

function applyUnslider() {
    var slides = $('.gallery-banner').unslider({
        speed: 800,
        delay: false,
        dots: false,
        fluid: true,
        keys: false,
        arrows: true,
        next: "",
        prev: ""
    });
    var data = slides.data('unslider');
    $('.gallery-banner').css({
        'position': 'relative',
        'overflow': 'hidden',
        'margin': '0 auto',
        'min-height': '178px'
    });
    $('.gallery-banner li').css({
        'list-style': 'none'
    });
    $('.gallery-banner ul li').css({
        'float': 'left'
    });
    $('.gallery-banner .thumb').css({
        'height': '160px',
        'width': '160px'
    });
    $('.content-wrapper .content').css({
        'text-align': 'center'
    });
}

function applySwipeEvent() {
    var slides = $('.gallery-banner').find('li'),
        i = 0;
    slides
    .on('swipeleft', function (e) {
        slider.eq(i + 1).addClass('active');
    })
    .on('swiperight', function (e) {
        slides.eq(i - 1).addClass('active');
    });
};

function reintializeDonorSlider() {
    var slider = $('.banner').unslider({
        speed: 800,
        delay: false,
        dots: false,
        fluid: true,
        keys: false
    });
    var data = slider.data('unslider');
    $('.prev').click(function () {
        data.prev();
    });
    $('.next').click(function () {
        data.next();
    });
}

function toggleShadow() {
    if (!shadowVisible) {
        header.addClass("header-shadow");
        shadowVisible = true;
    } else if ($(window).scrollTop() == 0) {
        header.removeClass("header-shadow");
        shadowVisible = false;
    }
}

function isWP7() {
    return navigator.userAgent.toLowerCase().match(/(windows phone os 7)/) == null ? false : true;
}

function isAndroid(version) {
    var re = new RegExp('android ' + version);
    return navigator.userAgent.toLowerCase().match(re) == null ? false : true;
}