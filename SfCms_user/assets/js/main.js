var shadowVisible = false,
    ideaMenuVisible = false,
    percentsSet = false,
    rocketShown = false,
    maxTime = 5000,
    offset = 0,
    mobile;

var sectionsIDs = new Array();
sectionsIDs['home']        = 0;
sectionsIDs['stypendysta'] = 1;
sectionsIDs['program']     = 2;
sectionsIDs['mentor']      = 3;
sectionsIDs['efekt']       = 4;
sectionsIDs['crowdfunding']= 5;
sectionsIDs['biegaj']      = 6;
sectionsIDs['stypendysci'] = 7;
sectionsIDs['sponsorzy']   = 8;

var body,
    header,
    pageWrappers,
    sections,
    sectionsDOM,
    mainMenu_links,
    menu_mobile,
    nav_icon,
    previous_background = "bg-home",
    cloud_logos,
    ideaMenu,
    ideaMenu_links,
    ideaMenu_stypendysta,
    stypendysciDescriptions,
    stypendysciDescriptionsDOM;

var stop_nav = false;
var currentSectionIndex = 0;

var hash = window.location.hash;

$(window).load(function() {
    adjustContentPosition();
    if(hash) {
        header.removeClass("header-shadow");
        shadowVisible = false;
        active_section = $(hash);
        currentSectionIndex = sectionsIDs[hash.split("#")[1]];
        waypointsColorHandler(active_section);
        if(!isMobile())
            waypointsLinkHandler(active_section);
    }
    Waypoint.enableAll();
    $(window).scroll(toggleShadow);
});

$(window).resize(function () {
    adjustContentPosition();
    if ($(window).width() >= 720) {
        body.removeClass('noscroll');
        nav_icon.removeClass('open');
        menu_mobile.slideUp(200);
    }
});

$(document).ready(function() {
    $('.banner').unslider({
        speed: 800,            
        delay: 5000,            
        dots: true,
        fluid: true
    });
    
    $('#amb-' + Math.round(Math.random())).show();
    
    body = $('body'),
    header = $("#header"),
    pageWrappers = $('.page .wrapper'),
    sections = $(".fantom"),
    sectionsDOM = sections.map(function() { return $(this); }),
    mainMenu_links = $("#menu-desktop a"),
    menu_mobile = $('#menu-mobile'),
    nav_icon = $('#nav-icon'),
    cloud_logos = $("#cloud img"),
    ideaMenu = $("#idea-menu"),
    ideaMenu_links = $("#idea-menu a"),
    ideaMenu_stypendysta = $("#idea-menu a[href='#stypendysta']"),
    stypendysciDescriptions = $(".person"),
    stypendysciDescriptionsDOM = stypendysciDescriptions.map(function() { return $(this); });
    
    if(isMobile()) {
        sections.each(function() {
            new Waypoint({
                element: this,
                handler: mobileWaypoints,
                enabled: false,
                group: 'section'
            })
        });
    } else {
        sections.each(function() {
            new Waypoint({
                element: this,
                handler: desktopWaypoints,
                enabled: false,
                group: 'section'
            })
        });
    }
    
    $(".scroll").click(function(e) {
        e.preventDefault();
        var target_top = $("#" + this.href.split("#")[1]).offset().top;
        $('html,body').animate({scrollTop:target_top}, 800, "easeInOutQuad", function() {
            header.removeClass("header-shadow");
            shadowVisible = false;
        });
        body.removeClass('noscroll');
        nav_icon.removeClass('open');
        menu_mobile.slideUp(200);
    });
    $(document).keyup(function(e){
       if(e.keyCode == 27){
            body.removeClass('noscroll');
            nav_icon.removeClass('open');
            menu_mobile.slideUp(200);
       }; 
    });
    
    if (isAndroid(2.2) || isWP7()) {
        $('.nav-icon-old').click(function() {
            $(this).toggleClass('nav-icon-close');
            $('#menu-mobile').slideToggle(200);
        });
        $('#nav-icon').hide();
        $('.nav-icon-old').show();
    }
      
    nav_icon.click(function(e) {
        body.toggleClass('noscroll');
        $(this).toggleClass('open');
        menu_mobile.slideToggle(200);
        return e.preventDefault();
	});
    
    $(".img-container").hover(function() {
        stypendysciDescriptionsDOM[$(this).attr("data-id")].addClass('visible');
    },function() {
        stypendysciDescriptions.removeClass('visible');
    });   
});

function adjustContentPosition() {
    if(style !== undefined)
        style.remove();
    /*if(!isMobile()) {*/
        var h = window.innerHeight - (window.innerWidth < 960 ? 75 : 90);
        h =  h < 400 ? 400 : h;
        style = $("<style />", {
            id  : 'pageHeightStyle',
            type: 'text/css',
            html: ".page {height:" + h + "px}"
        });
        style.appendTo('head');

        pageWrappers.each(function() {
            $this = $(this);
            $image = $('.image', this);
            $image_column = $('.image-column', this);
                
            if($image.length > 0) {
                img_col = $image.prop('naturalHeight') + parseInt($image.css('padding-top')) + $('div.image-column div.button', this).outerHeight(true) + parseInt($image_column.css('padding-top')) + parseInt($image_column.css('padding-bottom'));
                pt = ($this.parent().height() - $this.height()) / 2;
                pt = pt < 0 ? 0 : pt;
                if(img_col > h)
                    $this.css({'height': '100%', 'padding-top': '0'});
                else
                    $this.css({'height': 'auto', 'padding-top': pt + 'px'});
            }
        });
    /*}*/
    Waypoint.refreshAll();
}

function mobileWaypoints(direction) {
    active_section = waypointsGetSection(direction, this);
    waypointsColorHandler(active_section);
}

function desktopWaypoints(direction) {
    active_section = waypointsGetSection(direction, this);    
    waypointsColorHandler(active_section);
    waypointsLinkHandler(active_section);
}

function waypointsGetSection(direction, el) {
    var active_section;
    if(direction === "up") {
        //active_section = sectionsDOM[--currentSectionIndex % sectionsDOM.length];
        active_section = $(el.previous().element);
        header.addClass("header-shadow");
        shadowVisible = true;
    } else {
        //active_section = sectionsDOM[++currentSectionIndex % sectionsDOM.length];
        active_section = $(el.element);
        header.removeClass("header-shadow");
        shadowVisible = false;
    }
    sec_id = active_section.attr("id");
    active_section.removeAttr("id");
    window.location.hash = '#' + sec_id;
    active_section.attr("id", sec_id);
    
    return active_section;
}

function waypointsColorHandler(active_section) {
    sec_id = active_section.attr("id");

    header.removeClass(previous_background);
    header.addClass("bg-" + sec_id);
    previous_background = "bg-" + sec_id;

    cloud_logos.removeClass("visible");
    $("#logo-" + sec_id).addClass("visible"); 
}

function waypointsLinkHandler(active_section) {
    var active_link, 
        sec_id = active_section.attr("id");
    
    if(active_section.attr("data-group") !== undefined) {
        active_link = $('#menu-desktop a[href="#' + active_section.attr("data-group") + '"]');
        ideaMenu_links.removeClass("active");
        $('#idea-menu a[href="#' + sec_id + '"]').addClass("active");
        if(!ideaMenuVisible) {
            ideaMenu.stop(true, true).fadeIn(500);
            ideaMenuVisible = true;
        } 
    } else {
        active_link = $('#menu-desktop a[href="#' + sec_id + '"]');
        if(ideaMenuVisible) {
            ideaMenu_links.removeClass("active");
            ideaMenu.stop(true, true).fadeOut(500);
            ideaMenuVisible = false;
        }
    }
    if(sec_id === "efekt" && !rocketShown) {
        $('#r').addClass('anim');
        $('#static-rocket').addClass('anim');
        rocketShown = true;
        if($("#r").hasClass('anim')){
            setTimeout(function(){$("#r").remove();}, 12000);
        }
    } else if (sec_id === "stypendysci" && !percentsSet) {
        //$('#clue').addClass('visible');
        //setPercents();
        percentsSet = true;
    }
    
    mainMenu_links.removeClass("active");
    active_link.addClass("active");
}

function toggleShadow() {
    if (!shadowVisible) {
        header.addClass("header-shadow");
        shadowVisible = true;
    } else if ($(window).scrollTop() == 0)  {
        header.removeClass("header-shadow");
        shadowVisible = false;
    }
}

function setPercents() {
    var i = 0;
    $('#stypendysci-list li > a').each(function() {
        var d1 = Math.round(percVals[i] / 100 * 360);
        var d2 = Math.min(180, d1);
        var d3 = d2 == 180 ? 180 : d2 - 45;

        $('.r1', this).css(fillRotateStyles(d1));
        $('.r2', this).css(fillRotateStyles(d2));

        $('.m1', this).css(fillRotateStyles(d2));
        $('.m1 .mask-content', this).css(fillRotateStyles(-1 * d2));

        $('.m2', this).css(fillRotateStyles(d3));
        $('.m2 .mask-content', this).css(fillRotateStyles(-1 * d3)); 

        $('.rect', this).css(fillTrainstionTime(percVals[i]));

        $('.img-container span', this).countTo({
            from: 0,
            to: percVals[i],
            speed: (percVals[i] + 1) / 100 * maxTime,
            refreshInterval: 5,
            decimals: 2
        });
        i++;
    });
}

function fillRotateStyles(rotation) {
    return {
        '-webkit-transform' : 'rotate(' + rotation + 'deg)',
        '-moz-transform' : 'rotate(' + rotation + 'deg)',
        '-ms-transform' : 'rotate(' + rotation + 'deg)',
        '-o-transform' : 'rotate(' + rotation + 'deg)',
        'transform' : 'rotate(' + rotation + 'deg)'
    }; 
}

function fillTrainstionTime(value) {
    time = value/100 * maxTime;
    return {
        '-webkit-transition': 'all ease-in-out ' + time + 'ms',
        '-moz-transition': 'all ease-in-out ' + time + 'ms', 
        '-o-transition': 'all ease-in-out ' + time + 'ms',  
        'transition': 'all ease-in-out ' + time + 'ms'
    };
}

function isMobile() {
    if(mobile !== undefined)
        return mobile;
    else
        return mobile = navigator.userAgent.toLowerCase().match(/(iphone|windows phone|android)/) ? true : false;
}

function isWP7() {
    return navigator.userAgent.toLowerCase().match(/(windows phone os 7)/) == null ? false : true;
}
function isAndroid(version) {
    var re = new RegExp('android ' + version);
    return navigator.userAgent.toLowerCase().match(re) == null ? false : true;   
}