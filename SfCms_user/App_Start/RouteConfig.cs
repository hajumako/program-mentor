﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SfCms_user
{
	public class RouteConfig
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //Ws
            routes.IgnoreRoute("Ws/{*pathInfo}");

            //Projekty
			routes.MapRoute(
				name: "Projekty",
				url: "Projekty/{id}/{action}",
				defaults: new { controller = "Projekty", action = "Index", id = UrlParameter.Optional }
			);

            //Default
			routes.MapRoute(
				name: "Default",
				url: "{controller}/{action}/{id}",
				defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
			);
		}
	}
}