﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
//
using SfCms_model;
using System.Xml;
using Newtonsoft.Json;

namespace SfCms_user.Ws
{
    /// <summary>
    /// Summary description for Ws
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Ws : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public XmlDocument AuthorizeSfCms_user()
        {
            XmlDocument wynik = new XmlDocument();

            string[] xml_tag = new string[2];
            xml_tag[0] = "wynik";
            xml_tag[1] = "user_id";

            XmlElement AuthorizeSfCms_user;
            XmlElement row;
            XmlElement xml_element_0;
            XmlElement xml_element_1;
            XmlText text;

            AuthorizeSfCms_user = wynik.CreateElement("AuthorizeSfCms_user");
            wynik.AppendChild(AuthorizeSfCms_user);

            row = wynik.CreateElement("row");
            row.SetAttribute("id", "0");
            AuthorizeSfCms_user.AppendChild(row);

            if (HttpContext.Current.Session["user_id"] != null)
            {
                //0
                xml_element_0 = wynik.CreateElement(xml_tag[0]);
                row.AppendChild(xml_element_0);
                text = wynik.CreateTextNode("true");
                xml_element_0.AppendChild(text);

                //1
                xml_element_1 = wynik.CreateElement(xml_tag[1]);
                row.AppendChild(xml_element_1);
                text = wynik.CreateTextNode(HttpContext.Current.Session["user_id"] as string);
                xml_element_1.AppendChild(text);
            }
            else
            {
                //0
                xml_element_0 = wynik.CreateElement(xml_tag[0]);
                row.AppendChild(xml_element_0);
                text = wynik.CreateTextNode("false");
                xml_element_0.AppendChild(text);

                //1
                xml_element_1 = wynik.CreateElement(xml_tag[1]);
                row.AppendChild(xml_element_1);
                text = wynik.CreateTextNode("0");
                xml_element_1.AppendChild(text);
            }

            return wynik;
        }

        [WebMethod(EnableSession = true)]
        public XmlDocument Paylane_user(string json)
        {
            XmlDocument wynik = new XmlDocument();

            //tool
            class_tool tool = new class_tool();

            //paylane_ws
            class_paylane_ws paylane_ws = JsonConvert.DeserializeObject<class_paylane_ws>(json);

            //ustawiam czy_anonymous
            if (paylane_ws.czy_anonymous)
            {
                HttpContext.Current.Session["czy_anonymous"] = "true";
            }
            else
            {
                HttpContext.Current.Session["czy_anonymous"] = "false";
            }

            //paylane
            int paylane_id = int.Parse(paylane_ws.description);
            class_paylane paylane = new class_paylane(paylane_id);
            paylane.czy_anonymous = paylane_ws.czy_anonymous;
            paylane.update();

            //beneficiary
            class_beneficiary beneficiary = new class_beneficiary();
            beneficiary.generate_list(class_beneficiary.search_type.project_id, paylane.project_id.ToString());

            string paylane_merchant_id = string.Empty;
            string paylane_salt = string.Empty;
            string paylane_hash = string.Empty;
            if (beneficiary.list.Count() > 0)
            {
                paylane_merchant_id = beneficiary.list[0].paylane_merchant_id;
                paylane_salt = beneficiary.list[0].paylane_salt;
                paylane_hash = paylane.sha1(paylane_salt, paylane_ws.description, paylane_ws.amount, paylane_ws.currency, paylane_ws.transaction_type);
            }

            string[] xml_tag = new string[12];

            xml_tag[0] = "merchant_id";
            xml_tag[1] = "description";
            xml_tag[2] = "transaction_description";
            xml_tag[3] = "amount";
            xml_tag[4] = "currency";
            xml_tag[5] = "transaction_type";
            xml_tag[6] = "back_url";
            xml_tag[7] = "hash";
            xml_tag[8] = "language";
            xml_tag[9] = "customer_name";
            xml_tag[10] = "customer_email";
            xml_tag[11] = "customer_country";

            XmlElement Paylane_user;
            XmlElement row;
            XmlElement xml_element_0;
            XmlElement xml_element_1;
            XmlElement xml_element_2;
            XmlElement xml_element_3;
            XmlElement xml_element_4;
            XmlElement xml_element_5;
            XmlElement xml_element_6;
            XmlElement xml_element_7;
            XmlElement xml_element_8;
            XmlElement xml_element_9;
            XmlElement xml_element_10;
            XmlElement xml_element_11;
            XmlText text;

            Paylane_user = wynik.CreateElement("Paylane_user");
            wynik.AppendChild(Paylane_user);

            row = wynik.CreateElement("row");
            row.SetAttribute("id", "0");
            Paylane_user.AppendChild(row);

            //0
            xml_element_0 = wynik.CreateElement(xml_tag[0]);
            row.AppendChild(xml_element_0);
            text = wynik.CreateTextNode(paylane_ws.merchant_id);
            xml_element_0.AppendChild(text);

            //1
            xml_element_1 = wynik.CreateElement(xml_tag[1]);
            row.AppendChild(xml_element_1);
            text = wynik.CreateTextNode(paylane_ws.description);
            xml_element_1.AppendChild(text);

            //2
            xml_element_2 = wynik.CreateElement(xml_tag[2]);
            row.AppendChild(xml_element_2);
            text = wynik.CreateTextNode(paylane_ws.transaction_description);
            xml_element_2.AppendChild(text);

            //3
            xml_element_3 = wynik.CreateElement(xml_tag[3]);
            row.AppendChild(xml_element_3);
            text = wynik.CreateTextNode(tool.format_decimal_dot(paylane_ws.amount));
            xml_element_3.AppendChild(text);

            //4
            xml_element_4 = wynik.CreateElement(xml_tag[4]);
            row.AppendChild(xml_element_4);
            text = wynik.CreateTextNode(paylane_ws.currency);
            xml_element_4.AppendChild(text);

            //5
            xml_element_5 = wynik.CreateElement(xml_tag[5]);
            row.AppendChild(xml_element_5);
            text = wynik.CreateTextNode(paylane_ws.transaction_type);
            xml_element_5.AppendChild(text);

            //6
            xml_element_6 = wynik.CreateElement(xml_tag[6]);
            row.AppendChild(xml_element_6);
            text = wynik.CreateTextNode(paylane_ws.back_url);
            xml_element_6.AppendChild(text);

            //7
            xml_element_7 = wynik.CreateElement(xml_tag[7]);
            row.AppendChild(xml_element_7);
            text = wynik.CreateTextNode(paylane_hash);
            xml_element_7.AppendChild(text);

            //8
            xml_element_8 = wynik.CreateElement(xml_tag[8]);
            row.AppendChild(xml_element_8);
            text = wynik.CreateTextNode(paylane_ws.language);
            xml_element_8.AppendChild(text);

            //9
            xml_element_9 = wynik.CreateElement(xml_tag[9]);
            row.AppendChild(xml_element_9);
            text = wynik.CreateTextNode(paylane_ws.customer_name);
            xml_element_9.AppendChild(text);

            //10
            xml_element_10 = wynik.CreateElement(xml_tag[10]);
            row.AppendChild(xml_element_10);
            text = wynik.CreateTextNode(paylane_ws.customer_email);
            xml_element_10.AppendChild(text);

            //11
            xml_element_11 = wynik.CreateElement(xml_tag[11]);
            row.AppendChild(xml_element_11);
            text = wynik.CreateTextNode(paylane_ws.customer_country);
            xml_element_11.AppendChild(text);

            return wynik;
        }
    }
}
