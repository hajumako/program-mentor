﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SfCms_user
{
    public static class StaticMethods
    {
        public static string getBaseUrl(string urlContent) {
            return string.Format("{0}://{1}{2}", HttpContext.Current.Request.Url.Scheme, HttpContext.Current.Request.Url.Authority, urlContent);
        }
    }
}