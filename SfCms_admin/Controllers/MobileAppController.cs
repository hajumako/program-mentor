﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//
using SfCms_model;
using System.IO;
using System.Web.UI;
using ImageResizer;
using System.Security.Cryptography;
using System.Text;

namespace SfCms_admin.Controllers
{
    [AuthorizeSfCms_admin()]
    public class MobileAppController : Controller
    {
        //pattern
        private class_pattern pattern = new class_pattern();
        //settings
        private class_settings settings = new class_settings();
        //validation
        private class_validation validation = new class_validation();
        //menu
        private class_menu menu = new class_menu();
        //dictionary
        private class_dictionary dictionary = new class_dictionary();

        #region Sponsor

        public ActionResult Index()
        {
            class_sponsor sponsor = new class_sponsor();
            sponsor.generate_list_with_additional_data(class_sponsor.search_type.order_by_sequence, string.Empty);

            //pattern - begin
            this.pattern.settings = this.settings;
            this.pattern.menu = this.menu;
            this.pattern.dictionary = this.dictionary;
            this.pattern.sponsor = sponsor;
            //patern - end

            return View(this.pattern);
        }

        public ActionResult AddSponsorForm()
        {
            class_sponsor sponsor = new class_sponsor();
            sponsor.generate_list(class_sponsor.search_type.order_by_sequence, String.Empty);

            //pattern - begin
            this.pattern.settings = this.settings;
            this.pattern.menu = this.menu;
            this.pattern.dictionary = this.dictionary;
            this.pattern.start_seq = sponsor.list[0].sequence + 1;
            //patern - end

            return View(this.pattern);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddSponsor(FormCollection form, HttpPostedFileBase file)
        {
            class_sql sql = new class_sql();

            //form
            bool form_active = false;
            int form_sequence = 0;
            string form_color1 = string.Empty;
            string form_color2 = string.Empty;
            string form_name = string.Empty;
            decimal form_amount = 0;
            decimal form_rate = 0;

            if (form["active"] == "true")
            {
                form_active = true;
            }
            form_sequence = int.Parse(form["sequence"]);
            form_color1 = form["color1"];
            form_color2 = form["color2"];
            form_name = form["name"];
            form_amount = decimal.Parse(form["amount"]);
            form_rate = decimal.Parse(form["rate"]);

            string fileName = "";

            if (file != null)
            {
                try
                {
                    string savePath = Server.MapPath("/sponsor_logos/");

                    HashAlgorithm algorithm = SHA1.Create();
                    byte[] buffer_arr = algorithm.ComputeHash(Encoding.UTF8.GetBytes((int)DateTime.Now.Ticks + file.FileName));

                    StringBuilder string_builder = new StringBuilder();
                    foreach (byte buffer in buffer_arr)
                    {
                        string_builder.Append(buffer.ToString("X2"));
                    }
                    string[] ext_arr = file.FileName.Split('.');
                    fileName = string_builder.ToString().ToLower() + '.' + ext_arr[ext_arr.Length - 1];
                    string pathToCheck = savePath + fileName;
                    string tempfileName = "";
                    if (System.IO.File.Exists(pathToCheck))
                    {
                        int counter = 2;
                        while (System.IO.File.Exists(pathToCheck))
                        {
                            tempfileName = counter.ToString() + fileName;
                            pathToCheck = savePath + tempfileName;
                            counter++;
                        }

                        fileName = tempfileName;
                    }
                    else
                    {
                        // Notify the user that the file was saved successfully.
                    }
                    savePath += fileName;
                    file.SaveAs(savePath);
                }
                catch (Exception e)
                {

                }
            }

            //sponsor
            class_sponsor sponsor = new class_sponsor(0);
            sponsor.active = form_active;
            sponsor.sequence = form_sequence;
            sponsor.color1 = form_color1;
            sponsor.color2 = form_color2;
            sponsor.name = form_name;
            sponsor.amount = form_amount;
            sponsor.rate = form_rate;
            sponsor.logo_link = fileName;
            sponsor.insert();

            return RedirectToAction("Index");
        }
 
        public ActionResult EditSponsorForm(bool error = false)
        {
            if (!this.validation.czy_int(RouteData.Values["id"]))
            {
                return RedirectToAction("HttpError404", "Error");
            }
            else
            {
                int id = int.Parse(RouteData.Values["id"].ToString());

                class_sponsor sponsor = new class_sponsor(id);

                //pattern - begin
                this.pattern.settings = this.settings;
                this.pattern.menu = this.menu;
                this.pattern.dictionary = this.dictionary;
                this.pattern.sponsor = sponsor;
                this.pattern.err = error;

                //patern - end

                return View(this.pattern);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditSponsor(FormCollection form, HttpPostedFileBase file)
        {
            class_sql sql = new class_sql();

            //form
            bool form_active = false;
            int form_sequence = 0;
            int form_id = 0;
            string form_color1 = string.Empty;
            string form_color2 = string.Empty;
            string form_name = string.Empty;
            decimal form_amount = 0;
            decimal form_rate = 0;

            if (form["active"] == "true")
            {
                form_active = true;
            }
            form_id = int.Parse(form["id"]);
            form_sequence = int.Parse(form["sequence"]);
            form_color1 = form["color1"];
            form_color2 = form["color2"];
            form_name = form["name"];
            form_amount = decimal.Parse(form["amount"]);
            form_rate = decimal.Parse(form["rate"]);

            class_sponsor sponsor_check = new class_sponsor();
            sponsor_check.generate_list(class_sponsor.search_type.search_by_seqence, form_sequence.ToString());
            if (sponsor_check.list.Count > 0 && sponsor_check.list[0].id != form_id)
            {
                return RedirectToAction("EditSponsorForm", new { id = form_id.ToString(), error = true });
            }
            else
            {
                //sponsor
                class_sponsor sponsor = new class_sponsor(form_id);
                sponsor.active = form_active;
                sponsor.sequence = form_sequence;
                sponsor.color1 = form_color1;
                sponsor.color2 = form_color2;
                sponsor.name = form_name;
                sponsor.amount = form_amount;
                sponsor.rate = form_rate;
           
                if (file != null)
                {
                    try
                    {
                        string savePath = Server.MapPath("/sponsor_logos/");
                        string[] up_file_arr = file.FileName.Split('.');
                        string[] db_file_arr = sponsor.logo_link.Split('.');
                        string fileName = db_file_arr[0] + "." + up_file_arr[up_file_arr.Length - 1];
                        string pathToCheck = savePath + fileName;
                        savePath += fileName;
                        file.SaveAs(savePath);
                        sponsor.logo_link = fileName;
                    }
                    catch (Exception e)
                    {

                    }
                }

                sponsor.update();

                return RedirectToAction("Index");
            }
        }

        public ActionResult DeleteSponsorForm(bool error = false)
        {
            if (!this.validation.czy_int(RouteData.Values["id"]))
            {
                return RedirectToAction("HttpError404", "Error");
            }
            else
            {
                int id = int.Parse(RouteData.Values["id"].ToString());

                class_sponsor_extended sponsor_extended = new class_sponsor_extended(id);

                //pattern - begin
                this.pattern.settings = this.settings;
                this.pattern.menu = this.menu;
                this.pattern.dictionary = this.dictionary;
                this.pattern.sponsor_extended = sponsor_extended;
                this.pattern.err = error;
                //patern - end

                return View(this.pattern);
            }
        }

        [HttpPost]
        public ActionResult DeleteSponsor(FormCollection form)
        {
            //form
            int form_id = 0;

            form_id = int.Parse(form["id"].ToString());

            //beneficiary
            class_sponsor_extended sponsor_extended = new class_sponsor_extended(form_id);

            if (!sponsor_extended.current_sponsor)
            {
                sponsor_extended.delete();
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("DeleteSponsorForm", new { id = form_id.ToString(), error = true });
            }
        }

        #endregion Sponsor

        #region Runs

        public ActionResult Runs(bool hide_nulls = false)
        {
            class_activity activity = new class_activity();
            activity.generate_list_with_additional_data(class_activity.search_type.all, string.Empty);

            //pattern - begin
            this.pattern.settings = this.settings;
            this.pattern.menu = this.menu;
            this.pattern.dictionary = this.dictionary;
            this.pattern.activity = activity;
            this.pattern.hide_nulls = hide_nulls;
            //patern - end

            return View(this.pattern);
        }

        #endregion Runs

    }
}
