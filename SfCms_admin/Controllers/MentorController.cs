﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//
using SfCms_model;
using System.IO;
using System.Web.UI;
using ImageResizer;

namespace SfCms_admin.Controllers
{
    [AuthorizeSfCms_admin()]
    public class MentorController : Controller
    {
        //pattern
        private class_pattern pattern = new class_pattern();
        //settings
        private class_settings settings = new class_settings();
        //validation
        private class_validation validation = new class_validation();
        //menu
        private class_menu menu = new class_menu();
        //dictionary
        private class_dictionary dictionary = new class_dictionary();
        //mentor_dictionary
        private class_mentor_dictionary mentor_dictionary = new class_mentor_dictionary();

        public ActionResult Index()
        {
            //mentor
            class_mentor mentor = new class_mentor();
            mentor.generate_list(class_mentor.search_type.all, string.Empty);

            //mentor_file
            class_mentor_file mentor_file = new class_mentor_file();

            //project
            class_project project = new class_project();

            //pattern - begin
            this.pattern.settings = this.settings;
            this.pattern.menu = this.menu;
            this.pattern.dictionary = this.dictionary;
            this.pattern.mentor_dictionary = this.mentor_dictionary;
            this.pattern.mentor = mentor;
            this.pattern.mentor_file = mentor_file;
            this.pattern.project = project;
            //patern - end

            return View(this.pattern);
        }

        public ActionResult AddAsk()
        {
            //project
            class_project project = new class_project();
            project.generate_list(class_project.search_type.all, string.Empty);

            //pattern - begin
            this.pattern.settings = this.settings;
            this.pattern.menu = this.menu;
            this.pattern.dictionary = this.dictionary;
            this.pattern.mentor_dictionary = this.mentor_dictionary;
            this.pattern.project = project;
            //patern - end

            return View(this.pattern);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Add(FormCollection form, HttpPostedFileBase file)
        {
            class_sql sql = new class_sql();

            //form
            bool form_active = false;
            int form_project_id = 0;
            string form_name = string.Empty;
            string form_surname = string.Empty;
            string form_content = string.Empty;

            if (form["active"] == "true")
            {
                form_active = true;
            }
            form_project_id = int.Parse(form["project_id"]);
            form_name = form["name"];
            form_surname = form["surname"];
            form_content = form["content"];

            //mentor
            class_mentor mentor = new class_mentor(0);
            mentor.active = form_active;
            mentor.project_id = form_project_id;
            mentor.name = form_name;
            mentor.surname = form_surname;
            mentor.content = form_content;
            mentor.insert();

            //file
            if (file != null)
            {
                //mentor_file
                class_mentor_file mentor_file = new class_mentor_file();

                //image
                class_image image = new class_image(file.ContentType);

                //version
                foreach (var suffix in image.versions.Keys)
                {
                    file.InputStream.Seek(0, SeekOrigin.Begin);
                    image.stream_out = new System.IO.MemoryStream();
                    ImageBuilder.Current.Build(new ImageJob(file.InputStream, image.stream_out, new Instructions(image.versions[suffix]), false, true));
                    image.stream_out.Seek(0, SeekOrigin.Begin);

                    if (suffix == image.large)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        mentor_file.content_large = image.file_content;
                    }
                    else if (suffix == image.medium)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        mentor_file.content_medium = image.file_content;
                    }
                    else if (suffix == image.small)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        mentor_file.content_small = image.file_content;
                    }
                }

                mentor_file.mentor_id = mentor.id;
                mentor_file.content_type = file.ContentType;
                mentor_file.name = file.FileName;
                mentor_file.insert();
            }

            return RedirectToAction("Index");
        }

        public ActionResult EditAsk()
        {
            //id
            if (!this.validation.czy_int(RouteData.Values["id"]))
            {
                return RedirectToAction("HttpError404", "Error");
            }
            else
            {
                int id = int.Parse(RouteData.Values["id"].ToString());

                //mentor
                class_mentor mentor = new class_mentor(id);

                //mentor_file
                class_mentor_file mentor_file = new class_mentor_file(class_mentor_file.search_type.mentor_id, class_mentor_file.image_type.empty, mentor.id.ToString());

                //project
                class_project project = new class_project();
                project.generate_list(class_project.search_type.all, string.Empty);

                //pattern - begin
                this.pattern.settings = this.settings;
                this.pattern.menu = this.menu;
                this.pattern.dictionary = this.dictionary;
                this.pattern.mentor_dictionary = this.mentor_dictionary;
                this.pattern.mentor = mentor;
                this.pattern.mentor_file = mentor_file;
                this.pattern.project = project;
                //patern - end

                return View(this.pattern);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(FormCollection form, HttpPostedFileBase file)
        {
            class_sql sql = new class_sql();

            //form
            int form_id = 0;
            bool form_active = false;
            int form_project_id = 0;
            string form_name = string.Empty;
            string form_surname = string.Empty;
            string form_content = string.Empty;

            form_id = int.Parse(form["id"].ToString());
            if (form["active"] == "true")
            {
                form_active = true;
            }
            form_project_id = int.Parse(form["project_id"]);
            form_name = form["name"];
            form_surname = form["surname"];
            form_content = form["content"];

            //mentor
            class_mentor mentor = new class_mentor(form_id);
            mentor.active = form_active;
            mentor.project_id = form_project_id;
            mentor.name = form_name;
            mentor.surname = form_surname;
            mentor.content = form_content;
            mentor.update();

            //file
            if (file != null)
            {
                //mentor_file
                class_mentor_file mentor_file = new class_mentor_file(class_mentor_file.search_type.mentor_id, class_mentor_file.image_type.empty, mentor.id.ToString());
                mentor_file.delete();

                //image
                class_image image = new class_image(file.ContentType);

                //version
                foreach (var suffix in image.versions.Keys)
                {
                    file.InputStream.Seek(0, SeekOrigin.Begin);
                    image.stream_out = new System.IO.MemoryStream();
                    ImageBuilder.Current.Build(new ImageJob(file.InputStream, image.stream_out, new Instructions(image.versions[suffix]), false, true));
                    image.stream_out.Seek(0, SeekOrigin.Begin);

                    if (suffix == image.large)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        mentor_file.content_large = image.file_content;
                    }
                    else if (suffix == image.medium)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        mentor_file.content_medium = image.file_content;
                    }
                    else if (suffix == image.small)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        mentor_file.content_small = image.file_content;
                    }
                }

                mentor_file.mentor_id = mentor.id;
                mentor_file.content_type = file.ContentType;
                mentor_file.name = file.FileName;
                mentor_file.insert();
            }

            return RedirectToAction("Index");
        }

        public ActionResult Show()
        {
            //id
            if (!this.validation.czy_int(RouteData.Values["id"]))
            {
                return RedirectToAction("HttpError404", "Error");
            }
            else
            {
                int id = int.Parse(RouteData.Values["id"].ToString());

                //mentor
                class_mentor mentor = new class_mentor(id);

                //mentor_file
                class_mentor_file mentor_file = new class_mentor_file(class_mentor_file.search_type.mentor_id, class_mentor_file.image_type.empty, mentor.id.ToString());

                //project
                class_project project = new class_project(mentor.project_id);

                //pattern - begin
                this.pattern.settings = this.settings;
                this.pattern.menu = this.menu;
                this.pattern.dictionary = this.dictionary;
                this.pattern.mentor_dictionary = this.mentor_dictionary;
                this.pattern.mentor = mentor;
                this.pattern.mentor_file = mentor_file;
                this.pattern.project = project;
                //patern - end

                return View(this.pattern);
            }
        }

        public ActionResult DeleteAsk()
        {
            //id
            if (!this.validation.czy_int(RouteData.Values["id"]))
            {
                return RedirectToAction("HttpError404", "Error");
            }
            else
            {
                int id = int.Parse(RouteData.Values["id"].ToString());

                //mentor
                class_mentor mentor = new class_mentor(id);

                //mentor_file
                class_mentor_file mentor_file = new class_mentor_file(class_mentor_file.search_type.mentor_id, class_mentor_file.image_type.empty, mentor.id.ToString());

                //project
                class_project project = new class_project(mentor.project_id);

                //pattern - begin
                this.pattern.settings = this.settings;
                this.pattern.menu = this.menu;
                this.pattern.dictionary = this.dictionary;
                this.pattern.mentor_dictionary = this.mentor_dictionary;
                this.pattern.mentor = mentor;
                this.pattern.mentor_file = mentor_file;
                this.pattern.project = project;
                //patern - end

                return View(this.pattern);
            }
        }

        [HttpPost]
        public ActionResult Delete(FormCollection form)
        {
            //form
            int form_id = 0;

            form_id = int.Parse(form["id"].ToString());

            //mentor
            class_mentor mentor = new class_mentor(form_id);
            mentor.delete();

            //mentor_file
            class_mentor_file mentor_file = new class_mentor_file(class_mentor_file.search_type.mentor_id, class_mentor_file.image_type.empty, mentor.id.ToString());
            mentor_file.delete();

            return RedirectToAction("Index");
        }
    }
}

