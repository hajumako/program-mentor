﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//
using SfCms_model;
using System.IO;
using System.Web.UI;

namespace SfCms_admin.Controllers
{
	[AuthorizeSfCms_admin()]
    public class HomeController : Controller
    {
		//pattern
		private class_pattern pattern = new class_pattern();
		//settings
		private class_settings settings = new class_settings(); 
		//validation
		private class_validation validation = new class_validation();
		//menu
		private class_menu menu = new class_menu();
		//dictionary
		private class_dictionary dictionary = new class_dictionary();
		
        public ActionResult Index()
        {
            //pattern - begin
			this.pattern.settings = this.settings;
			this.pattern.menu = this.menu;
			this.pattern.dictionary = this.dictionary;
            //patern - end

			return View(this.pattern);
        }
    }
}
