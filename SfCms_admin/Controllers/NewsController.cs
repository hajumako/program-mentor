﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//
using SfCms_model;
using System.IO;
using System.Web.UI;
using ImageResizer;

namespace SfCms_admin.Controllers
{
	[AuthorizeSfCms_admin()]
    public class NewsController : Controller
    {
		//pattern
		private class_pattern pattern = new class_pattern();
		//settings
		private class_settings settings = new class_settings();
        //validation
        private class_validation validation = new class_validation();
		//menu
		private class_menu menu = new class_menu();
		//dictionary
		private class_dictionary dictionary = new class_dictionary();
		//news_dictionary
		private class_news_dictionary news_dictionary = new class_news_dictionary();

        public ActionResult Index()
        {
			//news
			class_news news = new class_news();
			news.generate_list(class_news.search_type.all, string.Empty);

			//pattern - begin
			this.pattern.settings = this.settings;
			this.pattern.menu = this.menu;
			this.pattern.dictionary = this.dictionary;
			this.pattern.news_dictionary = this.news_dictionary;
			this.pattern.news = news;
			//patern - end

			return View(this.pattern);
        }

		public ActionResult AddAsk()
		{
            //s
            class_s s = new class_s(class_s.dictionary.s_news);

			//pattern - begin
			this.pattern.settings = this.settings;
			this.pattern.menu = this.menu;
			this.pattern.dictionary = this.dictionary;
			this.pattern.news_dictionary = this.news_dictionary;
            this.pattern.s = s;
			//patern - end

			return View(this.pattern);
		}

		[HttpPost]
		[ValidateInput(false)]
        public ActionResult Add(FormCollection form, HttpPostedFileBase file)
		{
			class_sql sql = new class_sql();

			//form
			bool form_active = false;
			string form_title = string.Empty;
			string form_content = string.Empty;
			DateTime form_date_user = sql.data_default;
            int form_s_news_id = 0;
			string form_logo_link = string.Empty;

			if (form["active"] == "true")
			{
				form_active = true;
			}
			form_title = form["title"];
			form_content = form["content"];
			form_date_user = Convert.ToDateTime(form["date_user"]);
            form_s_news_id = int.Parse(form["s_news_id"]);
			form_logo_link = form["logo_link"];

			//news
			class_news news = new class_news(0);
			news.active = form_active;
			news.title = form_title;
			news.content = form_content;
			news.date_user = form_date_user;
            news.s_news_id = form_s_news_id;
			news.logo_link = form_logo_link;
			news.insert();

			//file
			if (file != null)
			{
                //news_file
                class_news_file news_file = new class_news_file();

                //image
                class_image image = new class_image(file.ContentType);

                //version
                foreach (var suffix in image.versions.Keys)
                {
                    file.InputStream.Seek(0, SeekOrigin.Begin);
                    image.stream_out = new System.IO.MemoryStream();
                    ImageBuilder.Current.Build(new ImageJob(file.InputStream, image.stream_out, new Instructions(image.versions[suffix]), false, true));
                    image.stream_out.Seek(0, SeekOrigin.Begin);

                    if (suffix == image.large)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        news_file.content_large = image.file_content;
                    }
                    else if (suffix == image.medium)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        news_file.content_medium = image.file_content;
                    }
                    else if (suffix == image.small)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        news_file.content_small = image.file_content;
                    }
                }

				news_file.news_id = news.id;
				news_file.content_type = file.ContentType;
				news_file.name = file.FileName;
				news_file.insert();
			}

            return RedirectToAction("Index");
		}

        public ActionResult EditAsk()
        {
			//id
            if (!this.validation.czy_int(RouteData.Values["id"]))
            {
                return RedirectToAction("HttpError404", "Error");
            }
            else
            {
				int id = int.Parse(RouteData.Values["id"].ToString());

				//news
				class_news news = new class_news(id);

				//news_file
				class_news_file news_file = new class_news_file(class_news_file.search_type.news_id, class_news_file.image_type.empty, news.id.ToString());

                //s
                class_s s = new class_s(class_s.dictionary.s_news);

				//pattern - begin
				this.pattern.settings = this.settings;
				this.pattern.menu = this.menu;
				this.pattern.dictionary = this.dictionary;
				this.pattern.news_dictionary = this.news_dictionary;
				this.pattern.news = news;
				this.pattern.news_file = news_file;
                this.pattern.s = s;
				//patern - end

				return View(this.pattern);
            }
        }

        [HttpPost]
		[ValidateInput(false)]
        public ActionResult Edit(FormCollection form, HttpPostedFileBase file)
        {
			class_sql sql = new class_sql();

			//form
			int form_id = 0;
			bool form_active = false;
			string form_title = string.Empty;
			string form_content = string.Empty;
			DateTime form_date_user = sql.data_default;
            int form_s_news_id = 0;
			string form_logo_link = string.Empty;

			form_id = int.Parse(form["id"].ToString());
			if (form["active"] == "true")
			{
				form_active = true;
			}
			form_title = form["title"];
			form_content = form["content"];
			form_date_user = Convert.ToDateTime(form["date_user"]);
            form_s_news_id = int.Parse(form["s_news_id"]);
			form_logo_link = form["logo_link"];

			//news
			class_news news = new class_news(form_id);
			news.active = form_active;
			news.title = form_title;
			news.content = form_content;
			news.date_user = form_date_user;
            news.s_news_id = form_s_news_id;
			news.logo_link = form_logo_link;
			news.update();

			//file
			if (file != null)
			{
                //news_file
                class_news_file news_file = new class_news_file(class_news_file.search_type.news_id, class_news_file.image_type.empty, news.id.ToString());
                news_file.delete();

                //image
                class_image image = new class_image(file.ContentType);

                //version
                foreach (var suffix in image.versions.Keys)
                {
                    file.InputStream.Seek(0, SeekOrigin.Begin);
                    image.stream_out = new System.IO.MemoryStream();
                    ImageBuilder.Current.Build(new ImageJob(file.InputStream, image.stream_out, new Instructions(image.versions[suffix]), false, true));
                    image.stream_out.Seek(0, SeekOrigin.Begin);

                    if (suffix == image.large)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        news_file.content_large = image.file_content;
                    }
                    else if (suffix == image.medium)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        news_file.content_medium = image.file_content;
                    }
                    else if (suffix == image.small)
                    {
                        image.file_size = Convert.ToInt32(image.stream_out.Length.ToString());
                        image.file_content = new byte[image.file_size];
                        image.stream_out.Read(image.file_content, 0, image.file_size);

                        news_file.content_small = image.file_content;
                    }
                }

				news_file.news_id = news.id;
				news_file.content_type = file.ContentType;
				news_file.name = file.FileName;
				news_file.insert();
			}

            return RedirectToAction("Index");
        }

        public ActionResult Show()
        {
			//id
            if (!this.validation.czy_int(RouteData.Values["id"]))
            {
                return RedirectToAction("HttpError404", "Error");
            }
            else
            {
                int id = int.Parse(RouteData.Values["id"].ToString());

                //news
                class_news news = new class_news(id);

                //news_file
                class_news_file news_file = new class_news_file(class_news_file.search_type.news_id, class_news_file.image_type.empty, news.id.ToString());

                //pattern - begin
				this.pattern.settings = this.settings;
				this.pattern.menu = this.menu;
				this.pattern.dictionary = this.dictionary;
				this.pattern.news_dictionary = this.news_dictionary;
				this.pattern.news = news;
				this.pattern.news_file = news_file;
                //patern - end

                return View(this.pattern);
            }
        }

        public ActionResult DeleteAsk()
        {
			//id
            if (!this.validation.czy_int(RouteData.Values["id"]))
            {
                return RedirectToAction("HttpError404", "Error");
            }
            else
            {
                int id = int.Parse(RouteData.Values["id"].ToString());

                //news
                class_news news = new class_news(id);

                //news_file
                class_news_file news_file = new class_news_file(class_news_file.search_type.news_id, class_news_file.image_type.empty, news.id.ToString());

                //pattern - begin
				this.pattern.settings = this.settings;
				this.pattern.menu = this.menu;
				this.pattern.dictionary = this.dictionary;
				this.pattern.news_dictionary = this.news_dictionary;
                this.pattern.news = news;
                this.pattern.news_file = news_file;
                //patern - end

                return View(this.pattern);
            }
        }

        [HttpPost]
        public ActionResult Delete(FormCollection form)
        {
			//form
			int form_id = 0;

			form_id = int.Parse(form["id"].ToString());

			//news
			class_news news = new class_news(form_id);
			news.delete();

			//news_file
			class_news_file news_file = new class_news_file(class_news_file.search_type.news_id, class_news_file.image_type.empty, news.id.ToString());
			news_file.delete();

            return RedirectToAction("Index");
        }
    }
}
