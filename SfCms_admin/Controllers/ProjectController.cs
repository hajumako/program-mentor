﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//
using SfCms_model;
using System.IO;
using System.Web.UI;

namespace SfCms_admin.Controllers
{
	[AuthorizeSfCms_admin()]
    public class ProjectController : Controller
    {
		//pattern
		private class_pattern pattern = new class_pattern();
		//settings
		private class_settings settings = new class_settings();
		//validation
		private class_validation validation = new class_validation();
		//menu
		private class_menu menu = new class_menu();
		//dictionary
		private class_dictionary dictionary = new class_dictionary();
		//project_dictionary
		private class_project_dictionary project_dictionary = new class_project_dictionary();
		//beneficiary_dictionary
		private class_beneficiary_dictionary beneficiary_dictionary = new class_beneficiary_dictionary();
        //mentor_dictionary
        private class_mentor_dictionary mentor_dictionary = new class_mentor_dictionary();
		//tool
		private class_tool tool = new class_tool();

		public ActionResult Index()
		{
			//project
			class_project project = new class_project();
			project.generate_list(class_project.search_type.all, string.Empty);

			//pattern - begin
			this.pattern.settings = this.settings;
			this.pattern.menu = this.menu;
			this.pattern.dictionary = this.dictionary;
			this.pattern.project_dictionary = this.project_dictionary;
			this.pattern.project = project;
			//patern - end

			return View(this.pattern);
		}

		public ActionResult AddAsk()
		{
			//pattern - begin
			this.pattern.settings = this.settings;
			this.pattern.menu = this.menu;
			this.pattern.dictionary = this.dictionary;
			this.pattern.project_dictionary = this.project_dictionary;

            class_s_brick s_bricks = new class_s_brick();
            s_bricks.get_all_data();

            this.pattern.s_brick = s_bricks;
			//patern - end

			return View(this.pattern);
		}

		[HttpPost]
		[ValidateInput(false)]
		public ActionResult Add(FormCollection form, HttpPostedFileBase file)
		{
			class_sql sql = new class_sql();

			//form
			bool form_active = false;
			string form_name = string.Empty;
			string form_title = string.Empty;
			string form_content = string.Empty;
			DateTime form_date_start = sql.data_default;
			DateTime form_date_stop = sql.data_default;
			decimal form_amount = 0;
			
			if (form["active"] == "true")
			{
				form_active = true;
			}
			form_name = form["name"];
			form_title = form["title"];
			form_content = form["content"];
			form_date_start = Convert.ToDateTime(form["date_start"]);
			form_date_stop = Convert.ToDateTime(form["date_stop"]);
			form_amount = decimal.Parse(form["amount"]);

			//project
			class_project project = new class_project(0);
			project.active = form_active;
			project.name = form_name;
			project.title = form_title;
			project.content = form_content;
			project.date_start = form_date_start;
			project.date_stop = form_date_stop;
			project.amount = form_amount;
			project.insert();

            class_brick brick;
            class_s_brick s_bricks = new class_s_brick();
            s_bricks.get_all_data();

            for (int i = 0; i < s_bricks.list.Count; i++)
            {
                brick = new class_brick();
                brick.active = true;
                brick.project_id = project.id;
                brick.s_brick_id = int.Parse(form["s_brick-" + i + "-id"].ToString());
                brick.content = form["brick-" + i + "-content"];
                brick.insert();
            }

			return RedirectToAction("Index");
		}

		public ActionResult EditAsk()
		{
			//id
			if (!this.validation.czy_int(RouteData.Values["id"]))
			{
				return RedirectToAction("HttpError404", "Error");
			}
			else
			{
				int id = int.Parse(RouteData.Values["id"].ToString());

				//project
				class_project project = new class_project(id);
                project.generate_list_brick();

				//pattern - begin
				this.pattern.settings = this.settings;
				this.pattern.menu = this.menu;
				this.pattern.dictionary = this.dictionary;
				this.pattern.project_dictionary = this.project_dictionary;
				this.pattern.project = project;
				this.pattern.tool = this.tool;
                class_s_brick s_bricks = new class_s_brick();
                s_bricks.get_all_data();

                this.pattern.s_brick = s_bricks;
				//patern - end

				return View(this.pattern);
			}
		}

		[HttpPost]
		[ValidateInput(false)]
		public ActionResult Edit(FormCollection form, HttpPostedFileBase file)
		{
			class_sql sql = new class_sql();

			//form
			int form_id = 0;
			bool form_active = false;
			string form_name = string.Empty;
			string form_title = string.Empty;
			string form_content = string.Empty;
			DateTime form_date_start = sql.data_default;
			DateTime form_date_stop = sql.data_default;
			decimal form_amount = 0;

			form_id = int.Parse(form["id"].ToString());
			if (form["active"] == "true")
			{
				form_active = true;
			}
			form_name = form["name"];
			form_title = form["title"];
			form_content = form["content"];
			form_date_start = Convert.ToDateTime(form["date_start"]);
			form_date_stop = Convert.ToDateTime(form["date_stop"]);
			form_amount = decimal.Parse(form["amount"]);

			//project
			class_project project = new class_project(form_id);
			project.active = form_active;
			project.name = form_name;
			project.title = form_title;
			project.content = form_content;
			project.date_start = form_date_start;
			project.date_stop = form_date_stop;
			project.amount = form_amount;
			project.update();

            int brick_id;
            class_brick brick;
            class_s_brick s_bricks = new class_s_brick();
            s_bricks.get_all_data();

            for (int i = 0; i < s_bricks.list.Count; i++)
            {
                brick_id = int.Parse(form["brick-"+ i + "-id"].ToString());
                brick = new class_brick(brick_id);
                brick.content = form["brick-" + i + "-content"];
                brick.update();
            }

			return RedirectToAction("Index");
		}

		public ActionResult Show()
		{
			//id
			if (!this.validation.czy_int(RouteData.Values["id"]))
			{
				return RedirectToAction("HttpError404", "Error");
			}
			else
			{
				int id = int.Parse(RouteData.Values["id"].ToString());

				//project
				class_project project = new class_project(id);
				project.generate_list_beneficiary();
                project.generate_list_mentor();

				//beneficiary_file
				class_beneficiary_file beneficiary_file = new class_beneficiary_file();

                //mentor_file
                class_mentor_file mentor_file = new class_mentor_file();

				//pattern - begin
				this.pattern.settings = this.settings;
				this.pattern.menu = this.menu;
				this.pattern.dictionary = this.dictionary;
				this.pattern.project_dictionary = this.project_dictionary;
				this.pattern.beneficiary_dictionary = this.beneficiary_dictionary;
                this.pattern.mentor_dictionary = this.mentor_dictionary;
				this.pattern.beneficiary_file = beneficiary_file;
                this.pattern.mentor_file = mentor_file;
				this.pattern.project = project;
				this.pattern.tool = this.tool;
				//patern - end

				return View(this.pattern);
			}
		}

		public ActionResult DeleteAsk()
		{
			//id
			if (!this.validation.czy_int(RouteData.Values["id"]))
			{
				return RedirectToAction("HttpError404", "Error");
			}
			else
			{
				int id = int.Parse(RouteData.Values["id"].ToString());

				//project
				class_project project = new class_project(id);
				project.generate_list_beneficiary();
                project.generate_list_mentor();

				//beneficiary_file
				class_beneficiary_file beneficiary_file = new class_beneficiary_file();

                //mentor_file
                class_mentor_file mentor_file = new class_mentor_file();

				//pattern - begin
				this.pattern.settings = this.settings;
				this.pattern.menu = this.menu;
				this.pattern.dictionary = this.dictionary;
				this.pattern.project_dictionary = this.project_dictionary;
				this.pattern.beneficiary_dictionary = this.beneficiary_dictionary;
                this.pattern.mentor_dictionary = this.mentor_dictionary;
				this.pattern.beneficiary_file = beneficiary_file;
                this.pattern.mentor_file = mentor_file;
				this.pattern.project = project;
				this.pattern.tool = this.tool;
				//patern - end

				return View(this.pattern);
			}
		}

		[HttpPost]
		public ActionResult Delete(FormCollection form)
		{
			//form
			int form_id = 0;

			form_id = int.Parse(form["id"].ToString());

			//project
			class_project project = new class_project(form_id);
			project.delete();

			return RedirectToAction("Index");
		}
    }
}