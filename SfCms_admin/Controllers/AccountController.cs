﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//
using SfCms_model;

namespace SfCms_admin.Controllers
{
    public class AccountController : Controller
    {
		//pattern
		private class_pattern pattern = new class_pattern();
		//dictionary
		private class_dictionary dictionary = new class_dictionary();

		[AllowAnonymous]
		public ActionResult LogIn()
		{
			//pattern - begin
			this.pattern.dictionary = this.dictionary;
			//patern - end

			return View(this.pattern);
		}

		[HttpPost]
		[AllowAnonymous]
		public ActionResult LogIn(FormCollection form)
		{
			//form
			string form_login = string.Empty;
			string form_password = string.Empty;
			string form_password_md5 = string.Empty;

			form_login = form["login"];
			form_password = form["password"];

			//ViewBag
			ViewBag.LogInFailed = false;
			ViewBag.LogInInfo = string.Empty;

			//md5
			class_md5 md5 = new class_md5(form_password, class_md5.encoding.UTF8);
			form_password_md5 = md5.password_md5hash;

			//admin
			class_admin admin = new class_admin();
			if (admin.log_in(form_login, form_password_md5))
			{
				session.admin_id = admin.id.ToString();
				return RedirectToAction("Index","Home");
			}
			else
			{
				ViewBag.LogInFailed = true;
				ViewBag.LogInInfo = this.dictionary.get_phrase(class_dictionary.phrase.LogInFailed);
			}

			//pattern - begin
			this.pattern.dictionary = this.dictionary;
			//patern - end

			return View(this.pattern);
		}

		[AuthorizeSfCms_admin()]
		public ActionResult LogOff()
		{
			Session.Clear();

			return RedirectToAction("LogIn");
		}
    }
}
