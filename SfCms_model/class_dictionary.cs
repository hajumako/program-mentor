﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//
using System.Data;
using System.Data.SqlClient;

namespace SfCms_model
{
	public class class_dictionary
	{
		//variables

		//other

		//phrase
		public enum phrase
		{
			Yes = 1,
			No = 2,
			Add = 3,
			AddAsk = 4,
			Return = 5,
			Delete = 6,
			DeleteAskShe = 7,
			DeleteAskHe = 8,
			DeleteAsk = 9,
			DeleteAccept = 10,
			Edit = 11,
			EditAsk = 12,
			PhotoEdit = 13,
			More = 14,
			Details = 15,
			Show = 16,
			Save = 17,
			FieldRequired = 18,
			Choose = 19,
			IntegerRequired = 20,
			NotDeleteAskShe = 21,
			NotDeleteAskHe = 22,
			LogInFailed = 23
		}

		//variables (class)
		private class_sql sql = new class_sql();

		//constructor
		public class_dictionary()
		{
			//get_data
			this.get_data();
		}

		//properties
		#region "properties"
		public ds_db.dictionaryDataTable dictionary_db { get; set; }
		#endregion

		//properties other
		#region "properties other"
		#endregion

		//get_data
		#region "get_data"
		private void get_data()
		{
			ds_db ds_db = new ds_db();

			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_dictionary", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(class_settings.search_type.all);

					this.sql.adapter = new SqlDataAdapter(this.sql.result);
					this.sql.adapter.Fill(ds_db.dictionary);

					this.dictionary_db = ds_db.dictionary;
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}
		}
		#endregion

		public string get_phrase(phrase phrase)
		{
			string wynik = string.Empty;

			DataRow[] dictionary_db_row = this.dictionary_db.Select("id = " + (int)phrase + "");

			if (dictionary_db_row.Length != 0)
			{
				wynik = dictionary_db_row[0]["value"].ToString();
			}

			return wynik;
		}
	}
}