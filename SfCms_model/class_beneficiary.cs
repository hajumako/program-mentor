﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//
using System.Data;
using System.Data.SqlClient;

namespace SfCms_model
{
	public class class_beneficiary
	{
		//variables

		//other
		public enum search_type
		{
			all = 0,       //all
			id = 1,        //id
			active = 2,    //active
			count = 3,     //count
			project_id = 4 //project_id
		}

		//variables (class)
		private class_sql sql = new class_sql();

		//constructor
		public class_beneficiary(int id)
		{
			this.id = id;

			if (this.id > 0)
			{
				this.get_data();
			}
		}
		public class_beneficiary()
		{

		}

		//properties
		#region "properties"
		public int id { get; set; }
		public bool active { get; set; }
		public int project_id { get; set; }
		public string login { get; set; }
		public string password { get; set; }
		public string name { get; set; }
		public string surname { get; set; }
		public string nick { get; set; }
		public DateTime date_birth { get; set; }
		public string title { get; set; }
		public string content { get; set; }
		public int s_sex_id { get; set; }
		public string yt_link { get; set; }
		public string paylane_merchant_id { get; set; }
		public string paylane_salt { get; set; }

        public int blog_count_total { get; set; }
        public int blog_count_active { get; set; }
		#endregion

		//properties other
		#region "properties other"
		public List<class_beneficiary> list { get; set; }
		public string s_sex_id_dictionary
		{
			get
			{
				string wynik = string.Empty;

				class_s_sex s_sex = new class_s_sex(this.s_sex_id);
				wynik = s_sex.name;

				return wynik;
			}
		}
		#endregion

		//get_data
		#region "get_data"
		public void get_data()
		{
			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_beneficiary", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(class_beneficiary.search_type.id);
					this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = this.id.ToString();
					using (this.sql.reader = this.sql.result.ExecuteReader())
					{
						while (this.sql.reader.Read())
						{
							this.id = (int)(this.sql.reader["id"]);
							this.active = (bool)(this.sql.reader["active"]);
							this.project_id = (int)(this.sql.reader["project_id"]);
							this.login = (string)(this.sql.reader["login"]);
							this.password = (string)(this.sql.reader["password"]);
							this.name = (string)(this.sql.reader["name"]);
							this.surname = (string)(this.sql.reader["surname"]);
							this.nick = (string)(this.sql.reader["nick"]);
							this.date_birth = (DateTime)(this.sql.reader["date_birth"]);
							this.title = (string)(this.sql.reader["title"]);
							this.content = (string)(this.sql.reader["content"]);
							this.s_sex_id = (int)(this.sql.reader["s_sex_id"]);
							this.yt_link = (string)(this.sql.reader["yt_link"]);
							this.paylane_merchant_id = (string)(this.sql.reader["paylane_merchant_id"]);
							this.paylane_salt = (string)(this.sql.reader["paylane_salt"]);
						}
					}
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}
		}
		#endregion

		//insert
		#region "insert"
		public void insert()
		{
			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_beneficiary_insert", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@active", SqlDbType.Bit).Value = this.active;
					this.sql.result.Parameters.Add("@project_id", SqlDbType.Int).Value = this.project_id;
					this.sql.result.Parameters.Add("@login", SqlDbType.VarChar).Value = this.login;
					this.sql.result.Parameters.Add("@password", SqlDbType.VarChar).Value = this.password;
					this.sql.result.Parameters.Add("@name", SqlDbType.VarChar).Value = this.name;
					this.sql.result.Parameters.Add("@surname", SqlDbType.VarChar).Value = this.surname;
					this.sql.result.Parameters.Add("@nick", SqlDbType.VarChar).Value = this.nick;
					this.sql.result.Parameters.Add("@date_birth", SqlDbType.DateTime).Value = this.date_birth;
					this.sql.result.Parameters.Add("@title", SqlDbType.VarChar).Value = this.title;
					this.sql.result.Parameters.Add("@content", SqlDbType.VarChar).Value = this.content;
					this.sql.result.Parameters.Add("@s_sex_id", SqlDbType.Int).Value = this.s_sex_id;
					this.sql.result.Parameters.Add("@yt_link", SqlDbType.VarChar).Value = this.yt_link;
					this.sql.result.Parameters.Add("@paylane_merchant_id", SqlDbType.VarChar).Value = this.paylane_merchant_id;
					this.sql.result.Parameters.Add("@paylane_salt", SqlDbType.VarChar).Value = this.paylane_salt;
					using (this.sql.reader = this.sql.result.ExecuteReader())
					{
						while (this.sql.reader.Read())
						{
							this.id = (int)(this.sql.reader["id"]);
						}
					}
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}
		}
		#endregion

		//update
		#region "update"
		public void update()
		{
			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_beneficiary_update", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@id", SqlDbType.Int).Value = this.id;
					this.sql.result.Parameters.Add("@active", SqlDbType.Bit).Value = this.active;
					this.sql.result.Parameters.Add("@project_id", SqlDbType.Int).Value = this.project_id;
					this.sql.result.Parameters.Add("@login", SqlDbType.VarChar).Value = this.login;
					this.sql.result.Parameters.Add("@password", SqlDbType.VarChar).Value = this.password;
					this.sql.result.Parameters.Add("@name", SqlDbType.VarChar).Value = this.name;
					this.sql.result.Parameters.Add("@surname", SqlDbType.VarChar).Value = this.surname;
					this.sql.result.Parameters.Add("@nick", SqlDbType.VarChar).Value = this.nick;
					this.sql.result.Parameters.Add("@date_birth", SqlDbType.DateTime).Value = this.date_birth;
					this.sql.result.Parameters.Add("@title", SqlDbType.VarChar).Value = this.title;
					this.sql.result.Parameters.Add("@content", SqlDbType.VarChar).Value = this.content;
					this.sql.result.Parameters.Add("@s_sex_id", SqlDbType.Int).Value = this.s_sex_id;
					this.sql.result.Parameters.Add("@yt_link", SqlDbType.VarChar).Value = this.yt_link;
					this.sql.result.Parameters.Add("@paylane_merchant_id", SqlDbType.VarChar).Value = this.paylane_merchant_id;
					this.sql.result.Parameters.Add("@paylane_salt", SqlDbType.VarChar).Value = this.paylane_salt;
					this.sql.result.ExecuteNonQuery();
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}
		}
		#endregion

		//delete
		#region "delete"
		public void delete()
		{
			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_beneficiary_delete", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@id", SqlDbType.Int).Value = this.id;
					this.sql.result.ExecuteNonQuery();
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}
		}
		#endregion

		//count
		#region "count"
		public int count()
		{
			int wynik = 0;

			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_beneficiary", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(class_beneficiary.search_type.count);
					this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = string.Empty;
					using (this.sql.reader = this.sql.result.ExecuteReader())
					{
						while (this.sql.reader.Read())
						{
							wynik = (int)(this.sql.reader["count"]);
						}
					}
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}

			return wynik;
		}
		#endregion

		public void generate_list(search_type search_type, string search_string)
		{
			this.list = new List<class_beneficiary>();

			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_beneficiary", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(search_type);
					this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = search_string;
					using (this.sql.reader = this.sql.result.ExecuteReader())
					{
						while (this.sql.reader.Read())
						{
							class_beneficiary beneficiary = new class_beneficiary
							{
								id = (int)(this.sql.reader["id"]),
								active = (bool)(this.sql.reader["active"]),
								project_id = (int)(this.sql.reader["project_id"]),
								login = (string)(this.sql.reader["login"]),
								password = (string)(this.sql.reader["password"]),
								name = (string)(this.sql.reader["name"]),
								surname = (string)(this.sql.reader["surname"]),
								nick = (string)(this.sql.reader["nick"]),
								date_birth = (DateTime)(this.sql.reader["date_birth"]),
								title = (string)(this.sql.reader["title"]),
								content = (string)(this.sql.reader["content"]),
								s_sex_id = (int)(this.sql.reader["s_sex_id"]),
								yt_link = (string)(this.sql.reader["yt_link"]),
								paylane_merchant_id = (string)(this.sql.reader["paylane_merchant_id"]),
								paylane_salt = (string)(this.sql.reader["paylane_salt"])
							};

							//list
							this.list.Add(beneficiary);
						}
					}
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}
		}

		public int age()
		{
			int wynik = 0;

			DateTime date_birthday = new DateTime(DateTime.Now.Year, this.date_birth.Month, this.date_birth.Day);

			wynik = DateTime.Now.Year - this.date_birth.Year;

			if (DateTime.Now < date_birthday)
			{
				wynik = wynik - 1;
			}

			return wynik;
		}

		public bool log_in(string login, string password)
		{
			bool wynik = false;

			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_beneficiary_log_in", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@login", SqlDbType.VarChar).Value = login;
					this.sql.result.Parameters.Add("@password", SqlDbType.VarChar).Value = password;
					using (this.sql.reader = this.sql.result.ExecuteReader())
					{
						while (this.sql.reader.Read())
						{
							wynik = (bool)(this.sql.reader["wynik"]);
                            this.id = (int)(this.sql.reader["beneficiary_id"]);
						}
					}
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}

			return wynik;
		}
	}
}