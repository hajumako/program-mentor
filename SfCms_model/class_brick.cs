﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//
using System.Data;
using System.Data.SqlClient;

namespace SfCms_model
{
    public class class_brick
    {
        //variables

        //other
        public enum search_type
        {
            id = 0,        //id
            project_id = 1 //project_id
        }

        //variables (class)
        private class_sql sql = new class_sql();

        //constructor
        public class_brick(int id)
        {
            this.id = id;

            if (this.id > 0)
            {
                this.get_data();
            }
        }
        public class_brick()
        {

        }

        //properties
        #region "properties"
        public int id { get; set; }
        public bool active { get; set; }
        public int project_id { get; set; }
        public int s_brick_id { get; set; }
        public string content { get; set; }
        #endregion

        //properties other
        #region "properties other"
        public List<class_brick> list { get; set; }
        public class_s_brick s_brick { get; set; }
        #endregion

        //get_data
        #region "get_data"
        public void get_data()
        {
            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_brick", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(class_brick.search_type.id);
                    this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = this.id.ToString();
                    using (this.sql.reader = this.sql.result.ExecuteReader())
                    {
                        while (this.sql.reader.Read())
                        {
                            this.id = (int)(this.sql.reader["id"]);
                            this.active = (bool)(this.sql.reader["active"]);
                            this.project_id = (int)(this.sql.reader["project_id"]);
                            this.s_brick_id = (int)(this.sql.reader["s_brick_id"]);
                            this.content = (string)(this.sql.reader["content"]);
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }

            //s_brick
            this.s_brick = new class_s_brick(this.s_brick_id);
        }
        #endregion

        //insert
        #region "insert"
        public void insert()
        {
            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_brick_insert", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@active", SqlDbType.Bit).Value = this.active;
                    this.sql.result.Parameters.Add("@project_id", SqlDbType.Int).Value = this.project_id;
                    this.sql.result.Parameters.Add("@s_brick_id", SqlDbType.Int).Value = this.s_brick_id;
                    this.sql.result.Parameters.Add("@content", SqlDbType.VarChar).Value = this.content;
                    using (this.sql.reader = this.sql.result.ExecuteReader())
                    {
                        while (this.sql.reader.Read())
                        {
                            this.id = (int)(this.sql.reader["id"]);
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
        }
        #endregion

        //insert
        #region "insert"
        public void insert(SqlConnection connection, SqlTransaction transaction)
        {
            this.sql.result = new SqlCommand("sfcms_brick_insert", connection, transaction);
            this.sql.result.CommandType = CommandType.StoredProcedure;
            this.sql.result.Parameters.Add("@active", SqlDbType.Bit).Value = this.active;
            this.sql.result.Parameters.Add("@project_id", SqlDbType.Int).Value = this.project_id;
            this.sql.result.Parameters.Add("@s_brick_id", SqlDbType.Int).Value = this.s_brick_id;
            this.sql.result.Parameters.Add("@content", SqlDbType.VarChar).Value = this.content;
            this.sql.result.ExecuteNonQuery();
        }
        #endregion

        //update
        #region "update"
        public void update()
        {
            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_brick_update", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@id", SqlDbType.Int).Value = this.id;
                    this.sql.result.Parameters.Add("@active", SqlDbType.Bit).Value = this.active;
                    this.sql.result.Parameters.Add("@project_id", SqlDbType.Int).Value = this.project_id;
                    this.sql.result.Parameters.Add("@s_brick_id", SqlDbType.Int).Value = this.s_brick_id;
                    this.sql.result.Parameters.Add("@content", SqlDbType.VarChar).Value = this.content;
                    this.sql.result.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
        }
        #endregion

        //update
        #region "update"
        public void update(SqlConnection connection, SqlTransaction transaction)
        {
            this.sql.connection.Open();
            this.sql.result = new SqlCommand("sfcms_brick_update", connection, transaction);
            this.sql.result.CommandType = CommandType.StoredProcedure;
            this.sql.result.Parameters.Add("@id", SqlDbType.Int).Value = this.id;
            this.sql.result.Parameters.Add("@active", SqlDbType.Bit).Value = this.active;
            this.sql.result.Parameters.Add("@project_id", SqlDbType.Int).Value = this.project_id;
            this.sql.result.Parameters.Add("@s_brick_id", SqlDbType.Int).Value = this.s_brick_id;
            this.sql.result.Parameters.Add("@content", SqlDbType.VarChar).Value = this.content;
            this.sql.result.ExecuteNonQuery();
        }
        #endregion

        //delete
        #region "delete"
        public void delete()
        {
            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_brick_delete", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@id", SqlDbType.Int).Value = this.id;
                    this.sql.result.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
        }
        #endregion

        //delete
        #region "delete"
        public void delete(SqlConnection connection, SqlTransaction transaction)
        {
            this.sql.connection.Open();
            this.sql.result = new SqlCommand("sfcms_brick_delete", connection, transaction);
            this.sql.result.CommandType = CommandType.StoredProcedure;
            this.sql.result.Parameters.Add("@id", SqlDbType.Int).Value = this.id;
            this.sql.result.ExecuteNonQuery();
        }
        #endregion

        public void generate_list(search_type search_type, string search_string)
        {
            this.list = new List<class_brick>();

            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_brick", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(search_type);
                    this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = search_string;
                    using (this.sql.reader = this.sql.result.ExecuteReader())
                    {
                        while (this.sql.reader.Read())
                        {
                            class_brick brick = new class_brick
                            {
                                id = (int)(this.sql.reader["id"]),
                                active = (bool)(this.sql.reader["active"]),
                                project_id = (int)(this.sql.reader["project_id"]),
                                s_brick_id = (int)(this.sql.reader["s_brick_id"]),
                                content = (string)(this.sql.reader["content"])
                            };

                            //list
                            this.list.Add(brick);
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
        }
    }
}
