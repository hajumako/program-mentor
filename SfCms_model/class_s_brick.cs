﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//
using System.Data;
using System.Data.SqlClient;

namespace SfCms_model
{
    public class class_s_brick
    {
        //variables

        //other
        public enum search_type
        {
            all = 0, // - bez szukania
            id = 1   // - id
        }

        //variables (class)
        private class_sql sql = new class_sql();

        //constructor
        public class_s_brick(int id)
        {
            this.id = id;

            if (this.id > 0)
            {
                this.get_data();
            }
        }
        public class_s_brick()
        {

        }

        //properties
        #region "properties"
        public int id { get; set; }
        public int value { get; set; }
        public bool active { get; set; }
        public int sequence { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public decimal amount_from { get; set; }
        public decimal amount_to { get; set; }
        public int seed { get; set; }
        #endregion

        //properties other
        #region "properties other"
        public List<class_s_brick> list { get; set; }
        #endregion

        //get_data
        #region "get_data"
        public void get_data()
        {
            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_s_brick", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(class_s_post.search_type.id);
                    this.sql.result.Parameters.Add("@search", SqlDbType.VarChar).Value = this.id.ToString();
                    using (this.sql.reader = this.sql.result.ExecuteReader())
                    {
                        while (this.sql.reader.Read())
                        {
                            this.id = (int)(this.sql.reader["id"]);
                            this.value = (int)(this.sql.reader["value"]);
                            this.active = (bool)(this.sql.reader["active"]);
                            this.sequence = (int)(this.sql.reader["sequence"]);
                            this.name = (string)(this.sql.reader["name"]);
                            this.description = (string)(this.sql.reader["description"]);
                            this.amount_from = (decimal)(this.sql.reader["amount_from"]);
                            this.amount_to = (decimal)(this.sql.reader["amount_to"]);
                            this.seed = (int)(this.sql.reader["seed"]);
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
        }

        public void get_all_data()
        {
            this.list = new List<class_s_brick>();

            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_s_brick", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(class_s_brick.search_type.all);
                    this.sql.result.Parameters.Add("@search", SqlDbType.VarChar).Value = this.id.ToString();
                    using (this.sql.reader = this.sql.result.ExecuteReader())
                    {
                        while (this.sql.reader.Read())
                        {
                            class_s_brick s_brick = new class_s_brick
                            {
                                id = (int)(this.sql.reader["id"]),
                                value = (int)(this.sql.reader["value"]),
                                active = (bool)(this.sql.reader["active"]),
                                sequence = (int)(this.sql.reader["sequence"]),
                                name = (string)(this.sql.reader["name"]),
                                description = (string)(this.sql.reader["description"]),
                                amount_from = (decimal)(this.sql.reader["amount_from"]),
                                amount_to = (decimal)(this.sql.reader["amount_to"]),
                                seed = (int)(this.sql.reader["seed"])
                            };

                            this.list.Add(s_brick);
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
        }
        #endregion
    }
}
