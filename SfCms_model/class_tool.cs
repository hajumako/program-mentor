﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//
using System.Data;
using System.Data.SqlClient;

namespace SfCms_model
{
	public class class_tool
	{
		//variables

		//other

		//variables (class)
		private class_sql sql = new class_sql();

		//constructor
		public class_tool()
		{

		}

		//properties
		#region "properties"
		#endregion

		//properties other
		#region "properties other"
		#endregion

		//get_data
		#region "get_data"
		#endregion

		public string format_decimal(decimal amount)
		{
			string wynik = string.Empty;

			wynik = String.Format("{0:0.00}", amount);

			return wynik;
		}

		public string format_decimal_dot(decimal amount)
		{
			string wynik = string.Empty;

			wynik = String.Format("{0:0.00}", amount);
			wynik = wynik.Replace(",", ".");

			return wynik;
		}

		public string format_decimal_int(decimal amount)
		{
			string wynik = string.Empty;

			wynik = String.Format("{0:0.##}", amount);  

			return wynik;
		}
	}
}