﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//
using System.Data;
using System.Data.SqlClient;

namespace SfCms_model
{
	public class class_beneficiary_file
	{
		//variables

		//other
		public enum search_type
		{
			all = 0,            //all
			id = 1,             //id
			beneficiary_id = 2  //beneficiary_id
		}

        public enum image_type
        {
            empty = 0,  //empty  
            large = 1,  //large
            medium = 2, //medium
            small = 3   //small
        }

		//variables (class)
		private class_sql sql = new class_sql();

		//constructor
        public class_beneficiary_file(search_type search_type, image_type image_type, string search_string)
		{
			this.get_data(search_type, image_type, search_string);
		}
		public class_beneficiary_file()
		{

		}

		//properties
		#region "properties"
		public int id { get; set; }
		public int beneficiary_id { get; set; }
        public byte[] content { get; set; }
        public string content_type { get; set; }
        public string name { get; set; }
		#endregion

		//properties other
		#region "properties other"
        public byte[] content_large { get; set; }
        public byte[] content_medium { get; set; }
        public byte[] content_small { get; set; }
		#endregion

		//get_data
		#region "get_data"
        public void get_data(search_type search_type, image_type image_type, string search_string)
		{
            //clear
            this.clear();

			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_beneficiary_file", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@search_type1", SqlDbType.Int).Value = (int)(search_type);
                    this.sql.result.Parameters.Add("@search_type2", SqlDbType.Int).Value = (int)(image_type);
                    this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = search_string;
					using (this.sql.reader = this.sql.result.ExecuteReader())
					{
						while (this.sql.reader.Read())
						{
							this.id = (int)(this.sql.reader["id"]);
							this.beneficiary_id = (int)(this.sql.reader["beneficiary_id"]);
                            if (image_type != class_beneficiary_file.image_type.empty)
                            {
                                this.content = (byte[])(this.sql.reader["content"]);
                            }
							this.content_type = (string)(this.sql.reader["content_type"]);
							this.name = (string)(this.sql.reader["name"]);
						}
					}
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}
		}
		#endregion

		//insert
		#region "insert"
		public void insert()
		{
			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_beneficiary_file_insert", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@beneficiary_id", SqlDbType.Int).Value = this.beneficiary_id;
                    this.sql.result.Parameters.Add("@content_large", SqlDbType.Image).Value = this.content_large;
                    this.sql.result.Parameters.Add("@content_medium", SqlDbType.Image).Value = this.content_medium;
                    this.sql.result.Parameters.Add("@content_small", SqlDbType.Image).Value = this.content_small;
                    this.sql.result.Parameters.Add("@content_type", SqlDbType.VarChar).Value = this.content_type;
                    this.sql.result.Parameters.Add("@name", SqlDbType.VarChar).Value = this.name;
					using (this.sql.reader = this.sql.result.ExecuteReader())
					{
						while (this.sql.reader.Read())
						{
							this.id = (int)(this.sql.reader["id"]);
						}
					}
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}
		}
		#endregion

		//delete
		#region "delete"
		public void delete()
		{
			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_beneficiary_file_delete", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@id", SqlDbType.Int).Value = this.id;
					this.sql.result.ExecuteNonQuery();
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}
		}
		#endregion

        //clear
        #region "clear"
        public void clear()
        {
            this.id = 0;
            this.beneficiary_id = 0;
            this.content = null;
            this.content_large = null;
            this.content_medium = null;
            this.content_small = null;
            this.content_type = null;
            this.name = null;
        }
        #endregion
	}
}