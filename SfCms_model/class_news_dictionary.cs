﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//
using System.Data;
using System.Data.SqlClient;

namespace SfCms_model
{
	public class class_news_dictionary
	{
		//variables

		//other

		//id
		public enum id
		{
			id_1 = 1,
			id_2 = 2,
			id_3 = 3,
			id_4 = 4,
			id_5 = 5,
			id_6 = 6,
			id_7 = 7,
			id_8 = 8,
			id_9 = 9,
			id_10 = 10,
			id_11 = 11,
            id_12 = 12,
			id_13 = 13
		}

		//variables (class)
		private class_sql sql = new class_sql();

		//constructor
		public class_news_dictionary()
		{
			//get_data
			this.get_data();
		}

		//properties
		#region "properties"
		public ds_db.news_dictionaryDataTable news_dictionary_db { get; set; }
		#endregion

		//properties other
		#region "properties other"
		#endregion

		//get_data
		#region "get_data"
		private void get_data()
		{
			ds_db ds_db = new ds_db();

			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_news_dictionary", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(class_settings.search_type.all);

					this.sql.adapter = new SqlDataAdapter(this.sql.result);
					this.sql.adapter.Fill(ds_db.news_dictionary);

					this.news_dictionary_db = ds_db.news_dictionary;
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}
		}
		#endregion

		public string get_phrase(id id)
		{
			string wynik = string.Empty;

			DataRow[] news_dictionary_db_row = this.news_dictionary_db.Select("id = " + (int)id + "");

			if (news_dictionary_db_row.Length != 0)
			{
				wynik = news_dictionary_db_row[0]["value"].ToString();
			}

			return wynik;
		}
	}
}