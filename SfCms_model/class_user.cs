﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;

namespace SfCms_model
{
    public class class_user
    {
        //variables

        //other
        public enum search_type
        {
            all = 0,                //all
            id = 1,                 //id
            active = 2,             //active
            count = 3,              //count
            email = 4,              //email
            uid_password_reset = 5, //uid_password_reset
            uid_register = 6,       //uid_register
            facebook_id = 7         //facebook_id
        }

        public enum insert_type
        {
            user = 0,                //user
            facebook = 1             //facebook
        }

        //variables (class)
        private class_sql sql = new class_sql();
        private class_settings settings = new class_settings();

        //constructor
        public class_user(int id)
        {
            this.id = id;

            if (this.id > 0)
            {
                this.get_data();
            }
        }
        public class_user()
        {

        }

        //properties
        #region "properties"
        public int id { get; set; }
        public bool active { get; set; }
        public string login { get; set; }
        public string password { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public string email { get; set; }
        public DateTime date_create { get; set; }
        public DateTime date_password_change { get; set; }
        public DateTime date_last_logged { get; set; }
        public string uid_password_reset { get; set; }
        public string uid_register { get; set; }
        public string facebook_id { get; set; }
        #endregion

        //properties other
        #region "properties other"
        public List<class_user> list { get; set; }
        public string password_no_md5 { get; set; }
        public string password_repeat { get; set; }
        public bool regulations { get; set; }
        public bool policy { get; set; }
        public string link_password_reset { get; set; }
        public string link_register { get; set; }
        public string subject_password_reset
        {
            get
            {
                string wynik = string.Empty;

                wynik = "Reset hasła | " + this.settings.name;

                return wynik;
            }
        }
        public string subject_thankyou
        {
            get
            {
                string wynik = string.Empty;

                wynik = "Dziękujemy za Twoje wsparcie | " + this.settings.name;

                return wynik;
            }
        }
        public string subject_register
        {
            get
            {
                string wynik = string.Empty;

                wynik = "Aktywacja konta | " + this.settings.name;

                return wynik;
            }
        }
		public bool insert_email_exists { get; set; }
		public bool insert_uid_register_exists { get; set; }
        public bool insert_facebook_id_exists { get; set; }
		#endregion

        //get_data
        #region "get_data"
        public void get_data()
        {
            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_user", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(class_user.search_type.id);
                    this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = this.id.ToString();
                    using (this.sql.reader = this.sql.result.ExecuteReader())
                    {
                        while (this.sql.reader.Read())
                        {
                            this.id = (int)(this.sql.reader["id"]);
                            this.active = (bool)(this.sql.reader["active"]);
                            this.login = (string)(this.sql.reader["login"]);
                            this.password = (string)(this.sql.reader["password"]);
                            this.name = (string)(this.sql.reader["name"]);
                            this.surname = (string)(this.sql.reader["surname"]);
                            this.email = (string)(this.sql.reader["email"]);
                            this.date_create = (DateTime)(this.sql.reader["date_create"]);
                            this.date_password_change = (DateTime)(this.sql.reader["date_password_change"]);
                            this.date_last_logged = (DateTime)(this.sql.reader["date_last_logged"]);
                            this.uid_password_reset = (string)(this.sql.reader["uid_password_reset"]);
                            this.uid_register = (string)(this.sql.reader["uid_register"]);
                            this.facebook_id = (string)(this.sql.reader["facebook_id"]);
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
        }
        #endregion

        //insert
        #region "insert"
        public void insert(insert_type insert_type)
        {
            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();

                    //transaction - begin
                    this.sql.transaction = this.sql.connection.BeginTransaction();

                    if (insert_type == class_user.insert_type.user)
                    {
                        this.sql.result = new SqlCommand("sfcms_user_exists", this.sql.connection, this.sql.transaction);
                        this.sql.result.CommandType = CommandType.StoredProcedure;
                        this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(search_type.email);
                        this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = this.email;
                        using (this.sql.reader = this.sql.result.ExecuteReader())
                        {
                            while (this.sql.reader.Read())
                            {
                                this.insert_email_exists = (bool)(this.sql.reader["wynik"]);
                            }
                        }

                        this.sql.result = new SqlCommand("sfcms_user_exists", this.sql.connection, this.sql.transaction);
                        this.sql.result.CommandType = CommandType.StoredProcedure;
                        this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(search_type.uid_register);
                        this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = this.uid_register;
                        using (this.sql.reader = this.sql.result.ExecuteReader())
                        {
                            while (this.sql.reader.Read())
                            {
                                this.insert_uid_register_exists = (bool)(this.sql.reader["wynik"]);
                            }
                        }

                        if (!this.insert_email_exists && !this.insert_uid_register_exists)
                        {
                            this.sql.result = new SqlCommand("sfcms_user_insert", this.sql.connection, this.sql.transaction);
                            this.sql.result.CommandType = CommandType.StoredProcedure;
                            this.sql.result.Parameters.Add("@active", SqlDbType.Bit).Value = this.active;
                            this.sql.result.Parameters.Add("@login", SqlDbType.VarChar).Value = this.login;
                            this.sql.result.Parameters.Add("@password", SqlDbType.VarChar).Value = this.password;
                            this.sql.result.Parameters.Add("@name", SqlDbType.VarChar).Value = this.name;
                            this.sql.result.Parameters.Add("@surname", SqlDbType.VarChar).Value = this.surname;
                            this.sql.result.Parameters.Add("@email", SqlDbType.VarChar).Value = this.email;
                            this.sql.result.Parameters.Add("@date_create", SqlDbType.DateTime).Value = this.date_create;
                            this.sql.result.Parameters.Add("@date_password_change", SqlDbType.DateTime).Value = this.date_password_change;
                            this.sql.result.Parameters.Add("@date_last_logged", SqlDbType.DateTime).Value = this.date_last_logged;
                            this.sql.result.Parameters.Add("@uid_password_reset", SqlDbType.VarChar).Value = this.uid_password_reset;
                            this.sql.result.Parameters.Add("@uid_register", SqlDbType.VarChar).Value = this.uid_register;
                            this.sql.result.Parameters.Add("@facebook_id", SqlDbType.VarChar).Value = this.facebook_id;
                            using (this.sql.reader = this.sql.result.ExecuteReader())
                            {
                                while (this.sql.reader.Read())
                                {
                                    this.id = (int)(this.sql.reader["id"]);
                                }
                            }
                        }
                    }
                    else if (insert_type == class_user.insert_type.facebook)
                    {
                        this.sql.result = new SqlCommand("sfcms_user_exists", this.sql.connection, this.sql.transaction);
                        this.sql.result.CommandType = CommandType.StoredProcedure;
                        this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(search_type.facebook_id);
                        this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = this.facebook_id;
                        using (this.sql.reader = this.sql.result.ExecuteReader())
                        {
                            while (this.sql.reader.Read())
                            {
                                this.insert_facebook_id_exists = (bool)(this.sql.reader["wynik"]);
                            }
                        }

                        if (!this.insert_facebook_id_exists)
                        {
                            this.sql.result = new SqlCommand("sfcms_user_insert", this.sql.connection, this.sql.transaction);
                            this.sql.result.CommandType = CommandType.StoredProcedure;
                            this.sql.result.Parameters.Add("@active", SqlDbType.Bit).Value = this.active;
                            this.sql.result.Parameters.Add("@login", SqlDbType.VarChar).Value = this.login;
                            this.sql.result.Parameters.Add("@password", SqlDbType.VarChar).Value = this.password;
                            this.sql.result.Parameters.Add("@name", SqlDbType.VarChar).Value = this.name;
                            this.sql.result.Parameters.Add("@surname", SqlDbType.VarChar).Value = this.surname;
                            this.sql.result.Parameters.Add("@email", SqlDbType.VarChar).Value = this.email;
                            this.sql.result.Parameters.Add("@date_create", SqlDbType.DateTime).Value = this.date_create;
                            this.sql.result.Parameters.Add("@date_password_change", SqlDbType.DateTime).Value = this.date_password_change;
                            this.sql.result.Parameters.Add("@date_last_logged", SqlDbType.DateTime).Value = this.date_last_logged;
                            this.sql.result.Parameters.Add("@uid_password_reset", SqlDbType.VarChar).Value = this.uid_password_reset;
                            this.sql.result.Parameters.Add("@uid_register", SqlDbType.VarChar).Value = this.uid_register;
                            this.sql.result.Parameters.Add("@facebook_id", SqlDbType.VarChar).Value = this.facebook_id;
                            using (this.sql.reader = this.sql.result.ExecuteReader())
                            {
                                while (this.sql.reader.Read())
                                {
                                    this.id = (int)(this.sql.reader["id"]);
                                }
                            }
                        }
                    }

                    //transaction - commit
                    this.sql.transaction.Commit();
                }
                catch (Exception ex)
                {
                    this.sql.transaction.Rollback();

                    this.sql.ex = ex.Message;
                }
            }
        }
        #endregion

        //update
        #region "update"
        public void update()
        {
            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_user_update", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@id", SqlDbType.Int).Value = this.id;
                    this.sql.result.Parameters.Add("@active", SqlDbType.Bit).Value = this.active;
                    this.sql.result.Parameters.Add("@login", SqlDbType.VarChar).Value = this.login;
                    this.sql.result.Parameters.Add("@password", SqlDbType.VarChar).Value = this.password;
                    this.sql.result.Parameters.Add("@name", SqlDbType.VarChar).Value = this.name;
                    this.sql.result.Parameters.Add("@surname", SqlDbType.VarChar).Value = this.surname;
                    this.sql.result.Parameters.Add("@email", SqlDbType.VarChar).Value = this.email;
                    this.sql.result.Parameters.Add("@date_create", SqlDbType.DateTime).Value = this.date_create;
                    this.sql.result.Parameters.Add("@date_password_change", SqlDbType.DateTime).Value = this.date_password_change;
                    this.sql.result.Parameters.Add("@date_last_logged", SqlDbType.DateTime).Value = this.date_last_logged;
                    this.sql.result.Parameters.Add("@uid_password_reset", SqlDbType.VarChar).Value = this.uid_password_reset;
                    this.sql.result.Parameters.Add("@uid_register", SqlDbType.VarChar).Value = this.uid_register;
                    this.sql.result.Parameters.Add("@facebook_id", SqlDbType.VarChar).Value = this.facebook_id;
                    this.sql.result.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
        }
        #endregion

        //delete
        #region "delete"
        public void delete()
        {
            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_user_delete", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@id", SqlDbType.Int).Value = this.id;
                    this.sql.result.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
        }
        #endregion

        //count
        #region "count"
        public int count()
        {
            int wynik = 0;

            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_user", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(class_user.search_type.count);
                    this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = string.Empty;
                    using (this.sql.reader = this.sql.result.ExecuteReader())
                    {
                        while (this.sql.reader.Read())
                        {
                            wynik = (int)(this.sql.reader["count"]);
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }

            return wynik;
        }
        #endregion

        public void generate_list(search_type search_type, string search_string)
        {
            this.list = new List<class_user>();

            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_user", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(search_type);
                    this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = search_string;
                    using (this.sql.reader = this.sql.result.ExecuteReader())
                    {
                        while (this.sql.reader.Read())
                        {
                            class_user user = new class_user
                            {
                                id = (int)(this.sql.reader["id"]),
                                active = (bool)(this.sql.reader["active"]),
                                login = (string)(this.sql.reader["login"]),
                                password = (string)(this.sql.reader["password"]),
                                name = (string)(this.sql.reader["name"]),
                                surname = (string)(this.sql.reader["surname"]),
                                email = (string)(this.sql.reader["email"]),
                                date_create = (DateTime)(this.sql.reader["date_create"]),
                                date_password_change = (DateTime)(this.sql.reader["date_password_change"]),
                                date_last_logged = (DateTime)(this.sql.reader["date_last_logged"]),
                                uid_password_reset = (string)(this.sql.reader["uid_password_reset"]),
                                uid_register = (string)(this.sql.reader["uid_register"]),
                                facebook_id = (string)(this.sql.reader["facebook_id"])
                            };

                            //list
                            this.list.Add(user);
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
        }

        public bool log_in(string login, string password)
        {
            bool wynik = false;

            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_user_log_in", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@login", SqlDbType.VarChar).Value = login;
                    this.sql.result.Parameters.Add("@password", SqlDbType.VarChar).Value = password;
                    using (this.sql.reader = this.sql.result.ExecuteReader())
                    {
                        while (this.sql.reader.Read())
                        {
                            wynik = (bool)(this.sql.reader["wynik"]);
                            this.id = (int)(this.sql.reader["user_id"]);
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }

            return wynik;
        }

        public bool exists(search_type search_type, string search_string)
        {
            bool wynik = false;

            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_user_exists", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(search_type);
                    this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = search_string;
                    using (this.sql.reader = this.sql.result.ExecuteReader())
                    {
                        while (this.sql.reader.Read())
                        {
                            wynik = (bool)(this.sql.reader["wynik"]);
                            this.id = (int)(this.sql.reader["user_id"]);
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }

            return wynik;
        }

        public string generate_uid()
        {
            char[] chars = new char[62];
            chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();

            byte[] data = new byte[1];

            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            data = new byte[32];
            crypto.GetNonZeroBytes(data);

            StringBuilder result = new StringBuilder(32);

            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length - 1)]);
            }

            return result.ToString();
        }

        public string body_password_reset(string email_template)
        {
            email_template = email_template.Replace("#ImagePath#", "http://program-mentor.pl/assets/email_templates/");
            email_template = email_template.Replace("#PasswordResetLink#", this.link_password_reset);

            return email_template;
        }

        public string body_thankyou(string email_template, class_paylane paylane, class_project project)
        {
            email_template = email_template.Replace("#ImagePath#", "http://program-mentor.pl/assets/email_templates/");
            email_template = email_template.Replace("#DonorName#", this.name);
            email_template = email_template.Replace("#BeneficairyName#", project.list_beneficiary[0].name);
            email_template = email_template.Replace("#Amount# ", paylane.response_amount.ToString());
            email_template = email_template.Replace("#BeneficiaryType1#", project.list_beneficiary[0].s_sex_id == 1 ? "jej" : "jego");
            email_template = email_template.Replace("#BeneficiaryType2#", project.list_beneficiary[0].s_sex_id == 1 ? "Stypendystki" : "Stypendysty");
            email_template = email_template.Replace("#BeneficiaryType3#", project.list_beneficiary[0].s_sex_id == 1 ? "mogła" : "mógł");
            email_template = email_template.Replace("#BeneficiaryBlog#", "http://program-mentor.pl/projekty/" + project.name);
      
            return email_template;
        }

        public string body_register(string email_template)
        {
            email_template = email_template.Replace("#ImagePath#", "http://program-mentor.pl/assets/email_templates/");
            email_template = email_template.Replace("#DonorName#", this.name);
            email_template = email_template.Replace("#ActivationLink#", this.link_register);

            return email_template;
        }
    }
}