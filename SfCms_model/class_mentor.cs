﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//
using System.Data;
using System.Data.SqlClient;

namespace SfCms_model
{
	public class class_mentor
	{
		//variables

		//other
		public enum search_type
		{
			all = 0,       //all
			id = 1,        //id
			active = 2,    //active
			count = 3,     //count
			project_id = 4 //project_id
		}

		//variables (class)
		private class_sql sql = new class_sql();

		//constructor
		public class_mentor(int id)
		{
			this.id = id;

			if (this.id > 0)
			{
				this.get_data();
			}
		}
		public class_mentor()
		{

		}

		//properties
		#region "properties"
		public int id { get; set; }
		public bool active { get; set; }
		public int project_id { get; set; }
		public string name { get; set; }
		public string surname { get; set; }
		public string content { get; set; }
		#endregion

		//properties other
		#region "properties other"
		public List<class_mentor> list { get; set; }
		#endregion

		//get_data
		#region "get_data"
		public void get_data()
		{
			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_mentor", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(class_mentor.search_type.id);
					this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = this.id.ToString();
					using (this.sql.reader = this.sql.result.ExecuteReader())
					{
						while (this.sql.reader.Read())
						{
							this.id = (int)(this.sql.reader["id"]);
							this.active = (bool)(this.sql.reader["active"]);
							this.project_id = (int)(this.sql.reader["project_id"]);
							this.name = (string)(this.sql.reader["name"]);
							this.surname = (string)(this.sql.reader["surname"]);
							this.content = (string)(this.sql.reader["content"]);
						}
					}
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}
		}
		#endregion

		//insert
		#region "insert"
		public void insert()
		{
			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_mentor_insert", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@active", SqlDbType.Bit).Value = this.active;
					this.sql.result.Parameters.Add("@project_id", SqlDbType.Int).Value = this.project_id;
					this.sql.result.Parameters.Add("@name", SqlDbType.VarChar).Value = this.name;
					this.sql.result.Parameters.Add("@surname", SqlDbType.VarChar).Value = this.surname;
					this.sql.result.Parameters.Add("@content", SqlDbType.VarChar).Value = this.content;
					using (this.sql.reader = this.sql.result.ExecuteReader())
					{
						while (this.sql.reader.Read())
						{
							this.id = (int)(this.sql.reader["id"]);
						}
					}
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}
		}
		#endregion

		//update
		#region "update"
		public void update()
		{
			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_mentor_update", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@id", SqlDbType.Int).Value = this.id;
					this.sql.result.Parameters.Add("@active", SqlDbType.Bit).Value = this.active;
					this.sql.result.Parameters.Add("@project_id", SqlDbType.Int).Value = this.project_id;
					this.sql.result.Parameters.Add("@name", SqlDbType.VarChar).Value = this.name;
					this.sql.result.Parameters.Add("@surname", SqlDbType.VarChar).Value = this.surname;
					this.sql.result.Parameters.Add("@content", SqlDbType.VarChar).Value = this.content;
					this.sql.result.ExecuteNonQuery();
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}
		}
		#endregion

		//delete
		#region "delete"
		public void delete()
		{
			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_mentor_delete", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@id", SqlDbType.Int).Value = this.id;
					this.sql.result.ExecuteNonQuery();
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}
		}
		#endregion

		//count
		#region "count"
		public int count()
		{
			int wynik = 0;

			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_mentor", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(class_mentor.search_type.count);
					this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = string.Empty;
					using (this.sql.reader = this.sql.result.ExecuteReader())
					{
						while (this.sql.reader.Read())
						{
							wynik = (int)(this.sql.reader["count"]);
						}
					}
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}

			return wynik;
		}
		#endregion

		public void generate_list(search_type search_type, string search_string)
		{
			this.list = new List<class_mentor>();

			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_mentor", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(search_type);
					this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = search_string;
					using (this.sql.reader = this.sql.result.ExecuteReader())
					{
						while (this.sql.reader.Read())
						{
							class_mentor mentor = new class_mentor
							{
								id = (int)(this.sql.reader["id"]),
								active = (bool)(this.sql.reader["active"]),
								project_id = (int)(this.sql.reader["project_id"]),
								name = (string)(this.sql.reader["name"]),
								surname = (string)(this.sql.reader["surname"]),
								content = (string)(this.sql.reader["content"])
							};

							//list
							this.list.Add(mentor);
						}
					}
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}
		}
	}
}