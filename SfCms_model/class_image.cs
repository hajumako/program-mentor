﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//
using System.IO;

namespace SfCms_model
{
    public class class_image
    {
        //variables

        //other
        
        //variables (class)
        private class_sql sql = new class_sql();
        private class_settings settings = new class_settings();
        
        //constructor
        public class_image(string content_type)
        {
            this.content_type = content_type;

            this.get_data();
        }

        //properties
        #region "properties"
        public string content_type { get; set; }
        public string format { get; set; }
        public string large { get; set; }
        public string medium { get; set; }
        public string small { get; set; }
        public int file_size { get; set; }
        public byte[] file_content { get; set; }
        public Stream stream_out { get; set; }
        public System.Collections.Generic.Dictionary<string, string> versions { get; set; }
        #endregion

        //properties other
        #region "properties other"
        #endregion

        //get_data
        #region "get_data"
        public void get_data()
        {
            //format
            this.format = this.content_type.Split('/')[1];

            this.large = "large";
            this.medium = "medium";
            this.small = "small";

            this.versions = new Dictionary<string, string>();

            this.versions.Add(this.large, "maxwidth=" + this.settings.image_large + "&maxheight=" + this.settings.image_large + "&format=" + this.format + "");
            this.versions.Add(this.medium, "maxwidth=" + this.settings.image_medium + "&maxheight=" + this.settings.image_medium + "&format=" + this.format + "");
            this.versions.Add(this.small, "maxwidth=" + this.settings.image_small + "&maxheight=" + this.settings.image_small + "&format=" + this.format + "");
        }
        #endregion
    }
}
