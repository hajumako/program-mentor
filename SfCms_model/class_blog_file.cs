﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//
using System.Data;
using System.Data.SqlClient;

namespace SfCms_model
{
    public class class_blog_file
    {
        //variables

        //other
        public enum search_type
        {
            blog_id = 0, //blog_id
            id = 1       //id
        }

        public enum image_type
        {
            empty = 0,  //empty  
            large = 1,  //large
            medium = 2, //medium
            small = 3   //small
        }

        //variables (class)
        private class_sql sql = new class_sql();

        //constructor
        public class_blog_file(int id, image_type image_type)
        {
            this.id = id;

            if (this.id > 0)
            {
                this.get_data(image_type);
            }
        }
        public class_blog_file()
        {

        }

        //properties
        #region "properties"
        public int id { get; set; }
        public int blog_id { get; set; }
        public int no { get; set; }
        public byte[] content { get; set; }
        public string content_type { get; set; }
        public string name { get; set; }
		public string title { get; set; }
        #endregion

        //properties other
        #region "properties other"
        public byte[] content_large { get; set; }
        public byte[] content_medium { get; set; }
        public byte[] content_small { get; set; }
        public List<class_blog_file> list { get; set; }
        #endregion

        //get_data
        #region "get_data"
        public void get_data(image_type image_type)
        {
            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_blog_file", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@search_type1", SqlDbType.Int).Value = (int)(search_type.id);
                    this.sql.result.Parameters.Add("@search_type2", SqlDbType.Int).Value = (int)(image_type);
                    this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = this.id;
                    using (this.sql.reader = this.sql.result.ExecuteReader())
                    {
                        while (this.sql.reader.Read())
                        {
                            this.id = (int)(this.sql.reader["id"]);
                            this.blog_id = (int)(this.sql.reader["blog_id"]);
                            this.no = (int)(this.sql.reader["no"]);
                            this.content = (byte[])(this.sql.reader["content"]);
                            this.content_type = (string)(this.sql.reader["content_type"]);
                            this.name = (string)(this.sql.reader["name"]);
							this.title = (string)(this.sql.reader["title"]);
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
        }
        #endregion

        //insert
        #region "insert"
        public void insert()
        {
            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_blog_file_insert", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@blog_id", SqlDbType.Int).Value = this.blog_id;
                    this.sql.result.Parameters.Add("@no", SqlDbType.Int).Value = this.no;
                    this.sql.result.Parameters.Add("@content_large", SqlDbType.Image).Value = this.content_large;
                    this.sql.result.Parameters.Add("@content_medium", SqlDbType.Image).Value = this.content_medium;
                    this.sql.result.Parameters.Add("@content_small", SqlDbType.Image).Value = this.content_small;
                    this.sql.result.Parameters.Add("@content_type", SqlDbType.VarChar).Value = this.content_type;
                    this.sql.result.Parameters.Add("@name", SqlDbType.VarChar).Value = this.name;
					this.sql.result.Parameters.Add("@title", SqlDbType.VarChar).Value = this.title;
                    using (this.sql.reader = this.sql.result.ExecuteReader())
                    {
                        while (this.sql.reader.Read())
                        {
                            this.id = (int)(this.sql.reader["id"]);
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
        }
        #endregion

		//update
		#region "update"
		public void update()
		{
			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_blog_file_update", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@id", SqlDbType.Int).Value = this.id;
					this.sql.result.Parameters.Add("@title", SqlDbType.VarChar).Value = this.title;
					this.sql.result.ExecuteNonQuery();
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}
		}
		#endregion

        //delete
        #region "delete"
        public void delete()
        {
            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_blog_file_delete", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@id", SqlDbType.Int).Value = this.id;
                    this.sql.result.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
        }
        #endregion

        //clear
        #region "clear"
        public void clear()
        {
            this.content = null;
            this.content_large = null;
            this.content_medium = null;
            this.content_small = null;
            this.content_type = null;
            this.name = null;
			this.title = null;
        }
        #endregion

        public void generate_list(search_type search_type, string search_string)
        {
            this.list = new List<class_blog_file>();

            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_blog_file", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@search_type1", SqlDbType.Int).Value = (int)(search_type);
                    this.sql.result.Parameters.Add("@search_type2", SqlDbType.Int).Value = (int)(image_type.empty);
                    this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = search_string;
                    using (this.sql.reader = this.sql.result.ExecuteReader())
                    {
                        while (this.sql.reader.Read())
                        {
                            class_blog_file blog_file = new class_blog_file
                            {
                                id = (int)(this.sql.reader["id"]),
                                blog_id = (int)(this.sql.reader["blog_id"]),
                                no = (int)(this.sql.reader["no"]),
                                //content = (byte[])(this.sql.reader["content"]),
                                content_type = (string)(this.sql.reader["content_type"]),
                                name = (string)(this.sql.reader["name"]),
								title = (string)(this.sql.reader["title"]),
                            };

                            //list
                            this.list.Add(blog_file);
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
        }
    }
}
