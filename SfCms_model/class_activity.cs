﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;

namespace SfCms_model
{
    public class class_activity
    {
        //variables

        //other
        public enum search_type
        {
            all = 0,        //all
            id = 1,         //id
            active = 2,     //active
            count = 3,      //count
            user_app_id = 4, //user_app_id
            sponsor_id = 5, //sponsor id
            sponsor_amount = 6 //total amount gathered for sponsor
        }

        //variables (class)
        private class_sql sql = new class_sql();

        //constructor
        public class_activity(int id)
        {
            this.id = id;

            if (this.id > 0)
            {
                this.get_data();
            }
        }
        public class_activity()
        {

        }

        //properties
        #region "properties"
        public int id { get; set; }
        public bool active { get; set; }
        public int user_app_id { get; set; }
        public decimal speed { get; set; }
        public decimal distance { get; set; }
        public decimal amount { get; set; }
        public bool czy_anonymous { get; set; }
        public int sponsor_id { get; set; }
        #endregion

        //properties other
        #region "properties other"
        public List<class_activity> list { get; set; }
        public List<class_activity_extended> list_ext { get; set; }
        #endregion

        //get_data
        #region "get_data"
        public void get_data()
        {
            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_activity", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(class_user.search_type.id);
                    this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = this.id.ToString();
                    using (this.sql.reader = this.sql.result.ExecuteReader())
                    {
                        while (this.sql.reader.Read())
                        {
                            this.id = (int)(this.sql.reader["id"]);
                            this.active = (bool)(this.sql.reader["active"]);
                            this.user_app_id = (int)(this.sql.reader["user_app_id"]);
                            this.speed = (decimal)(this.sql.reader["speed"]);
                            this.distance = (decimal)(this.sql.reader["distance"]);
                            this.amount = (decimal)(this.sql.reader["amount"]);
                            this.czy_anonymous = (bool)(this.sql.reader["czy_anonymous"]);
                            this.sponsor_id = (int)(this.sql.reader["sponsor_id"]);
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
        }
        #endregion

        //insert
        #region "insert"
        public void insert()
        {
            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();

                    int sponsor_current = 0;
                    class_sponsor sponsor = new class_sponsor();
                    sponsor.generate_list(class_sponsor.search_type.current, string.Empty);
                    if (sponsor.list.Count != 0)
                    {
                        sponsor_current = sponsor.list[0].id;
                    }

                    this.sql.result = new SqlCommand("sfcms_activity_insert", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@active", SqlDbType.Bit).Value = this.active;
                    this.sql.result.Parameters.Add("@user_app_id", SqlDbType.Int).Value = this.user_app_id;
                    this.sql.result.Parameters.Add("@speed", SqlDbType.Decimal).Value = this.speed;
                    this.sql.result.Parameters.Add("@distance", SqlDbType.Decimal).Value = this.distance;
                    this.sql.result.Parameters.Add("@amount", SqlDbType.Decimal).Value = this.amount;
                    this.sql.result.Parameters.Add("@czy_anonymous", SqlDbType.Bit).Value = this.czy_anonymous;
                    this.sql.result.Parameters.Add("@sponsor_id", SqlDbType.Int).Value = sponsor_current;
                    using (this.sql.reader = this.sql.result.ExecuteReader())
                    {
                        while (this.sql.reader.Read())
                        {
                            this.id = (int)(this.sql.reader["id"]);
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
        }
        #endregion

        //update
        #region "update"
        public void update()
        {
            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_activity_update", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@id", SqlDbType.Int).Value = this.id;
                    this.sql.result.Parameters.Add("@active", SqlDbType.Bit).Value = this.active;
                    this.sql.result.Parameters.Add("@user_app_id", SqlDbType.Int).Value = this.user_app_id;
                    this.sql.result.Parameters.Add("@speed", SqlDbType.Decimal).Value = this.speed;
                    this.sql.result.Parameters.Add("@distance", SqlDbType.Decimal).Value = this.distance;
                    this.sql.result.Parameters.Add("@amount", SqlDbType.Decimal).Value = this.amount;
                    this.sql.result.Parameters.Add("@czy_anonymous", SqlDbType.Bit).Value = this.czy_anonymous;
                    this.sql.result.Parameters.Add("@sponsor_id", SqlDbType.Int).Value = this.sponsor_id;
                    this.sql.result.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
        }
        #endregion

        //delete
        #region "delete"
        public void delete()
        {
            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_activity_delete", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@id", SqlDbType.Int).Value = this.id;
                    this.sql.result.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
        }
        #endregion

        //count
        #region "count"
        public int count()
        {
            int wynik = 0;

            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_activity", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(class_user.search_type.count);
                    this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = string.Empty;
                    using (this.sql.reader = this.sql.result.ExecuteReader())
                    {
                        while (this.sql.reader.Read())
                        {
                            wynik = (int)(this.sql.reader["count"]);
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }

            return wynik;
        }
        #endregion

        public void generate_list(search_type search_type, string search_string)
        {
            this.list = new List<class_activity>();

            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_activity", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(search_type);
                    this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = search_string;
                    using (this.sql.reader = this.sql.result.ExecuteReader())
                    {
                        while (this.sql.reader.Read())
                        {
                            class_activity activity = new class_activity
                            {
                                id = (int)(this.sql.reader["id"]),
                                active = (bool)(this.sql.reader["active"]),
                                user_app_id = (int)(this.sql.reader["user_app_id"]),
                                speed = (decimal)(this.sql.reader["speed"]),
                                distance = (decimal)(this.sql.reader["distance"]),
                                amount = (decimal)(this.sql.reader["amount"]),
                                czy_anonymous = (bool)(this.sql.reader["czy_anonymous"]),
                                sponsor_id = (int)(this.sql.reader["sponsor_id"]),
                            };

                            //list
                            this.list.Add(activity);
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
        }

        public void generate_list_with_additional_data(search_type search_type, string search_string)
        {
            this.list_ext = new List<class_activity_extended>();

            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_activity", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(search_type);
                    this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = search_string;
                    using (this.sql.reader = this.sql.result.ExecuteReader())
                    {
                        while (this.sql.reader.Read())
                        {
                            class_sponsor sponsor = new class_sponsor((int)(this.sql.reader["sponsor_id"]));
                            class_user_app user_app = new class_user_app((int)(this.sql.reader["user_app_id"]));

                            class_activity_extended activity = new class_activity_extended
                            {
                                id = (int)(this.sql.reader["id"]),
                                active = (bool)(this.sql.reader["active"]),
                                user_app_id = (int)(this.sql.reader["user_app_id"]),
                                speed = (decimal)(this.sql.reader["speed"]),
                                distance = (decimal)(this.sql.reader["distance"]),
                                amount = (decimal)(this.sql.reader["amount"]),
                                czy_anonymous = (bool)(this.sql.reader["czy_anonymous"]),
                                sponsor_id = (int)(this.sql.reader["sponsor_id"]),
                                sponsor_logo = sponsor.logo_link,
                                sponsor_name = sponsor.name,
                                user_name = user_app.name,
                                user_surname = user_app.surname,
                                user_email = user_app.email
                            };

                            //list
                            this.list_ext.Add(activity);
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
        }

        //total amount
        #region "total amount"
        public decimal total_amount(string search_string)
        {
            decimal wynik = 0;

            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_activity", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(search_type.sponsor_amount);
                    this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = search_string;
                    using (this.sql.reader = this.sql.result.ExecuteReader())
                    {
                        while (this.sql.reader.Read())
                        {
                            wynik = (int)(this.sql.reader["count"]);
                            wynik = (decimal)(this.sql.reader["amount"]);
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }

            return wynik;
        }
        #endregion
    }

    public class class_activity_extended : class_activity
    {
        public string sponsor_logo;
        public string sponsor_name;
        public string user_name;
        public string user_surname;
        public string user_email;
    }

    public class class_activity_ws_return
    {
        public int id { get; set; }
    }
}
