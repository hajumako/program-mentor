﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//
using System.Data;
using System.Data.SqlClient;

namespace SfCms_model
{
	public class class_admin
	{
		//variables

		//other
		public enum search_type
		{
			all = 0,       //all
			id = 1,        //id
			active = 2     //active
		}

		//variables (class)
		private class_sql sql = new class_sql();

		//constructor
		public class_admin(int id)
		{
			this.id = id;

			if (this.id > 0)
			{
				this.get_data();
			}
		}
		public class_admin()
		{

		}

		//properties
		#region "properties"
		public int id { get; set; }
		public bool active { get; set; }
		public string login { get; set; }
		public string password { get; set; }
		#endregion

		//properties other
		#region "properties other"
		public List<class_admin> list { get; set; }
		#endregion

		//get_data
		#region "get_data"
		public void get_data()
		{
			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_admin", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(class_admin.search_type.id);
					this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = this.id.ToString();
					using (this.sql.reader = this.sql.result.ExecuteReader())
					{
						while (this.sql.reader.Read())
						{
							this.id = (int)(this.sql.reader["id"]);
							this.active = (bool)(this.sql.reader["active"]);
							this.login = (string)(this.sql.reader["login"]);
							this.password = (string)(this.sql.reader["password"]);
						}
					}
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}
		}
		#endregion

		//insert
		#region "insert"
		#endregion

		//update
		#region "update"
		#endregion

		//delete
		#region "delete"
		#endregion

		public void generate_list(search_type search_type, string search_string)
		{
			this.list = new List<class_admin>();

			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_admin", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(search_type);
					this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = search_string;
					using (this.sql.reader = this.sql.result.ExecuteReader())
					{
						while (this.sql.reader.Read())
						{
							class_admin admin = new class_admin
							{
								id = (int)(this.sql.reader["id"]),
								active = (bool)(this.sql.reader["active"]),
								login = (string)(this.sql.reader["login"]),
								password = (string)(this.sql.reader["password"])
							};

							//list
							this.list.Add(admin);
						}
					}
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}
		}

		public bool log_in(string login, string password)
		{
			bool wynik = false;

			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_admin_log_in", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@login", SqlDbType.VarChar).Value = login;
					this.sql.result.Parameters.Add("@password", SqlDbType.VarChar).Value = password;
					using (this.sql.reader = this.sql.result.ExecuteReader())
					{
						while (this.sql.reader.Read())
						{
							wynik = (bool)(this.sql.reader["wynik"]);
							this.id = (int)(this.sql.reader["admin_id"]);
						}
					}
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}

			return wynik;
		}
	}
}