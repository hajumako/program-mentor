﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//
using System.Data;
using System.Data.SqlClient;
using Microsoft.VisualBasic;

namespace SfCms_model
{
	public class class_project
	{
		//variables

		//other
		public enum search_type
		{
			all = 0,      //all
			id = 1,       //id
			active = 2,   //active
			count = 3,    //count
			name = 4      //name
		}

		//variables (class)
		private class_sql sql = new class_sql();

		//constructor
		public class_project(int id)
		{
			this.id = id;

			if (this.id > 0)
			{
				this.get_data();
			}
		}
		public class_project()
		{

		}

		//properties
		#region "properties"
		public int id { get; set; }
		public bool active { get; set; }
		public string name { get; set; }
		public string title { get; set; }
		public string content { get; set; }
		public DateTime date_start { get; set; }
		public DateTime date_stop { get; set; }
		public decimal amount { get; set; }
		#endregion

		//properties other
		#region "properties other"
		public int support { get; set; }
		public decimal support_amount { get; set; }
		public decimal support_percent { get; set; }
        public bool czy_support { get; set; }
		public int to_end { get; set; }
		public List<class_project> list { get; set; }
		public List<class_beneficiary> list_beneficiary { get; set; }
        public List<class_mentor> list_mentor { get; set; }
        public List<class_brick> list_brick { get; set; }
        public List<class_paylane> list_paylane { get; set; }
		#endregion

		//get_data
		#region "get_data"
		public void get_data()
		{
			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_project", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(class_project.search_type.id);
					this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = this.id.ToString();
					using (this.sql.reader = this.sql.result.ExecuteReader())
					{
						while (this.sql.reader.Read())
						{
							this.id = (int)(this.sql.reader["id"]);
							this.active = (bool)(this.sql.reader["active"]);
							this.name = (string)(this.sql.reader["name"]);
							this.title = (string)(this.sql.reader["title"]);
							this.content = (string)(this.sql.reader["content"]);
							this.date_start = (DateTime)(this.sql.reader["date_start"]);
							this.date_stop = (DateTime)(this.sql.reader["date_stop"]);
							this.amount = (decimal)(this.sql.reader["amount"]);
						}

						//support
                        this._get_support();
		
						//support_percent
						this.support_percent = (this.support_amount * 100)/this.amount;

						//to_end
                        this.to_end = this._get_to_end();

                        //czy_support
                        if (this.support_amount < this.amount)
                        {
                            this.czy_support = true;
                        }
                        else
                        {
                            this.czy_support = false;
                        }
					}
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}

            //generate_list_brick
            this.generate_list_brick();
		}
		#endregion

		//insert
		#region "insert"
		public void insert()
		{
			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();

                    //transaction - begin
                    this.sql.transaction = this.sql.connection.BeginTransaction();

					this.sql.result = new SqlCommand("sfcms_project_insert", this.sql.connection, this.sql.transaction);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@active", SqlDbType.Bit).Value = this.active;
					this.sql.result.Parameters.Add("@name", SqlDbType.VarChar).Value = this.name;
					this.sql.result.Parameters.Add("@title", SqlDbType.VarChar).Value = this.title;
					this.sql.result.Parameters.Add("@content", SqlDbType.VarChar).Value = this.content;
					this.sql.result.Parameters.Add("@date_start", SqlDbType.DateTime).Value = this.date_start;
					this.sql.result.Parameters.Add("@date_stop", SqlDbType.DateTime).Value = this.date_stop;
					this.sql.result.Parameters.Add("@amount", SqlDbType.Decimal).Value = this.amount;
					using (this.sql.reader = this.sql.result.ExecuteReader())
					{
						while (this.sql.reader.Read())
						{
							this.id = (int)(this.sql.reader["id"]);
						}
					}

                    ////brick
                    //class_brick brick;

                    ////brick(s_brick_id = 1)
                    //brick = new class_brick();
                    //brick.active = true;
                    //brick.project_id = this.id;
                    //brick.s_brick_id = 1;
                    //brick.content = string.Empty;
                    //brick.insert(this.sql.connection, this.sql.transaction);

                    ////brick(s_brick_id = 2)
                    //brick = new class_brick();
                    //brick.active = true;
                    //brick.project_id = this.id;
                    //brick.s_brick_id = 2;
                    //brick.content = string.Empty;
                    //brick.insert(this.sql.connection, this.sql.transaction);

                    ////brick(s_brick_id = 3)
                    //brick = new class_brick();
                    //brick.active = true;
                    //brick.project_id = this.id;
                    //brick.s_brick_id = 3;
                    //brick.content = string.Empty;
                    //brick.insert(this.sql.connection, this.sql.transaction);

                    ////brick(s_brick_id = 4)
                    //brick = new class_brick();
                    //brick.active = true;
                    //brick.project_id = this.id;
                    //brick.s_brick_id = 4;
                    //brick.content = string.Empty;
                    //brick.insert(this.sql.connection, this.sql.transaction);

                    //transaction - commit
                    this.sql.transaction.Commit();
				}
				catch (Exception ex)
				{
                    this.sql.transaction.Rollback();

					this.sql.ex = ex.Message;
				}
			}
		}
		#endregion

		//update
		#region "update"
		public void update()
		{
			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_project_update", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@id", SqlDbType.Int).Value = this.id;
					this.sql.result.Parameters.Add("@active", SqlDbType.Bit).Value = this.active;
					this.sql.result.Parameters.Add("@name", SqlDbType.VarChar).Value = this.name;
					this.sql.result.Parameters.Add("@title", SqlDbType.VarChar).Value = this.title;
					this.sql.result.Parameters.Add("@content", SqlDbType.VarChar).Value = this.content;
					this.sql.result.Parameters.Add("@date_start", SqlDbType.DateTime).Value = this.date_start;
					this.sql.result.Parameters.Add("@date_stop", SqlDbType.DateTime).Value = this.date_stop;
					this.sql.result.Parameters.Add("@amount", SqlDbType.Decimal).Value = this.amount;
					this.sql.result.ExecuteNonQuery();
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}
		}
		#endregion

		//delete
		#region "delete"
		public void delete()
		{
			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_project_delete", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@id", SqlDbType.Int).Value = this.id;
					this.sql.result.ExecuteNonQuery();
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}
		}
		#endregion

		//count
		#region "count"
		public int count()
		{
			int wynik = 0;

			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_project", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(class_news.search_type.count);
					this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = string.Empty;
					using (this.sql.reader = this.sql.result.ExecuteReader())
					{
						while (this.sql.reader.Read())
						{
							wynik = (int)(this.sql.reader["count"]);
						}
					}
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}

			return wynik;
		}
		#endregion

		public void generate_list(search_type search_type, string search_string)
		{
			this.list = new List<class_project>();

			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_project", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(search_type);
					this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = search_string;
					using (this.sql.reader = this.sql.result.ExecuteReader())
					{
						while (this.sql.reader.Read())
						{
							class_project project = new class_project
							{
								id = (int)(this.sql.reader["id"]),
								active = (bool)(this.sql.reader["active"]),
								name = (string)(this.sql.reader["name"]),
								title = (string)(this.sql.reader["title"]),
								content = (string)(this.sql.reader["content"]),
								date_start = (DateTime)(this.sql.reader["date_start"]),
								date_stop = (DateTime)(this.sql.reader["date_stop"]),
								amount = (decimal)(this.sql.reader["amount"])
							};

                            //support
                            project._get_support();

                            //support_percent
                            project.support_percent = Math.Ceiling((project.support_amount * 100) / project.amount);

                            //to_end
                            project.to_end = project._get_to_end();

                            //czy_support
                            if (project.support_amount < project.amount)
                            {
                                project.czy_support = true;
                            }
                            else
                            {
                                project.czy_support = false;
                            }

							//list
							this.list.Add(project);
						}
					}
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}

            //generate_list_brick
            for (var i = 0; i < this.list.Count(); i++)
            {
                this.list[i].generate_list_brick();
            }
		}

		public void generate_list_beneficiary()
		{
			this.list_beneficiary = new List<class_beneficiary>();

			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_beneficiary", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(class_beneficiary.search_type.project_id);
					this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = this.id.ToString();
					using (this.sql.reader = this.sql.result.ExecuteReader())
					{
						while (this.sql.reader.Read())
						{
							class_beneficiary beneficiary = new class_beneficiary((int)(this.sql.reader["id"]));

							//list
							this.list_beneficiary.Add(beneficiary);
						}
					}
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}
		}

        public void generate_list_mentor()
        {
            this.list_mentor = new List<class_mentor>();

            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_mentor", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(class_mentor.search_type.project_id);
                    this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = this.id.ToString();
                    using (this.sql.reader = this.sql.result.ExecuteReader())
                    {
                        while (this.sql.reader.Read())
                        {
                            class_mentor mentor = new class_mentor((int)(this.sql.reader["id"]));

                            //list
                            this.list_mentor.Add(mentor);
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
        }

        public void generate_list_brick()
        {
            this.list_brick = new List<class_brick>();

            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_brick", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(class_brick.search_type.project_id);
                    this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = this.id.ToString();
                    using (this.sql.reader = this.sql.result.ExecuteReader())
                    {
                        while (this.sql.reader.Read())
                        {
                            class_brick brick = new class_brick((int)(this.sql.reader["id"]));

                            //list
                            this.list_brick.Add(brick);
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
        }

        public int _get_to_end()
        {
            int wynik = 0;

            DateTime today = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

            wynik = Convert.ToInt32(DateAndTime.DateDiff(DateInterval.Day, today, this.date_stop).ToString());

            if (wynik < 0)
            {
                wynik = 0;
            }
            else
            {
                wynik = wynik + 1;
            }

            return wynik;
        }

        public void _get_support()
        {
            int support = 0;
            decimal support_amount = 0;
            decimal support_percent = 0;

            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_paylane", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(class_paylane.search_type.support);
                    this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = this.id.ToString();
                    using (this.sql.reader = this.sql.result.ExecuteReader())
                    {
                        while (this.sql.reader.Read())
                        {
                            support = (int)(this.sql.reader["support"]);
                            support_amount = (decimal)(this.sql.reader["support_amount"]);
                        }

                        //support
                        this.support = support;

                        //support_amount
                        this.support_amount = support_amount;
                    }
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }

            support_percent = 0; Math.Ceiling((this.support_amount * 100) / this.amount);

            this.support_percent = support_percent;
        }
	}
}