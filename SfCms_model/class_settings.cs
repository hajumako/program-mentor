﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//
using System.Data;
using System.Data.SqlClient;

namespace SfCms_model
{
	public class class_settings
	{
		//variables

		//other
		public enum search_type
		{
			all = 0,  // - bez szukania
		}
        public enum phrase
        {
            version = 1,              //version
			name = 2,                 //name
            host_admin = 3,           //host_admin
            host_user = 4,            //host_user
            host_ws = 5,              //host_ws
            smtp_credentials = 6,     //smtp_credentials
            smtp_host = 7,            //smtp_host
            smtp_port = 8,            //smtp_port
            smtp_user = 9,            //smtp_user
            smtp_password = 10,       //smtp_password
            smtp_timeout = 11,        //smtp_timeout
            paylane_back_url = 12,    //paylane_back_url
            facebook_app_id = 13,     //facebook_app_id
            facebook_app_secret = 14, //facebook_app_secret
            image_large = 15,         //image_large
            image_medium = 16,        //image_medium
            image_small = 17          //image_small
        }

		//variables (class)
		private class_sql sql = new class_sql();

		//constructor
		public class_settings()
		{
            this.get_data();
		}

		//properties
		#region "properties"
        public string version { get; set; }
		public string name { get; set; }
        public string host_admin { get; set; }
        public string host_user { get; set; }
        public string host_ws { get; set; }
        public bool smtp_credentials { get; set; }
        public string smtp_host { get; set; }
        public int smtp_port { get; set; }
        public string smtp_user { get; set; }
        public string smtp_password { get; set; }
        public int smtp_timeout { get; set; }
        public string paylane_back_url { get; set; }
        public string facebook_app_id { get; set; }
        public string facebook_app_secret { get; set; }
        public int image_large { get; set; }
        public int image_medium { get; set; }
        public int image_small { get; set; }
		#endregion

		//properties other
		#region "properties other"
        private ds_db.settingsDataTable settings_db { get; set; }
		#endregion

		//get_data
		#region "get_data"
		public void get_data()
		{
            ds_db ds_db = new ds_db();

            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_settings", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(class_settings.search_type.all);

                    this.sql.adapter = new SqlDataAdapter(this.sql.result);
                    this.sql.adapter.Fill(ds_db.settings);

                    this.settings_db = ds_db.settings;

                    //properties
                    this.version = this.get_phrase(phrase.version);
					this.name = this.get_phrase(phrase.name);
                    this.host_admin = this.get_phrase(phrase.host_admin);
                    this.host_user = this.get_phrase(phrase.host_user);
                    this.host_ws = this.get_phrase(phrase.host_ws);
                    this.smtp_credentials = Convert.ToBoolean(this.get_phrase(phrase.smtp_credentials));
                    this.smtp_host = this.get_phrase(phrase.smtp_host);
                    this.smtp_port = Convert.ToInt32(this.get_phrase(phrase.smtp_port));
                    this.smtp_user = this.get_phrase(phrase.smtp_user);
                    this.smtp_password = this.get_phrase(phrase.smtp_password);
                    this.smtp_timeout = Convert.ToInt32(this.get_phrase(phrase.smtp_timeout));
                    this.paylane_back_url = this.get_phrase(phrase.paylane_back_url);
                    this.facebook_app_id = this.get_phrase(phrase.facebook_app_id);
                    this.facebook_app_secret = this.get_phrase(phrase.facebook_app_secret);
                    this.image_large = Convert.ToInt32(this.get_phrase(phrase.image_large));
                    this.image_medium = Convert.ToInt32(this.get_phrase(phrase.image_medium));
                    this.image_small = Convert.ToInt32(this.get_phrase(phrase.image_small));
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
		}
		#endregion

        private string get_phrase(phrase phrase)
        {
            string wynik = string.Empty;

            DataRow[] settings_db_row = this.settings_db.Select("id = " + (int)phrase + "");

            if (settings_db_row.Length != 0)
            {
                wynik = settings_db_row[0]["value"].ToString();
            }

            return wynik;
        }
	}
}