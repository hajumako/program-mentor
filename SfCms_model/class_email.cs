﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;

namespace SfCms_model
{
    public class class_email
    {
        //variables

        //other
        public enum search_type
        {
            all = 0,      //all
            id = 1,       //id
            active = 2,   //active
            count = 3     //count
        }

        //variables (class)
        private class_sql sql = new class_sql();

        //constructor
        public class_email()
        {

        }

        //properties
        #region "properties"
        public int id { get; set; }
        public bool active { get; set; }
        public string title { get; set; }
        public string content { get; set; }
        public DateTime date_user { get; set; }
        public DateTime date_admin { get; set; }
        #endregion

        //properties other
        #region "properties other"
        public List<class_news> list { get; set; }
        #endregion

        public bool send(string to,string subject, string body)
        {
            bool wynik = false;

            //settings
            class_settings settings = new class_settings();

            //email
            MailMessage email = new MailMessage();
            email.From = new MailAddress(settings.smtp_user, "Program Mentor");
            email.To.Add(to);
            email.Subject = subject;
            email.Body = body;
            email.IsBodyHtml = true;

            //smtp
            SmtpClient smtp = new SmtpClient(settings.smtp_host);

            if (settings.smtp_credentials)
            {
                smtp.Port = settings.smtp_port;
                smtp.EnableSsl = true;
                smtp.Timeout = settings.smtp_timeout;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential(settings.smtp_user, settings.smtp_password);
            }

            try
            {
                smtp.Send(email);

                //wynik
                wynik = true;
            }
            catch (Exception ex)
            {
                //wynik
                wynik = false;
            }

            return wynik;
        }
    }
}