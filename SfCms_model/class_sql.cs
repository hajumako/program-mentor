﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//
using System.Data.SqlClient;
using System.Configuration;

namespace SfCms_model
{
	public class class_sql
	{
		//variables
		private string _connection_string = string.Empty;
		private SqlConnection _connection;
		private SqlTransaction _transaction;
		private SqlCommand _command;
		private SqlCommand _result = new SqlCommand();
		private SqlDataReader _reader;
		private SqlDataAdapter _adapter;
		private string _query = string.Empty;
		private DateTime _data_default = new DateTime(1900, 1, 1);
		private string _ex = string.Empty;

		//other

		//variables (class)

		//constructor
		public class_sql()
		{

		}

		//properties
		#region "properties"
		public string connection_string
		{
			get
			{
				return this.get_appsetting("connection_string");
			}
			set
			{
				this._connection_string = value;
			}
		}
		public SqlConnection connection
		{
			get
			{
				return this._connection;
			}
			set
			{
				this._connection = value;
			}
		}
		public SqlTransaction transaction
		{
			get
			{
				return this._transaction;
			}
			set
			{
				this._transaction = value;
			}
		}
		public SqlCommand command
		{
			get
			{
				return this._command;
			}
			set
			{
				this._command = value;
			}
		}
		public SqlCommand result
		{
			get
			{
				return this._result;
			}
			set
			{
				this._result = value;
			}
		}
		public SqlDataReader reader
		{
			get
			{
				return this._reader;
			}
			set
			{
				this._reader = value;
			}
		}
		public SqlDataAdapter adapter
		{
			get
			{
				return this._adapter;
			}
			set
			{
				this._adapter = value;
			}
		}
		public string query
		{
			get
			{
				return this._query;
			}
			set
			{
				this._query = value;
			}
		}
		public DateTime data_default
		{
			get
			{
				return this._data_default;
			}
		}
		public string ex
		{
			get
			{
				return this._ex;
			}
			set
			{
				this._ex = value;
			}
		}
		#endregion

		//properties other
		#region "properties other"
		#endregion

		//get_data
		#region "get_data"
		private void get_data()
		{
		}
		#endregion

		public string get_appsetting(string name)
		{
			string wynik = string.Empty;

			AppSettingsReader app_config = new AppSettingsReader();
			wynik = (String)app_config.GetValue(name, typeof(string));

			return wynik;
		}
	}
}