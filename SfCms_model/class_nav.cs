﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//
using System.Data;
using System.Data.SqlClient;

namespace SfCms_model
{
	public class class_nav
	{
		//variables

		//other

		//variables (class)
		private class_sql sql = new class_sql();

		//constructor
		public class_nav()
		{
			this.get_data();
		}

		//properties
		#region "properties"
		public string info { get; set; }					//info walidacji
		public bool success { get; set; }					//czy post zakończył się sukcesem(logicznym)
		public bool post { get; set; }						//czy żądanie pochodzi z wsyłania formularza
		public bool modal_login { get; set; }				//czy pokazać modal form_login
        public bool modal_login_brick { get; set; }			//czy pokazać modal form_login_brick
		public bool modal_register { get; set; }			//czy pokazać modal form_register
		public bool modal_password_reset { get; set; }		//czy pokazać modal: form_password_reset
		public bool validation_login { get; set; }			//czy walidacja formularza form_login = true()
        public bool validation_login_brick { get; set; }	//czy walidacja formularza form_login_brick = true()
		public bool validation_register { get; set; }		//czy walidacja formularza form_register = true()
		public bool validation_password_reset { get; set; } //czy walidacja formularza form_password_reset = true()
        public bool register { get; set; }		            //czy wykonanie register = true()
		public bool password_reset { get; set; }            //czy wykonanie password_reset = true()
		#endregion

		//properties other
		#region "properties other"
		#endregion

		//get_data
		#region "get_data"
		public void get_data()
		{
			this.post = false;
			this.modal_login = false;
            this.modal_login_brick = false;
			this.modal_register = false;
			this.modal_password_reset = false;
			this.validation_login = false;
            this.validation_login_brick = false;
			this.validation_register = false;
			this.validation_password_reset = false;
		}
		#endregion
	}
}