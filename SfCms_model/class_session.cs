﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//
using System.Data;
using System.Data.SqlClient;

namespace SfCms_model
{
	public class session
	{
        //variables

        //other

        //variables (class)

        //constructor
        private session()
        {

        }

        //properties
        #region "properties"
        public static string admin_id //uwierzytelnienie (SfCms_admin)
        {
            get
            {
                return HttpContext.Current.Session["admin_id"] as string;
            }
            set
            {
                HttpContext.Current.Session["admin_id"] = value;
            }
        }
        public static string beneficiary_id //uwierzytelnienie (SfCms_user - blog)
        {
            get
            {
                return HttpContext.Current.Session["beneficiary_id"] as string;
            }
            set
            {
                HttpContext.Current.Session["beneficiary_id"] = value;
            }
        }
        public static string user_id //uwierzytelnienie (SfCms_user - paylane)
        {
            get
            {
                return HttpContext.Current.Session["user_id"] as string;
            }
            set
            {
                HttpContext.Current.Session["user_id"] = value;
            }
        }
        public static string czy_fb_error //(SfCms_user - paylane, czy błąd logowania FB)
        {
            get
            {
                return HttpContext.Current.Session["czy_fb_error"] as string;
            }
            set
            {
                HttpContext.Current.Session["czy_fb_error"] = value;
            }
        }
        public static string project_id //(SfCms_user - paylane, back url - powrót do projektu)
        {
            get
            {
                return HttpContext.Current.Session["project_id"] as string;
            }
            set
            {
                HttpContext.Current.Session["project_id"] = value;
            }
        }
        public static string czy_anonymous //(SfCms_user - paylane, back url - czy wsparcie anonimowe)
        {
            get
            {
                return HttpContext.Current.Session["czy_anonymous"] as string;
            }
            set
            {
                HttpContext.Current.Session["czy_anonymous"] = value;
            }
        }
        public static string paylane_result
        {
            get
            {
                return HttpContext.Current.Session["czy_paylane_error"] as string;
            }
            set
            {
                HttpContext.Current.Session["czy_paylane_error"] = value;
            }
        }
        #endregion

        //properties other
        #region "properties other"
        #endregion

        //get_data
        #region "get_data"
        #endregion
	}
}