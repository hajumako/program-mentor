﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//
using System.Data;
using System.Data.SqlClient;

namespace SfCms_model
{
    public class class_news
    {
        //variables

        //other
        public enum search_type
        {
            all = 0,      //all
            id = 1,       //id
            active = 2,   //active
			count = 3     //count
        }

        //variables (class)
        private class_sql sql = new class_sql();

        //constructor
        public class_news(int id)
        {
            this.id = id;

            if (this.id > 0)
            {
                this.get_data();
            }
        }
		public class_news()
		{

		}

        //properties
        #region "properties"
        public int id { get; set; }
        public bool active { get; set; }
        public string title { get; set; }
        public string content { get; set; }
        public DateTime date_user { get; set; }
        public DateTime date_admin { get; set; }
        public int s_news_id { get; set; }
		public string logo_link { get; set; }
        #endregion

        //properties other
        #region "properties other"
		public List<class_news> list { get; set; }
        public string s_news_id_dictionary
        {
            get
            {
                string wynik = string.Empty;

                class_s_news s_news = new class_s_news(this.s_news_id);
                wynik = s_news.name;

                return wynik;
            }
        }
        #endregion

        //get_data
        #region "get_data"
        public void get_data()
        {
            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_news", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(class_news.search_type.id);
                    this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = this.id.ToString();
                    using (this.sql.reader = this.sql.result.ExecuteReader())
                    {
                        while (this.sql.reader.Read())
                        {
                            this.id = (int)(this.sql.reader["id"]);
                            this.active = (bool)(this.sql.reader["active"]);
                            this.title = (string)(this.sql.reader["title"]);
                            this.content = (string)(this.sql.reader["content"]);
                            this.date_user = (DateTime)(this.sql.reader["date_user"]);
                            this.date_admin = (DateTime)(this.sql.reader["date_admin"]);
                            this.s_news_id = (int)(this.sql.reader["s_news_id"]);
							this.logo_link = (string)(this.sql.reader["logo_link"]);
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
        }
        #endregion

        //insert
        #region "insert"
        public void insert()
        {
            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_news_insert", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@active", SqlDbType.Bit).Value = this.active;
                    this.sql.result.Parameters.Add("@title", SqlDbType.VarChar).Value = this.title;
                    this.sql.result.Parameters.Add("@content", SqlDbType.VarChar).Value = this.content;
                    this.sql.result.Parameters.Add("@date_user", SqlDbType.DateTime).Value = this.date_user;
                    this.sql.result.Parameters.Add("@s_news_id", SqlDbType.Int).Value = this.s_news_id;
					this.sql.result.Parameters.Add("@logo_link", SqlDbType.VarChar).Value = this.logo_link;
					using (this.sql.reader = this.sql.result.ExecuteReader())
                    {
                        while (this.sql.reader.Read())
                        {
                            this.id = (int)(this.sql.reader["id"]);
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
        }
        #endregion

        //update
        #region "update"
        public void update()
        {
            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_news_update", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@id", SqlDbType.Int).Value = this.id;
                    this.sql.result.Parameters.Add("@active", SqlDbType.Bit).Value = this.active;
                    this.sql.result.Parameters.Add("@title", SqlDbType.VarChar).Value = this.title;
                    this.sql.result.Parameters.Add("@content", SqlDbType.VarChar).Value = this.content;
                    this.sql.result.Parameters.Add("@date_user", SqlDbType.DateTime).Value = this.date_user;
                    this.sql.result.Parameters.Add("@s_news_id", SqlDbType.Int).Value = this.s_news_id;
					this.sql.result.Parameters.Add("@logo_link", SqlDbType.VarChar).Value = this.logo_link;
                    this.sql.result.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
        }
        #endregion

        //delete
        #region "delete"
        public void delete()
        {
            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_news_delete", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@id", SqlDbType.Int).Value = this.id;
                    this.sql.result.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
        }
        #endregion

		//count
		#region "count"
		public int count()
		{
			int wynik = 0;

			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_news", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(class_news.search_type.count);
					this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = string.Empty;
					using (this.sql.reader = this.sql.result.ExecuteReader())
					{
						while (this.sql.reader.Read())
						{
							wynik = (int)(this.sql.reader["count"]);
						}
					}
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}

			return wynik;
		}
		#endregion

		public void generate_list(search_type search_type, string search_string)
		{
			this.list = new List<class_news>();

			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_news", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(search_type);
					this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = search_string;
					using (this.sql.reader = this.sql.result.ExecuteReader())
					{
						while (this.sql.reader.Read())
						{
							class_news news = new class_news
							{
								id = (int)(this.sql.reader["id"]),
								active = (bool)(this.sql.reader["active"]),
								title = (string)(this.sql.reader["title"]),
								content = (string)(this.sql.reader["content"]),
								date_user = (DateTime)(this.sql.reader["date_user"]),
								date_admin = (DateTime)(this.sql.reader["date_admin"]),
                                s_news_id = (int)(this.sql.reader["s_news_id"]),
								logo_link = (string)(this.sql.reader["logo_link"])
							};

							//list
							this.list.Add(news);
						}
					}
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}
		}
    }
}