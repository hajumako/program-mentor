﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//
using System.Data;
using System.Data.SqlClient;

namespace SfCms_model
{
    public class class_s_news
    {
        //variables

        //other
        public enum search_type
        {
            all = 0, // - bez szukania
            id = 1   // - id
        }

        //variables (class)
        private class_sql sql = new class_sql();

        //constructor
        public class_s_news(int id)
        {
            this.id = id;

            if (this.id > 0)
            {
                this.get_data();
            }
        }
        public class_s_news()
        {

        }

        //properties
        #region "properties"
        public int id { get; set; }
        public int value { get; set; }
        public bool active { get; set; }
        public int sequence { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        #endregion

        //properties other
        #region "properties other"
        #endregion

        //get_data
        #region "get_data"
        public void get_data()
        {
            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_s_news", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(class_s_news.search_type.id);
                    this.sql.result.Parameters.Add("@search", SqlDbType.VarChar).Value = this.id.ToString();
                    using (this.sql.reader = this.sql.result.ExecuteReader())
                    {
                        while (this.sql.reader.Read())
                        {
                            this.id = (int)(this.sql.reader["id"]);
                            this.value = (int)(this.sql.reader["value"]);
                            this.active = (bool)(this.sql.reader["active"]);
                            this.sequence = (int)(this.sql.reader["sequence"]);
                            this.name = (string)(this.sql.reader["name"]);
                            this.description = (string)(this.sql.reader["description"]);
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
        }
        #endregion
    }
}