﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SfCms_model
{
	public class AuthorizeSfCms_admin : AuthorizeAttribute
	{
		public AuthorizeSfCms_admin()
		{

		}

		protected override bool AuthorizeCore(HttpContextBase httpContext)
		{
			bool authorize = false;

			if (HttpContext.Current.Session["admin_id"] != null)
			{
				authorize = true;
			}

			return authorize;
		}

		protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
		{
			filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Account", action = "LogIn" }));
		}
	}
}
