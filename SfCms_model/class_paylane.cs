﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Text;
using Microsoft.VisualBasic;
using System.IO;
using System.Net;
using Newtonsoft.Json;

namespace SfCms_model
{
	public class class_paylane
	{
		//variables

		//other
        public enum search_type
        {
            id = 0,      //id
            support = 1, //project_id
            donor = 2,    //donor
            donor_all = 3
        }

        //variables (class)
        private class_sql sql = new class_sql();
        class_tool tool = new class_tool();

		//constructor
        public class_paylane(int id)
		{
			this.id = id;

            if (this.id > 0)
            {
                this.get_data();
            }
            else
            {
                this.set_data();
            }
		}
		public class_paylane()
		{
            this.set_data();
		}

		//properties
		#region "properties"
        public int id { get; set; }
		public int project_id { get; set; }
        public int user_id { get; set; }
        public String facebook_user_id { get; set; }
        public String facebook_user_pic { get; set; }
		public DateTime date_request { get; set; }
        public DateTime date_response { get; set; }
        public bool czy_anonymous { get; set; }
		//request
		public string request_merchant_id { get; set; } //32
		public string request_description { get; set; } //2-20
		public string request_transaction_description { get; set; } //10000
		public decimal request_amount { get; set; } //12.2
		public string request_currency { get; set; } //3
		public string request_transaction_type { get; set; } //1
		public string request_back_url { get; set; } //500
		public string request_hash { get; set; } //40
		public string request_language { get; set; } //2
		public string request_customer_name { get; set; } //50
		public string request_customer_email { get; set; } //80
		public string request_customer_address { get; set; } //46
		public string request_customer_zip { get; set; } //9
		public string request_customer_city { get; set; } //40
		public string request_customer_state { get; set; } //40
		public string request_customer_country { get; set; } //2
		//response
		public string response_status { get; set; }
			//PENDING – płatność oczekuje na realizację;
			//PERFORMED – płatność została pomyślnie zrealizowana;
			//CLEARED – środki zostały pobrane (otrzymano potwierdzenie z banku);
			//ERROR – płatność nie powiodła się.
		public string response_description { get; set; } //20
		public decimal response_amount { get; set; } //12,2
		public string response_currency { get; set; } //3
		public string response_hash { get; set; } //40
		public int response_id_authorization { get; set; } //10
		public int response_id_sale { get; set; } //10
		public int response_id_error { get; set; } //10
		public int response_error_code { get; set; } //3
		public string response_error_text { get; set; } //500
		public decimal response_fraud_score { get; set; } //4,2
		public string response_avs_result { get; set; } //2
		#endregion

		//properties other
		#region "properties other"
        public string salt = string.Empty;
        public List<class_paylane> list { get; set; }
		#endregion

        //get_data
        #region "get_data"
        public void get_data()
        {
            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_paylane", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(class_paylane.search_type.id);
                    this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = this.id.ToString();
                    using (this.sql.reader = this.sql.result.ExecuteReader())
                    {
                        while (this.sql.reader.Read())
                        {
                            this.id = (int)(this.sql.reader["id"]);
		                    this.project_id = (int)(this.sql.reader["project_id"]);
                            this.user_id = (int)(this.sql.reader["user_id"]);
                            this.date_request = (DateTime)(this.sql.reader["date_request"]);
                            this.date_response = (DateTime)(this.sql.reader["date_response"]);
                            this.czy_anonymous = (bool)(this.sql.reader["czy_anonymous"]);
		                    //request
                            this.request_merchant_id = (string)(this.sql.reader["request_merchant_id"]);
                            this.request_description = (string)(this.sql.reader["request_description"]);
                            this.request_transaction_description = (string)(this.sql.reader["request_transaction_description"]);
                            this.request_amount = (decimal)(this.sql.reader["request_amount"]);
                            this.request_currency = (string)(this.sql.reader["request_currency"]);
                            this.request_transaction_type = (string)(this.sql.reader["request_transaction_type"]);
                            this.request_back_url = (string)(this.sql.reader["request_back_url"]);
                            this.request_hash = (string)(this.sql.reader["request_hash"]);
                            this.request_language = (string)(this.sql.reader["request_language"]);
                            this.request_customer_name = (string)(this.sql.reader["request_customer_name"]);
                            this.request_customer_email = (string)(this.sql.reader["request_customer_email"]);
                            this.request_customer_address = (string)(this.sql.reader["request_customer_address"]);
                            this.request_customer_zip = (string)(this.sql.reader["request_customer_zip"]);
                            this.request_customer_city = (string)(this.sql.reader["request_customer_city"]);
                            this.request_customer_state = (string)(this.sql.reader["request_customer_state"]);
                            this.request_customer_country = (string)(this.sql.reader["request_customer_country"]);
		                    //response
                            this.response_status = (string)(this.sql.reader["response_status"]);
                            this.response_description = (string)(this.sql.reader["response_description"]);
                            this.response_amount = (decimal)(this.sql.reader["response_amount"]);
                            this.response_currency = (string)(this.sql.reader["response_currency"]);
                            this.response_hash = (string)(this.sql.reader["response_hash"]);
                            this.response_id_authorization = (int)(this.sql.reader["response_id_authorization"]);
                            this.response_id_sale = (int)(this.sql.reader["response_id_sale"]);
                            this.response_id_error = (int)(this.sql.reader["response_id_error"]);
                            this.response_error_code = (int)(this.sql.reader["response_error_code"]);
                            this.response_error_text = (string)(this.sql.reader["response_error_text"]);
                            this.response_fraud_score = (decimal)(this.sql.reader["response_fraud_score"]);
                            this.response_avs_result = (string)(this.sql.reader["response_avs_result"]);
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
        }
        #endregion

        //set_data
        #region "set_data"
        public void set_data()
        {
            this.id = 0;
            this.project_id = 0;
            this.user_id = 0;
            this.date_request = this.sql.data_default;
            this.date_response = this.sql.data_default;
            this.czy_anonymous = false;
            //request
            this.request_merchant_id = string.Empty;
            this.request_description = string.Empty;
            this.request_transaction_description = string.Empty;
            this.request_amount = 0;
            this.request_currency = string.Empty;
            this.request_transaction_type = string.Empty;
            this.request_back_url = string.Empty;
            this.request_hash = string.Empty;
            this.request_language = string.Empty;
            this.request_customer_name = string.Empty;
            this.request_customer_email = string.Empty;
            this.request_customer_address = string.Empty;
            this.request_customer_zip = string.Empty;
            this.request_customer_city = string.Empty;
            this.request_customer_state = string.Empty;
            this.request_customer_country = string.Empty;
            //response
            this.response_status = string.Empty;
            this.response_description = string.Empty;
            this.response_amount = 0;
            this.response_currency = string.Empty;
            this.response_hash = string.Empty;
            this.response_id_authorization = 0;
            this.response_id_sale = 0;
            this.response_id_error = 0;
            this.response_error_code = 0;
            this.response_error_text = string.Empty;
            this.response_fraud_score = 0;
            this.response_avs_result = string.Empty;
        }
        #endregion

        //insert
        #region "insert"
        public void insert()
        {
            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_paylane_insert", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@project_id", SqlDbType.Int).Value = this.project_id;
                    this.sql.result.Parameters.Add("@user_id", SqlDbType.Int).Value = this.user_id;
	                this.sql.result.Parameters.Add("@date_request", SqlDbType.DateTime).Value = this.date_request;
	                this.sql.result.Parameters.Add("@date_response", SqlDbType.DateTime).Value = this.date_response;
                    this.sql.result.Parameters.Add("@czy_anonymous", SqlDbType.Bit).Value = this.czy_anonymous;
	                //---------------------------------
	                this.sql.result.Parameters.Add("@request_merchant_id", SqlDbType.VarChar).Value = this.request_merchant_id;
	                this.sql.result.Parameters.Add("@request_description", SqlDbType.VarChar).Value = this.request_description;
	                this.sql.result.Parameters.Add("@request_transaction_description", SqlDbType.VarChar).Value = this.request_transaction_description;
	                this.sql.result.Parameters.Add("@request_amount", SqlDbType.Decimal).Value = this.request_amount;
	                this.sql.result.Parameters.Add("@request_currency", SqlDbType.VarChar).Value = this.request_currency;
	                this.sql.result.Parameters.Add("@request_transaction_type", SqlDbType.Char).Value = this.request_transaction_type;
	                this.sql.result.Parameters.Add("@request_back_url", SqlDbType.VarChar).Value = this.request_back_url;
	                this.sql.result.Parameters.Add("@request_hash", SqlDbType.VarChar).Value = this.request_hash;
	                this.sql.result.Parameters.Add("@request_language", SqlDbType.VarChar).Value = this.request_language;
	                this.sql.result.Parameters.Add("@request_customer_name", SqlDbType.VarChar).Value = this.request_customer_name;
	                this.sql.result.Parameters.Add("@request_customer_email", SqlDbType.VarChar).Value = this.request_customer_email;
	                this.sql.result.Parameters.Add("@request_customer_address", SqlDbType.VarChar).Value = this.request_customer_address;
	                this.sql.result.Parameters.Add("@request_customer_zip", SqlDbType.VarChar).Value = this.request_customer_zip;
	                this.sql.result.Parameters.Add("@request_customer_city", SqlDbType.VarChar).Value = this.request_customer_city;
	                this.sql.result.Parameters.Add("@request_customer_state", SqlDbType.VarChar).Value = this.request_customer_state;
	                this.sql.result.Parameters.Add("@request_customer_country", SqlDbType.VarChar).Value = this.request_customer_country;
	                //---------------------------------
	                this.sql.result.Parameters.Add("@response_status", SqlDbType.VarChar).Value = this.response_status;
	                this.sql.result.Parameters.Add("@response_description", SqlDbType.VarChar).Value = this.response_description;
	                this.sql.result.Parameters.Add("@response_amount", SqlDbType.Decimal).Value = this.response_amount;
	                this.sql.result.Parameters.Add("@response_currency", SqlDbType.VarChar).Value = this.response_currency;
	                this.sql.result.Parameters.Add("@response_hash", SqlDbType.VarChar).Value = this.response_hash;
	                this.sql.result.Parameters.Add("@response_id_authorization", SqlDbType.Int).Value = this.response_id_authorization;
	                this.sql.result.Parameters.Add("@response_id_sale", SqlDbType.Int).Value = this.response_id_sale;
	                this.sql.result.Parameters.Add("@response_id_error", SqlDbType.Int).Value = this.response_id_error;
	                this.sql.result.Parameters.Add("@response_error_code", SqlDbType.Int).Value = this.response_error_code;
	                this.sql.result.Parameters.Add("@response_error_text", SqlDbType.VarChar).Value = this.response_error_text;
                    this.sql.result.Parameters.Add("@response_fraud_score", SqlDbType.Decimal).Value = this.response_fraud_score;
	                this.sql.result.Parameters.Add("@response_avs_result", SqlDbType.VarChar).Value = this.response_avs_result;
                    using (this.sql.reader = this.sql.result.ExecuteReader())
                    {
                        while (this.sql.reader.Read())
                        {
                            this.id = (int)(this.sql.reader["id"]);
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
        }
        #endregion

        //update
        #region "update"
        public void update()
        {
            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_paylane_update", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@id", SqlDbType.Int).Value = this.id;
                    this.sql.result.Parameters.Add("@project_id", SqlDbType.Int).Value = this.project_id;
                    this.sql.result.Parameters.Add("@user_id", SqlDbType.Int).Value = this.user_id;
                    this.sql.result.Parameters.Add("@date_request", SqlDbType.DateTime).Value = this.date_request;
                    this.sql.result.Parameters.Add("@date_response", SqlDbType.DateTime).Value = this.date_response;
                    this.sql.result.Parameters.Add("@czy_anonymous", SqlDbType.Bit).Value = this.czy_anonymous;
                    //---------------------------------
                    this.sql.result.Parameters.Add("@request_merchant_id", SqlDbType.VarChar).Value = this.request_merchant_id;
                    this.sql.result.Parameters.Add("@request_description", SqlDbType.VarChar).Value = this.request_description;
                    this.sql.result.Parameters.Add("@request_transaction_description", SqlDbType.VarChar).Value = this.request_transaction_description;
                    this.sql.result.Parameters.Add("@request_amount", SqlDbType.Decimal).Value = this.request_amount;
                    this.sql.result.Parameters.Add("@request_currency", SqlDbType.VarChar).Value = this.request_currency;
                    this.sql.result.Parameters.Add("@request_transaction_type", SqlDbType.Char).Value = this.request_transaction_type;
                    this.sql.result.Parameters.Add("@request_back_url", SqlDbType.VarChar).Value = this.request_back_url;
                    this.sql.result.Parameters.Add("@request_hash", SqlDbType.VarChar).Value = this.request_hash;
                    this.sql.result.Parameters.Add("@request_language", SqlDbType.VarChar).Value = this.request_language;
                    this.sql.result.Parameters.Add("@request_customer_name", SqlDbType.VarChar).Value = this.request_customer_name;
                    this.sql.result.Parameters.Add("@request_customer_email", SqlDbType.VarChar).Value = this.request_customer_email;
                    this.sql.result.Parameters.Add("@request_customer_address", SqlDbType.VarChar).Value = this.request_customer_address;
                    this.sql.result.Parameters.Add("@request_customer_zip", SqlDbType.VarChar).Value = this.request_customer_zip;
                    this.sql.result.Parameters.Add("@request_customer_city", SqlDbType.VarChar).Value = this.request_customer_city;
                    this.sql.result.Parameters.Add("@request_customer_state", SqlDbType.VarChar).Value = this.request_customer_state;
                    this.sql.result.Parameters.Add("@request_customer_country", SqlDbType.VarChar).Value = this.request_customer_country;
                    //---------------------------------
                    this.sql.result.Parameters.Add("@response_status", SqlDbType.VarChar).Value = this.response_status;
                    this.sql.result.Parameters.Add("@response_description", SqlDbType.VarChar).Value = this.response_description;
                    this.sql.result.Parameters.Add("@response_amount", SqlDbType.Decimal).Value = this.response_amount;
                    this.sql.result.Parameters.Add("@response_currency", SqlDbType.VarChar).Value = this.response_currency;
                    this.sql.result.Parameters.Add("@response_hash", SqlDbType.VarChar).Value = this.response_hash;
                    this.sql.result.Parameters.Add("@response_id_authorization", SqlDbType.Int).Value = this.response_id_authorization;
                    this.sql.result.Parameters.Add("@response_id_sale", SqlDbType.Int).Value = this.response_id_sale;
                    this.sql.result.Parameters.Add("@response_id_error", SqlDbType.Int).Value = this.response_id_error;
                    this.sql.result.Parameters.Add("@response_error_code", SqlDbType.Int).Value = this.response_error_code;
                    this.sql.result.Parameters.Add("@response_error_text", SqlDbType.VarChar).Value = this.response_error_text;
                    this.sql.result.Parameters.Add("@response_fraud_score", SqlDbType.Decimal).Value = this.response_fraud_score;
                    this.sql.result.Parameters.Add("@response_avs_result", SqlDbType.VarChar).Value = this.response_avs_result;
                    this.sql.result.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
        }
        #endregion

        //delete
        #region "delete"
        public void delete()
        {
            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_paylane_delete", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@id", SqlDbType.Int).Value = this.id;
                    this.sql.result.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
        }
        #endregion

        public void generate_list(search_type search_type, string search_string)
        {
            this.list = new List<class_paylane>();

            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_paylane", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(search_type);
                    this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = search_string;
                    using (this.sql.reader = this.sql.result.ExecuteReader())
                    {
                        while (this.sql.reader.Read())
                        {
                            class_user user = new class_user();
                            user.generate_list(class_user.search_type.id, (this.sql.reader["user_id"]).ToString());
                            String facebook_pic_url = "";

                            if (user.list[0].facebook_id.Length > 0)
                            {
                                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://graph.facebook.com/v2.4/" + user.list[0].facebook_id + "/picture/?redirect=false");
                                try
                                {
                                    WebResponse response = request.GetResponse();
                                    using (Stream responseStream = response.GetResponseStream())
                                    {
                                        StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                                        String r = reader.ReadToEnd();
                                        facebook_pic_url = JsonConvert.DeserializeObject<FacebookData>(r).data.url;
                                    }
                                }
                                catch (WebException ex)
                                {

                                }
                            }                            

                            class_paylane paylane = new class_paylane
                            {
                                id = (int)(this.sql.reader["id"]),
		                        project_id = (int)(this.sql.reader["project_id"]),
                                user_id = (int)(this.sql.reader["user_id"]),
                                facebook_user_id = user.list[0].facebook_id,
                                facebook_user_pic = facebook_pic_url,
                                date_request = (DateTime)(this.sql.reader["date_request"]),
                                date_response = (DateTime)(this.sql.reader["date_response"]),
                                czy_anonymous = (bool)(this.sql.reader["czy_anonymous"]),
		                        //request
                                request_merchant_id = (string)(this.sql.reader["request_merchant_id"]),
                                request_description = (string)(this.sql.reader["request_description"]),
                                request_transaction_description = (string)(this.sql.reader["request_transaction_description"]),
                                request_amount = (decimal)(this.sql.reader["request_amount"]),
                                request_currency = (string)(this.sql.reader["request_currency"]),
                                request_transaction_type = (string)(this.sql.reader["request_transaction_type"]),
                                request_back_url = (string)(this.sql.reader["request_back_url"]),
                                request_hash = (string)(this.sql.reader["request_hash"]),
                                request_language = (string)(this.sql.reader["request_language"]),
                                request_customer_name = (string)(this.sql.reader["request_customer_name"]),
                                request_customer_email = (string)(this.sql.reader["request_customer_email"]),
                                request_customer_address = (string)(this.sql.reader["request_customer_address"]),
                                request_customer_zip = (string)(this.sql.reader["request_customer_zip"]),
                                request_customer_city = (string)(this.sql.reader["request_customer_city"]),
                                request_customer_state = (string)(this.sql.reader["request_customer_state"]),
                                request_customer_country = (string)(this.sql.reader["request_customer_country"]),
		                        //response
                                response_status = (string)(this.sql.reader["response_status"]),
                                response_description = (string)(this.sql.reader["response_description"]),
                                response_amount = (decimal)(this.sql.reader["response_amount"]),
                                response_currency = (string)(this.sql.reader["response_currency"]),
                                response_hash = (string)(this.sql.reader["response_hash"]),
                                response_id_authorization = (int)(this.sql.reader["response_id_authorization"]),
                                response_id_sale = (int)(this.sql.reader["response_id_sale"]),
                                response_id_error = (int)(this.sql.reader["response_id_error"]),
                                response_error_code = (int)(this.sql.reader["response_error_code"]),
                                response_error_text = (string)(this.sql.reader["response_error_text"]),
                                response_fraud_score = (decimal)(this.sql.reader["response_fraud_score"]),
                                response_avs_result = (string)(this.sql.reader["response_avs_result"])

                            };

                            //list
                            this.list.Add(paylane);
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
        }

        private class FacebookData
        {
            public Data data { get; set; }
        }

        private class Data
        {
            public string is_silhouette { get; set; }
            public string url { get; set; }
        }

		private byte[] GetHash(string param)
		{
			byte[] wynik;

			HashAlgorithm algorithm = SHA1.Create();
			wynik = algorithm.ComputeHash(Encoding.UTF8.GetBytes(param));

			return wynik;
		}

		public string sha1(string salt, string description, decimal amount, string currency, string transaction_type)
		{
			string wynik = string.Empty;

			string param = string.Empty;
			param += salt + "|";
			param += description + "|";
			param += this.tool.format_decimal_dot(amount) + "|";
			param += currency + "|";
			param += transaction_type;

			StringBuilder string_builder = new StringBuilder();
			foreach (byte buffer in this.GetHash(param))
			{
				string_builder.Append(buffer.ToString("X2"));
			}

			wynik = string_builder.ToString().ToLower();

			return wynik;
		}

        public string time_from_response()
        {
            string wynik = string.Empty;
			int min = 0;

            min = Convert.ToInt32(DateAndTime.DateDiff(DateInterval.Minute, this.date_response, DateTime.Now).ToString());

			//minuty
			if (min >= 0 && min < 60)
			{
				wynik = min.ToString() + " min";
			}
			//godziny
			else if (min >= 60 && min < 1440)
			{
				wynik = Math.Floor(Convert.ToDouble(min / 60)).ToString() + " h";
			}
			//dni
			else if (min >= 1440 && min < 2880)
			{
				wynik = Math.Floor(Convert.ToDouble(min / 1440)).ToString() + " dzień";
			}
			else
			{
				wynik = Math.Floor(Convert.ToDouble(min / 1440)).ToString() + " dni";
			}

            return wynik;
        }
	}
}