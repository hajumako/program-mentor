﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SfCms_model
{
    public class class_validation
    {
        //variables

        //other

        //variables (class)

        //constructor
		public class_validation()
		{

		}

        //properties
        #region "properties"
        #endregion

        //properties other
        #region "properties other"
        #endregion

        //get_data
        #region "get_data"
        public void get_data()
        {
            
        }
        #endregion

        public bool czy_int(Object parametr)
		{
			bool wynik = true;

			try
			{
				int _wynik;
				_wynik = int.Parse(parametr.ToString());
			}
			catch
			{
				wynik = false;
			}

			return wynik;
		}

		public bool czy_page(Object parametr)
		{
			bool wynik = true;

			try
			{
				int _wynik;

				if(parametr != null)
				{
					_wynik = int.Parse(parametr.ToString());
				}
			}
			catch
			{
				wynik = false;
			}

			return wynik;
		}

		public bool czy_id(Object parametr)
		{
			bool wynik = true;

			try
			{
				int _wynik;

				if (parametr != null)
				{
					_wynik = int.Parse(parametr.ToString());
				}
			}
			catch
			{
				wynik = false;
			}

			return wynik;
		}

		public bool czy_string_not_empty(Object parametr)
		{
			bool wynik = true;

			try
			{
				string _wynik;

				if (parametr != null)
				{
					_wynik = parametr.ToString();

					if (_wynik == string.Empty)
					{
						wynik = false;
					}
				}
			}
			catch
			{
				wynik = false;
			}

			return wynik;
		}

		public bool czy_email(Object parametr)
		{
			bool wynik = true;

			try
			{
				var _wynik = new System.Net.Mail.MailAddress(parametr.ToString());
			}
			catch
			{
				wynik = false;
			}

			return wynik;
		}
    }
}
