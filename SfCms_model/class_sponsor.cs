﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//
using System.Data;
using System.Data.SqlClient;

namespace SfCms_model
{
	public class class_sponsor
	{
		//variables

		//other
		public enum search_type
		{
			all = 0,    //all
			id = 1,     //id
			active = 2, //active
            count = 3,  //count
            current = 4, //current
            order_by_sequence = 5, //all by sequence
            search_by_seqence = 6  //by sequence number
		}

		//variables (class)
		private class_sql sql = new class_sql();

		//constructor
		public class_sponsor(int id)
		{
			this.id = id;

			if (this.id > 0)
			{
                this.get_data();
			}
		}
		public class_sponsor()
		{

		}

		//properties
		#region "properties"
		public int id { get; set; }
		public bool active { get; set; }
		public int sequence { get; set; }
		public string name { get; set; }
		public string logo_link { get; set; }
		public string color1 { get; set; }
		public string color2 { get; set; }
		public decimal amount { get; set; }
		public decimal rate { get; set; }
		#endregion

		//properties other
        #region "properties other"
        public List<class_sponsor> list { get; set; }
        public List<class_sponsor_extended> list_ext { get; set; }
		#endregion

		//get_data
		#region "get_data"
		public void get_data()
		{
			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_sponsor", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(class_sponsor.search_type.id);
					this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = this.id.ToString();
					using (this.sql.reader = this.sql.result.ExecuteReader())
					{
						while (this.sql.reader.Read())
						{
							this.id = (int)(this.sql.reader["id"]);
							this.active = (bool)(this.sql.reader["active"]);
							this.sequence = (int)(this.sql.reader["sequence"]);
							this.name = (string)(this.sql.reader["name"]);
							this.logo_link = (string)(this.sql.reader["logo_link"]);
							this.color1 = (string)(this.sql.reader["color1"]);
							this.color2 = (string)(this.sql.reader["color2"]);
                            this.amount = (decimal)(this.sql.reader["amount"]);
                            this.rate = (decimal)(this.sql.reader["rate"]);
						}
					}
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}
		}
		#endregion

		//insert
		#region "insert"
		public void insert()
		{
			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_sponsor_insert", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@active", SqlDbType.Bit).Value = this.active;
					this.sql.result.Parameters.Add("@sequence", SqlDbType.Int).Value = this.sequence;
					this.sql.result.Parameters.Add("@name", SqlDbType.VarChar).Value = this.name;
					this.sql.result.Parameters.Add("@logo_link", SqlDbType.VarChar).Value = this.logo_link;
					this.sql.result.Parameters.Add("@color1", SqlDbType.VarChar).Value = this.color1;
					this.sql.result.Parameters.Add("@color2", SqlDbType.VarChar).Value = this.color2;
					this.sql.result.Parameters.Add("@amount", SqlDbType.Decimal).Value = this.amount;
					this.sql.result.Parameters.Add("@rate", SqlDbType.Decimal).Value = this.rate;
					using (this.sql.reader = this.sql.result.ExecuteReader())
					{
						while (this.sql.reader.Read())
						{
							this.id = (int)(this.sql.reader["id"]);
						}
					}
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}
		}
		#endregion

		//update
		#region "update"
		public void update()
		{
			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_sponsor_update", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@id", SqlDbType.Int).Value = this.id;
					this.sql.result.Parameters.Add("@active", SqlDbType.Bit).Value = this.active;
					this.sql.result.Parameters.Add("@sequence", SqlDbType.Int).Value = this.sequence;
					this.sql.result.Parameters.Add("@name", SqlDbType.VarChar).Value = this.name;
					this.sql.result.Parameters.Add("@logo_link", SqlDbType.VarChar).Value = this.logo_link;
					this.sql.result.Parameters.Add("@color1", SqlDbType.VarChar).Value = this.color1;
					this.sql.result.Parameters.Add("@color2", SqlDbType.VarChar).Value = this.color2;
					this.sql.result.Parameters.Add("@amount", SqlDbType.Decimal).Value = this.amount;
					this.sql.result.Parameters.Add("@rate", SqlDbType.Decimal).Value = this.rate;
					this.sql.result.ExecuteNonQuery();
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}
		}
		#endregion

		//delete
		#region "delete"
		public void delete()
		{
			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_sponsor_delete", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@id", SqlDbType.Int).Value = this.id;
					this.sql.result.ExecuteNonQuery();
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}
		}
		#endregion

		//count
		#region "count"
		public int count()
		{
			int wynik = 0;

			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_sponsor", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(class_sponsor.search_type.count);
					this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = string.Empty;
					using (this.sql.reader = this.sql.result.ExecuteReader())
					{
						while (this.sql.reader.Read())
						{
							wynik = (int)(this.sql.reader["count"]);
						}
					}
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}

			return wynik;
		}
		#endregion

		public void generate_list(search_type search_type, string search_string)
		{
			this.list = new List<class_sponsor>();

			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_sponsor", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(search_type);
					this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = search_string;
					using (this.sql.reader = this.sql.result.ExecuteReader())
					{
						while (this.sql.reader.Read())
						{
							class_sponsor sponsor = new class_sponsor
							{
								id = (int)(this.sql.reader["id"]),
								active = (bool)(this.sql.reader["active"]),
								sequence = (int)(this.sql.reader["sequence"]),
								name = (string)(this.sql.reader["name"]),
								logo_link = (string)(this.sql.reader["logo_link"]),
								color1 = (string)(this.sql.reader["color1"]),
								color2 = (string)(this.sql.reader["color2"]),
								amount = (decimal)(this.sql.reader["amount"]),
								rate = (decimal)(this.sql.reader["rate"])
							};

							//list
							this.list.Add(sponsor);
						}
					}
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}
		}
	
        public void generate_list_with_additional_data(search_type search_type, string search_string)
		{
			this.list_ext = new List<class_sponsor_extended>();

            class_sponsor current_sponsor = new class_sponsor();
            current_sponsor.generate_list(search_type.current, String.Empty);
            int current_sponsor_id = current_sponsor.list[0].id;

			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_sponsor", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(search_type);
					this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = search_string;
					using (this.sql.reader = this.sql.result.ExecuteReader())
					{
						while (this.sql.reader.Read())
						{
                            class_activity activity = new class_activity();       
                            decimal total = activity.total_amount(this.sql.reader["id"].ToString());
							class_sponsor_extended sponsor = new class_sponsor_extended
							{
								id = (int)(this.sql.reader["id"]),
								active = (bool)(this.sql.reader["active"]),
								sequence = (int)(this.sql.reader["sequence"]),
								name = (string)(this.sql.reader["name"]),
								logo_link = (string)(this.sql.reader["logo_link"]),
								color1 = (string)(this.sql.reader["color1"]),
								color2 = (string)(this.sql.reader["color2"]),
								amount = (decimal)(this.sql.reader["amount"]),
								rate = (decimal)(this.sql.reader["rate"]),
                                current_sponsor = (int)(this.sql.reader["id"]) == current_sponsor_id,
                                gathered_amount = total
							};

							//list
							this.list_ext.Add(sponsor);
						}
					}
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}
		}
    }

    public class class_sponsor_extended : class_sponsor
    {
        public bool current_sponsor { get; set; }
        public decimal gathered_amount { get; set; }

        private class_sql sql = new class_sql();

        public class_sponsor_extended()
		{

		}

		public class_sponsor_extended(int id)
		{
			this.id = id;

			if (this.id > 0)
			{
                this.get_data_extended();
			}
		}

        //get_data_extended
        #region "get_data_extended"
        public void get_data_extended()
        {
            class_sponsor current_sponsor = new class_sponsor();
            current_sponsor.generate_list(search_type.current, String.Empty);
            int current_sponsor_id = current_sponsor.list[0].id;

            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_sponsor", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(class_sponsor.search_type.id);
                    this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = this.id.ToString();
                    using (this.sql.reader = this.sql.result.ExecuteReader())
                    {
                        while (this.sql.reader.Read())
                        {
                            class_activity activity = new class_activity();

                            this.id = (int)(this.sql.reader["id"]);
                            this.active = (bool)(this.sql.reader["active"]);
                            this.sequence = (int)(this.sql.reader["sequence"]);
                            this.name = (string)(this.sql.reader["name"]);
                            this.logo_link = (string)(this.sql.reader["logo_link"]);
                            this.color1 = (string)(this.sql.reader["color1"]);
                            this.color2 = (string)(this.sql.reader["color2"]);
                            this.amount = (decimal)(this.sql.reader["amount"]);
                            this.rate = (decimal)(this.sql.reader["rate"]);
                            this.current_sponsor = (int)(this.sql.reader["id"]) == current_sponsor_id;
                            this.gathered_amount = activity.total_amount(this.sql.reader["id"].ToString());
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
        }
        #endregion
    }

    public class class_sponsor_ws_return
    {
        public int id { get; set; }
    }
}