﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Text;
using Microsoft.VisualBasic;
using System.IO;
using System.Net;
using Newtonsoft.Json;

namespace SfCms_model
{
	public class class_paylane_ws
	{
		//variables

		//other

		//variables (class)
        private class_sql sql = new class_sql();

		//constructor
		public class_paylane_ws()
		{

		}

        //properties
        #region "properties"
        public string merchant_id { get; set; }
        public string description { get; set; }
        public string transaction_description { get; set; }
        public decimal amount { get; set; }
        public bool czy_anonymous { get; set; }
        public string currency { get; set; }
        public string transaction_type { get; set; }
        public string back_url { get; set; }
        public string hash { get; set; }
        public string language { get; set; }
        public string customer_name { get; set; }
        public string customer_email { get; set; }
        public string customer_country { get; set; }
        #endregion

        //properties other
        #region "properties other"
        public List<class_transaction_data> content { get; set; }
        public int content_size { get; set; }
        public string communication_id { get; set; }
        public string token { get; set; }
        #endregion

        //get_data
        #region "get_data"
        public void get_data()
        {

        }
        #endregion

        //insert
        #region "insert"
        public void insert()
        {

        }
        #endregion

		//update
		#region "update"
		public bool update(class_transaction_data transaction_data)
		{           
            class_paylane cp = new class_paylane(int.Parse(transaction_data.text));
            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_paylane_update", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;

                    this.sql.result.Parameters.Add("@id", SqlDbType.Int).Value = transaction_data.text;
                    this.sql.result.Parameters.Add("@date_response", SqlDbType.DateTime).Value = DateTime.Now;
                    this.sql.result.Parameters.Add("@response_status", SqlDbType.VarChar).Value = "CLEARED";
                    this.sql.result.Parameters.Add("@response_amount", SqlDbType.Decimal).Value = transaction_data.amount;
                    this.sql.result.Parameters.Add("@response_currency", SqlDbType.VarChar).Value = transaction_data.currency;
                    this.sql.result.Parameters.Add("@response_id_sale", SqlDbType.Int).Value = transaction_data.id_sale;
                    this.sql.result.Parameters.Add("@response_description", SqlDbType.VarChar).Value = transaction_data.text;

                    //this.sql.result.Parameters.Add("@id", SqlDbType.Int).Value = cp.id;
                    this.sql.result.Parameters.Add("@project_id", SqlDbType.Int).Value = cp.project_id;
                    this.sql.result.Parameters.Add("@user_id", SqlDbType.Int).Value = cp.user_id;
                    this.sql.result.Parameters.Add("@date_request", SqlDbType.DateTime).Value = cp.date_request;
                    //this.sql.result.Parameters.Add("@date_response", SqlDbType.DateTime).Value = cp.date_response;
                    this.sql.result.Parameters.Add("@czy_anonymous", SqlDbType.Bit).Value = cp.czy_anonymous;
                    //---------------------------------
                    this.sql.result.Parameters.Add("@request_merchant_id", SqlDbType.VarChar).Value = cp.request_merchant_id;
                    this.sql.result.Parameters.Add("@request_description", SqlDbType.VarChar).Value = cp.request_description;
                    this.sql.result.Parameters.Add("@request_transaction_description", SqlDbType.VarChar).Value = cp.request_transaction_description;
                    this.sql.result.Parameters.Add("@request_amount", SqlDbType.Decimal).Value = cp.request_amount;
                    this.sql.result.Parameters.Add("@request_currency", SqlDbType.VarChar).Value = cp.request_currency;
                    this.sql.result.Parameters.Add("@request_transaction_type", SqlDbType.Char).Value = cp.request_transaction_type;
                    this.sql.result.Parameters.Add("@request_back_url", SqlDbType.VarChar).Value = cp.request_back_url;
                    this.sql.result.Parameters.Add("@request_hash", SqlDbType.VarChar).Value = cp.request_hash;
                    this.sql.result.Parameters.Add("@request_language", SqlDbType.VarChar).Value = cp.request_language;
                    this.sql.result.Parameters.Add("@request_customer_name", SqlDbType.VarChar).Value = cp.request_customer_name;
                    this.sql.result.Parameters.Add("@request_customer_email", SqlDbType.VarChar).Value = cp.request_customer_email;
                    this.sql.result.Parameters.Add("@request_customer_address", SqlDbType.VarChar).Value = cp.request_customer_address;
                    this.sql.result.Parameters.Add("@request_customer_zip", SqlDbType.VarChar).Value = cp.request_customer_zip;
                    this.sql.result.Parameters.Add("@request_customer_city", SqlDbType.VarChar).Value = cp.request_customer_city;
                    this.sql.result.Parameters.Add("@request_customer_state", SqlDbType.VarChar).Value = "ws";
                    this.sql.result.Parameters.Add("@request_customer_country", SqlDbType.VarChar).Value = cp.request_customer_country;
                    //---------------------------------
                    //this.sql.result.Parameters.Add("@response_status", SqlDbType.VarChar).Value = cp.response_status;
                    //this.sql.result.Parameters.Add("@response_description", SqlDbType.VarChar).Value = cp.response_description;
                    //this.sql.result.Parameters.Add("@response_amount", SqlDbType.Decimal).Value = cp.response_amount;
                    //this.sql.result.Parameters.Add("@response_currency", SqlDbType.VarChar).Value = cp.response_currency;
                    this.sql.result.Parameters.Add("@response_hash", SqlDbType.VarChar).Value = cp.response_hash;
                    this.sql.result.Parameters.Add("@response_id_authorization", SqlDbType.Int).Value = cp.response_id_authorization;
                    //this.sql.result.Parameters.Add("@response_id_sale", SqlDbType.Int).Value = cp.response_id_sale;
                    this.sql.result.Parameters.Add("@response_id_error", SqlDbType.Int).Value = cp.response_id_error;
                    this.sql.result.Parameters.Add("@response_error_code", SqlDbType.Int).Value = cp.response_error_code;
                    this.sql.result.Parameters.Add("@response_error_text", SqlDbType.VarChar).Value = cp.response_error_text;
                    this.sql.result.Parameters.Add("@response_fraud_score", SqlDbType.Decimal).Value = cp.response_fraud_score;
                    this.sql.result.Parameters.Add("@response_avs_result", SqlDbType.VarChar).Value = cp.response_avs_result;

                    this.sql.result.ExecuteNonQuery();
                    return true;
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                    return false;
                }
            }
		}
		#endregion

        //delete
        #region "delete"
        public void delete()
        {

        }
        #endregion
	}

    public class class_transaction_data
    {
        public string type { get; set; }
        public int id_sale { get; set; }
        public string date { get; set; }
        public decimal amount { get; set; }
        public string currency { get; set; }
        public string text { get; set; }
    }
}