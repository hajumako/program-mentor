﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;

namespace SfCms_model
{
    public class class_user_app
    {
        //variables

        //other
        public enum search_type
        {
            all = 0,         //all
            id = 1,          //id
            active = 2,      //active
            count = 3,       //count
            email = 4,       //email
            facebook_id = 5, //facebook_id
        }

        //variables (class)
        private class_sql sql = new class_sql();

        //constructor
        public class_user_app(int id)
        {
            this.id = id;

            if (this.id > 0)
            {
                this.get_data();
            }
        }
        public class_user_app()
        {

        }

        //properties
        #region "properties"
        public int id { get; set; }
        public bool active { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string facebook_id { get; set; }
        public int user_app_type { get; set; } //1 - (konto email). 2 - (konto facebook) 
        #endregion

        //properties other
        #region "properties other"
        public List<class_user_app> list { get; set; }
        public bool insert_email_exists { get; set; }
        public bool insert_facebook_id_exists { get; set; }
        #endregion

        //get_data
        #region "get_data"
        public void get_data()
        {
            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_user_app", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(class_user.search_type.id);
                    this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = this.id.ToString();
                    using (this.sql.reader = this.sql.result.ExecuteReader())
                    {
                        while (this.sql.reader.Read())
                        {
                            this.id = (int)(this.sql.reader["id"]);
                            this.active = (bool)(this.sql.reader["active"]);
                            this.name = (string)(this.sql.reader["name"]);
                            this.surname = (string)(this.sql.reader["surname"]);
                            this.email = (string)(this.sql.reader["email"]);
                            this.password = (string)(this.sql.reader["password"]);
                            this.facebook_id = (string)(this.sql.reader["facebook_id"]);
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
        }
        #endregion

        //insert
        #region "insert"
        public void insert()
        {
            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();

                    //transaction - begin
                    this.sql.transaction = this.sql.connection.BeginTransaction();

                    this.insert_email_exists = true;
                    this.insert_facebook_id_exists = true;

                    if (this.user_app_type == 1)
                    {
                        this.sql.result = new SqlCommand("sfcms_user_app_exists", this.sql.connection, this.sql.transaction);
                        this.sql.result.CommandType = CommandType.StoredProcedure;
                        this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(search_type.email);
                        this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = this.email;
                        using (this.sql.reader = this.sql.result.ExecuteReader())
                        {
                            while (this.sql.reader.Read())
                            {
                                this.insert_email_exists = (bool)(this.sql.reader["wynik"]);
                            }
                        }
                    }
                    else if (this.user_app_type == 2)
                    {
                        this.sql.result = new SqlCommand("sfcms_user_app_exists", this.sql.connection, this.sql.transaction);
                        this.sql.result.CommandType = CommandType.StoredProcedure;
                        this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(search_type.facebook_id);
                        this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = this.facebook_id;
                        using (this.sql.reader = this.sql.result.ExecuteReader())
                        {
                            while (this.sql.reader.Read())
                            {
                                this.insert_facebook_id_exists = (bool)(this.sql.reader["wynik"]);
                            }
                        }
                    }

                    if ((this.user_app_type == 1 && !this.insert_email_exists) || (this.user_app_type == 2 && !this.insert_facebook_id_exists))
                    {
                        this.sql.result = new SqlCommand("sfcms_user_app_insert", this.sql.connection, this.sql.transaction);
                        this.sql.result.CommandType = CommandType.StoredProcedure;
                        this.sql.result.Parameters.Add("@active", SqlDbType.Bit).Value = this.active;
                        this.sql.result.Parameters.Add("@name", SqlDbType.VarChar).Value = this.name;
                        this.sql.result.Parameters.Add("@surname", SqlDbType.VarChar).Value = this.surname;
                        this.sql.result.Parameters.Add("@email", SqlDbType.VarChar).Value = this.email;
                        this.sql.result.Parameters.Add("@password", SqlDbType.VarChar).Value = this.password;
                        this.sql.result.Parameters.Add("@facebook_id", SqlDbType.VarChar).Value = this.facebook_id;
                        this.sql.result.Parameters.Add("@user_app_type", SqlDbType.VarChar).Value = this.user_app_type;
                        using (this.sql.reader = this.sql.result.ExecuteReader())
                        {
                            while (this.sql.reader.Read())
                            {
                                this.id = (int)(this.sql.reader["id"]);
                            }
                        }
                    }
                   
                    //transaction - commit
                    this.sql.transaction.Commit();
                }
                catch (Exception ex)
                {
                    this.sql.transaction.Rollback();

                    this.sql.ex = ex.Message;
                }
            }
        }
        #endregion

        //update
        #region "update"
        public void update()
        {
            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_user_app_update", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@id", SqlDbType.Int).Value = this.id;
                    this.sql.result.Parameters.Add("@active", SqlDbType.Bit).Value = this.active;
                    this.sql.result.Parameters.Add("@name", SqlDbType.VarChar).Value = this.name;
                    this.sql.result.Parameters.Add("@surname", SqlDbType.VarChar).Value = this.surname;
                    this.sql.result.Parameters.Add("@email", SqlDbType.VarChar).Value = this.email;
                    this.sql.result.Parameters.Add("@password", SqlDbType.VarChar).Value = this.password;
                    this.sql.result.Parameters.Add("@facebook_id", SqlDbType.VarChar).Value = this.facebook_id;
                    this.sql.result.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
        }
        #endregion

        //delete
        #region "delete"
        public void delete()
        {
            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_user_app_delete", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@id", SqlDbType.Int).Value = this.id;
                    this.sql.result.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
        }
        #endregion

        //count
        #region "count"
        public int count()
        {
            int wynik = 0;

            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_user_app", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(class_user.search_type.count);
                    this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = string.Empty;
                    using (this.sql.reader = this.sql.result.ExecuteReader())
                    {
                        while (this.sql.reader.Read())
                        {
                            wynik = (int)(this.sql.reader["count"]);
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }

            return wynik;
        }
        #endregion

        public void generate_list(search_type search_type, string search_string)
        {
            this.list = new List<class_user_app>();

            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_user_app", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(search_type);
                    this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = search_string;
                    using (this.sql.reader = this.sql.result.ExecuteReader())
                    {
                        while (this.sql.reader.Read())
                        {
                            class_user_app user_app = new class_user_app
                            {
                                id = (int)(this.sql.reader["id"]),
                                active = (bool)(this.sql.reader["active"]),
                                name = (string)(this.sql.reader["name"]),
                                surname = (string)(this.sql.reader["surname"]),
                                email = (string)(this.sql.reader["email"]),
                                password = (string)(this.sql.reader["password"]),
                                facebook_id = (string)(this.sql.reader["facebook_id"])
                            };

                            //list
                            this.list.Add(user_app);
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
        }

        public bool log_in(string email, string facebook_id, string password, int user_app_type)
        {
            bool wynik = false;

            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_user_app_log_in", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@email", SqlDbType.VarChar).Value = email;
                    this.sql.result.Parameters.Add("@password", SqlDbType.VarChar).Value = password;
                    this.sql.result.Parameters.Add("@facebook_id", SqlDbType.VarChar).Value = facebook_id;
                    this.sql.result.Parameters.Add("@user_app_type", SqlDbType.Int).Value = user_app_type;
                    using (this.sql.reader = this.sql.result.ExecuteReader())
                    {
                        while (this.sql.reader.Read())
                        {
                            wynik = (bool)(this.sql.reader["wynik"]);
                            this.id = (int)(this.sql.reader["user_app_id"]);
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }

            return wynik;
        }

        public bool exists(search_type search_type, string search_string)
        {
            bool wynik = false;

            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_user_app_exists", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(search_type);
                    this.sql.result.Parameters.Add("@search_string", SqlDbType.VarChar).Value = search_string;
                    using (this.sql.reader = this.sql.result.ExecuteReader())
                    {
                        while (this.sql.reader.Read())
                        {
                            wynik = (bool)(this.sql.reader["wynik"]);
                            this.id = (int)(this.sql.reader["user_app_id"]);
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }

            return wynik;
        }
    }

    public class class_user_app_ws_return
    {
        public int id { get; set; }
        public bool authentication { get; set; } 
    }
}
