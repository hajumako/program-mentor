﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//
using System.Data;
using System.Data.SqlClient;

namespace SfCms_model
{
	public class class_s
	{
		//variables

		//other
		public enum dictionary
		{
			all = 0,			// - wszystkie słowniki
			s_post = 1,		    // - tylko s_post
			s_brick = 2,        // - tylko s_brick
            s_news = 3,         // - tylko s_news
            s_sex = 4           // - tylko s_sex
		}

		//variables (class)
		private class_sql sql = new class_sql();

		//constructor
		public class_s(dictionary dictionary)
		{
			this.get_data(dictionary);
		}

		//properties
		#region "properties"
		#endregion

		//properties other
		#region "properties other"
		public List<class_s_post> s_post { get; set; }
		public List<class_s_brick> s_brick { get; set; }
        public List<class_s_news> s_news { get; set; }
        public List<class_s_sex> s_sex { get; set; }
		#endregion

		//get_data
		#region "get_data"
		private void get_data(dictionary dictionary)
		{
			if (dictionary == class_s.dictionary.s_post)
			{
				this.repository_s_post();
			}
			else if (dictionary == class_s.dictionary.s_brick)
			{
				this.repository_s_brick();
			}
            else if (dictionary == class_s.dictionary.s_news)
            {
                this.repository_s_news();
            }
            else if (dictionary == class_s.dictionary.s_sex)
            {
                this.repository_s_sex();
            }
			else if (dictionary == class_s.dictionary.all)
			{
				this.repository_s_post();
				this.repository_s_brick();
                this.repository_s_news();
                this.repository_s_sex();
			}
		}
		#endregion

		private void repository_s_post()
		{
			this.s_post = new List<class_s_post>();

			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_s_post", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(class_s_post.search_type.all);
					this.sql.result.Parameters.Add("@search", SqlDbType.VarChar).Value = string.Empty;
					using (this.sql.reader = this.sql.result.ExecuteReader())
					{
						while (this.sql.reader.Read())
						{
							class_s_post s_post = new class_s_post
							{
								id = (int)(this.sql.reader["id"]),
								value = (int)(this.sql.reader["value"]),
								active = (bool)(this.sql.reader["active"]),
								sequence = (int)(this.sql.reader["sequence"]),
								name = (string)(this.sql.reader["name"]),
								description = (string)(this.sql.reader["description"])
							};

							this.s_post.Add(s_post);
						}
					}
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}
		}

		private void repository_s_brick()
		{
			this.s_brick = new List<class_s_brick>();

			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_s_brick", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(class_s_brick.search_type.all);
					this.sql.result.Parameters.Add("@search", SqlDbType.VarChar).Value = string.Empty;
					using (this.sql.reader = this.sql.result.ExecuteReader())
					{
						while (this.sql.reader.Read())
						{
							class_s_brick s_brick = new class_s_brick
							{
								id = (int)(this.sql.reader["id"]),
								value = (int)(this.sql.reader["value"]),
								active = (bool)(this.sql.reader["active"]),
								sequence = (int)(this.sql.reader["sequence"]),
								name = (string)(this.sql.reader["name"]),
								description = (string)(this.sql.reader["description"]),
								amount_from = (decimal)(this.sql.reader["amount_from"]),
								amount_to = (decimal)(this.sql.reader["amount_to"]),
								seed = (int)(this.sql.reader["seed"])
							};

							this.s_brick.Add(s_brick);
						}
					}
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}
		}

        private void repository_s_news()
        {
            this.s_news = new List<class_s_news>();

            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_s_news", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(class_s_news.search_type.all);
                    this.sql.result.Parameters.Add("@search", SqlDbType.VarChar).Value = string.Empty;
                    using (this.sql.reader = this.sql.result.ExecuteReader())
                    {
                        while (this.sql.reader.Read())
                        {
                            class_s_news s_news = new class_s_news
                            {
                                id = (int)(this.sql.reader["id"]),
                                value = (int)(this.sql.reader["value"]),
                                active = (bool)(this.sql.reader["active"]),
                                sequence = (int)(this.sql.reader["sequence"]),
                                name = (string)(this.sql.reader["name"]),
                                description = (string)(this.sql.reader["description"])
                            };

                            this.s_news.Add(s_news);
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
        }

        private void repository_s_sex()
        {
            this.s_sex = new List<class_s_sex>();

            using (this.sql.connection = new SqlConnection(this.sql.connection_string))
            {
                try
                {
                    this.sql.connection.Open();
                    this.sql.result = new SqlCommand("sfcms_s_sex", this.sql.connection);
                    this.sql.result.CommandType = CommandType.StoredProcedure;
                    this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(class_s_sex.search_type.all);
                    this.sql.result.Parameters.Add("@search", SqlDbType.VarChar).Value = string.Empty;
                    using (this.sql.reader = this.sql.result.ExecuteReader())
                    {
                        while (this.sql.reader.Read())
                        {
                            class_s_sex s_sex = new class_s_sex
                            {
                                id = (int)(this.sql.reader["id"]),
                                value = (int)(this.sql.reader["value"]),
                                active = (bool)(this.sql.reader["active"]),
                                sequence = (int)(this.sql.reader["sequence"]),
                                name = (string)(this.sql.reader["name"]),
                                description = (string)(this.sql.reader["description"])
                            };

                            this.s_sex.Add(s_sex);
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.sql.ex = ex.Message;
                }
            }
        }
	}
}