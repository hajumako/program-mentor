﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Text;

namespace SfCms_model
{
	public class class_md5
	{
		//variables

		//other
		public enum encoding
		{
			UTF8 = 0,	// - UTF8
			win1250 = 1 // - win1250
		}

		//variables (class)
		private class_sql sql = new class_sql();

		//constructor
		public class_md5(string password, encoding encoding)
		{
			this.password = password;
			this.encode = encoding;
			if (this.password != string.Empty)
			{
				this.get_data();
			}
		}

		//properties
		#region "properties"
		public string password { get; set; }
		public encoding encode { get; set; }
		public string password_md5hash { get; set; }
		#endregion

		//properties other
		#region "properties other"
		#endregion

		//get_data
		#region "get_data"
		public void get_data()
		{
			Encoding win1250 = Encoding.GetEncoding(1250);
			StringBuilder sb = new StringBuilder();
			MD5 md5 = System.Security.Cryptography.MD5.Create();
			byte[] inputBytes;
			byte[] hash;

			//UTF8
			if (this.encode == encoding.UTF8)
			{
				inputBytes = System.Text.Encoding.UTF8.GetBytes(this.password);
				hash = md5.ComputeHash(inputBytes);

				for (int i = 0; i < hash.Length; i++)
				{
					sb.Append(hash[i].ToString("X2"));
				}
			}
			//win1250
			else if (this.encode == encoding.win1250)
			{
				inputBytes = win1250.GetBytes(this.password);
				hash = md5.ComputeHash(inputBytes);

				for (int i = 0; i < hash.Length; i++)
				{
					sb.Append(hash[i].ToString("X2"));
				}
			}

			this.password_md5hash = sb.ToString().ToLower();
		}
		#endregion
	}
}