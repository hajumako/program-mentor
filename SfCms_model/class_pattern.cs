﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SfCms_model
{
	public class class_pattern
	{
		//variables

		//other

		//variables (class)

		//constructor
		public class_pattern()
		{

		}

		//properties
		#region "properties"
		#endregion

		//properties other
		#region "properties other"
        public class_activity activity { get; set; }
		public class_admin admin { get; set; }
		public class_beneficiary beneficiary { get; set; }
		public class_beneficiary_dictionary beneficiary_dictionary { get; set; }
		public class_beneficiary_file beneficiary_file { get; set; }
		public class_blog blog { get; set; }
        public class_blog_file blog_file { get; set; }
        public class_brick brick { get; set; }
		public class_dictionary dictionary { get; set; }
		public class_mentor mentor { get; set; }
        public class_mentor_dictionary mentor_dictionary { get; set; }
        public class_mentor_file mentor_file { get; set; }
		public class_menu menu { get; set; }
		public class_nav nav { get; set; }
		public class_news news { get; set; }
		public class_news_dictionary news_dictionary { get; set; }
        public class_news_file news_file { get; set; }
		public class_paylane paylane { get; set; }
        public class_project project { get; set; }
		public class_project_dictionary project_dictionary { get; set; }
        public class_s s { get; set; }
		public class_s_brick s_brick { get; set; }
        public class_s_news s_news { get; set; }
        public class_s_post s_post { get; set; }
        public class_s_sex s_sex { get; set; }
        public class_settings settings { get; set; }
        public class_sponsor sponsor { get; set; }
        public class_sponsor_extended sponsor_extended { get; set; }
		public class_tool tool { get; set; }
		public class_user user { get; set; }
		public class_validation validation { get; set; }

        public String str_osoby { get; set; }
        public String str_wsparcie { get; set; }

        public int start_seq { get; set; }
        public bool err { get; set; }
        public bool hide_nulls { get; set; }
		#endregion

		//get_data
		#region "get_data"
		private void get_data()
		{
		}
		#endregion
	}
}