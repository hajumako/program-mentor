﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//
using System.Data;
using System.Data.SqlClient;

namespace SfCms_model
{
	public class class_menu
	{
		//variables

		//other

		//variables (class)
		private class_sql sql = new class_sql();

		//constructor
		public class_menu()
		{

		}

		//properties
		#region "properties"
		public int id { get; set; }
		public bool active { get; set; }
		public int sequence { get; set; }
		public string name { get; set; }
		public string description { get; set; }	
		public int parent { get; set; }
		public bool child { get; set; }
		public string controller { get; set; }
		public string action { get; set; }
		#endregion

		//properties other
		#region "properties other"
		public List<class_menu> list { get; set; }
		public List<class_menu> main_menu { get; set; }
		public List<class_menu> sub_menu { get; set; }
		#endregion

		//get_data
		#region "get_data"
		public void get_data(int id)
		{
			var query_menu = from obj in this.list where obj.id == id select obj;

			//clear_data
			this.clear_data();

			foreach (class_menu menu in query_menu)
			{
				this.id = menu.id;
				this.active = menu.active;
				this.sequence = menu.sequence;
				this.name = menu.name;
				this.description = menu.description;
				this.parent = menu.parent;
				this.child = menu.child;
				this.controller = menu.controller;
				this.action = menu.action;
			}
		}
		#endregion

		private void clear_data()
		{
			this.id = 0;
			this.active = false;
			this.sequence = 0;
			this.name = string.Empty;
			this.description = string.Empty;
			this.parent = 0;
			this.child = false;
			this.controller = string.Empty;
			this.action = string.Empty;
		}

		public void generate_list()
		{
			this.list = new List<class_menu>();
			this.main_menu = new List<class_menu>();

			using (this.sql.connection = new SqlConnection(this.sql.connection_string))
			{
				try
				{
					this.sql.connection.Open();
					this.sql.result = new SqlCommand("sfcms_menu", this.sql.connection);
					this.sql.result.CommandType = CommandType.StoredProcedure;
					this.sql.result.Parameters.Add("@search_type", SqlDbType.Int).Value = (int)(class_settings.search_type.all);
					using (this.sql.reader = this.sql.result.ExecuteReader())
					{
						while (this.sql.reader.Read())
						{
							class_menu menu = new class_menu
							{
								id = (int)(this.sql.reader["id"]),
								active = (bool)(this.sql.reader["active"]),
								sequence = (int)(this.sql.reader["sequence"]),
								name = (string)(this.sql.reader["name"]),
								description = (string)(this.sql.reader["description"]),
								parent = (int)(this.sql.reader["parent"]),
								child = (bool)(this.sql.reader["child"]),
								controller = (string)(this.sql.reader["controller"]),
								action = (string)(this.sql.reader["action"])
							};

							//list
							this.list.Add(menu);

							//main_menu
							if(menu.parent == 0)
							{
								this.main_menu.Add(menu);
							}
						}
					}
				}
				catch (Exception ex)
				{
					this.sql.ex = ex.Message;
				}
			}
		}

		public void get_sub_menu(int parent)
		{
			this.sub_menu = new List<class_menu>();

			var query_menu = from obj in this.list where obj.parent == parent select obj;

			foreach (class_menu menu in query_menu)
			{
				class_menu _menu = new class_menu
				{
					id = menu.id,
					active = menu.active,
					sequence = menu.sequence,
					name = menu.name,
					description = menu.description,
					parent = menu.parent,
					child = menu.child,
					controller = menu.controller,
					action = menu.action
				};

				this.sub_menu.Add(_menu);
			}
		}

		public int news_count()
		{
			int wynik = 0;

			class_news news = new class_news();
			wynik = news.count();

			return wynik;
		}

		public int project_count()
		{
			int wynik = 0;

			class_project project = new class_project();
			wynik = project.count();

			return wynik;
		}

		public int beneficiary_count()
		{
			int wynik = 0;

			class_beneficiary beneficiary = new class_beneficiary();
			wynik = beneficiary.count();

			return wynik;
		}

        public int mentor_count()
        {
            int wynik = 0;

            class_mentor mentor = new class_mentor();
            wynik = mentor.count();

            return wynik;
        }
	}
}