﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//
using SfCms_model;
using Newtonsoft.Json;
using System.Json;

namespace SfCms_ws.Controllers
{
    public class WsController : Controller
    {
		[HttpGet]
		public JsonResult Index()
		{
			class_sponsor sponsor = new class_sponsor(2);

			HttpContext.Response.AppendHeader("Access-Control-Allow-Origin", "*");
			return Json(sponsor, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public JsonResult Sponsor(class_sponsor sponsor)
		{
			HttpContext.Response.AppendHeader("Access-Control-Allow-Origin", "*");

            int sponsor_current = 0;
            class_sponsor sponsor_ = new class_sponsor();
            sponsor_.generate_list(class_sponsor.search_type.current, string.Empty);
            if (sponsor_.list.Count != 0)
            {
                sponsor_current = sponsor_.list[0].id;
            }

			if (sponsor.id == sponsor_current)
			{
                class_sponsor_ws_return sponsor_ws_return = new class_sponsor_ws_return();

                sponsor_ws_return.id = sponsor_current;

                return Json(sponsor_ws_return, JsonRequestBehavior.DenyGet);
			}
			else if (sponsor_.list.Count != 0)
			{
				return Json(sponsor_.list[0], JsonRequestBehavior.DenyGet);
			}
            else
            {
                return Json(sponsor_, JsonRequestBehavior.DenyGet);
            }
		}

        [HttpPost]
        public JsonResult Register(class_user_app user_app)
        {
            HttpContext.Response.AppendHeader("Access-Control-Allow-Origin", "*");

            if (user_app.facebook_id == null)
            {
                user_app.facebook_id = string.Empty;
            }

            user_app.active = true;
            user_app.insert();

            class_user_app_ws_return user_app_ws_return = new class_user_app_ws_return();
            user_app_ws_return.id = user_app.id;

            return Json(user_app_ws_return, JsonRequestBehavior.DenyGet);
        }

        [HttpPost]
        public JsonResult LogIn(class_user_app user_app)
        {
            HttpContext.Response.AppendHeader("Access-Control-Allow-Origin", "*");

            if (user_app.facebook_id == null)
            {
                user_app.facebook_id = string.Empty;
            }

            class_user_app_ws_return user_app_ws_return = new class_user_app_ws_return();
            
            user_app_ws_return.authentication = user_app.log_in(user_app.email, user_app.facebook_id, user_app.password, user_app.user_app_type);
            user_app_ws_return.id = user_app.id;

            return Json(user_app_ws_return, JsonRequestBehavior.DenyGet);
        }

        [HttpPost]
        public JsonResult Activity(class_activity activity)
        {
            HttpContext.Response.AppendHeader("Access-Control-Allow-Origin", "*");

            activity.generate_list(class_activity.search_type.user_app_id, activity.user_app_id.ToString());

            return Json(activity.list, JsonRequestBehavior.DenyGet);
        }

        [HttpPost]
        public JsonResult ActivityAdd(class_activity activity)
        {
            HttpContext.Response.AppendHeader("Access-Control-Allow-Origin", "*");

            activity.active = true;
            activity.insert();

            class_activity_ws_return activity_ws_return = new class_activity_ws_return();
            activity_ws_return.id = activity.id;

            return Json(activity_ws_return, JsonRequestBehavior.DenyGet);
        }
    }
}