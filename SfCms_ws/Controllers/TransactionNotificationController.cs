﻿using Newtonsoft.Json;
using SfCms_model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Globalization;

namespace SfCms_ws.Controllers
{
    public class TransactionNotificationController : Controller
    {
        private readonly String token = "mx3azGa8GP";

        public ActionResult Index()
        {
            HttpContext.Response.AppendHeader("Access-Control-Allow-Origin", "*");
            return Content("hello!");
        }

        //[BasicAuthenticationAttribute]
        public String UpdateStatus(class_paylane_ws notification_data)
        {
            saveRequestToFile(HttpContext.Request);

            HttpContext.Response.AppendHeader("Access-Control-Allow-Origin", "*");

            HttpContext.Request.InputStream.Position = 0;
            notification_data.content = parseQueryString(new StreamReader(HttpContext.Request.InputStream).ReadToEnd());

            try
            {
                if (notification_data.communication_id == null)
                    return "null communication id";

                if (notification_data.token != token)
                    return "wrong token";

                if (notification_data.content.Count > 0)
                {
                    foreach (class_transaction_data transaction_data in notification_data.content)
                    {
                        if (transaction_data.type == "S")
                            if (notification_data.update(transaction_data))
                            {
                                //user
                                class_paylane paylane = new class_paylane(int.Parse(transaction_data.text));
                                //project
                                class_project project = new class_project(paylane.project_id);
                                project.generate_list_beneficiary();
                                //user
                                class_user user = new class_user(paylane.user_id);
                                //email
                                class_email email = new class_email();

                                string msg_template = System.IO.File.ReadAllText(Server.MapPath("/assets/email_templates/thankyou_email_template.html"));

                                if (email.send(user.email, user.subject_thankyou, user.body_thankyou(msg_template, paylane, project)))
                                {
                                    //smth
                                }
                                else
                                {
                                    //smth
                                }
                            }
                            else
                            {
                                //return "update failed";
                                return null;
                            }
                    }
                }
                else
                {
                    return null;
                }

                return notification_data.communication_id;
            }
            catch (Exception ex)
            {
                //return ex.ToString();
                return null;
            }
        }

        private List<class_transaction_data> parseQueryString(string queryString)
        {
            string decodedString = WebUtility.UrlDecode(queryString);
            string content = decodedString.Split(new string[] { "&content_size" }, StringSplitOptions.None)[0];
            string[] content_arr = content.Split(new string[] { "content" }, StringSplitOptions.RemoveEmptyEntries);
            List<class_transaction_data> transaction_data_list = new List<class_transaction_data>();

            int i = 0;
            class_transaction_data transaction_data = new class_transaction_data();
            foreach (string data in content_arr)
            {
                string[] data_arr = data.Replace("[", "").Split(']');
                if (int.Parse(data_arr[0]) > i)
                {
                    transaction_data_list.Add(transaction_data);
                    i++;
                }
                else
                {
                    string value = data_arr[2].Trim(new char[] { '=', '&' });
                    if (data_arr[1].Equals("type"))
                        transaction_data.type = value;
                    else if (data_arr[1].Equals("id_sale"))
                        transaction_data.id_sale = int.Parse(value);
                    else if (data_arr[1].Equals("date"))
                        transaction_data.date = value;
                    else if (data_arr[1].Equals("amount"))
                        transaction_data.amount = Convert.ToDecimal(value, new CultureInfo("en-US"));
                    else if (data_arr[1].Equals("currency_code"))
                        transaction_data.currency = value;
                    else if (data_arr[1].Equals("text"))
                        transaction_data.text = value;
                }
            }
            transaction_data_list.Add(transaction_data);

            return transaction_data_list;
        }

        private void saveRequestToFile(HttpRequestBase request)
        {
            string writer = HttpRequestExtensions.ToRaw(request);
            String path = Server.MapPath("/log.txt");
            if (request.ContentLength > 0)
            {
                byte[] buffer = new byte[request.ContentLength];
                request.InputStream.Read(buffer, 0, request.ContentLength);
                writer += Encoding.ASCII.GetString(buffer);
                //writer += Encoding.ASCII.GetString(buffer).Replace(",", ",\n").Replace("{", "{\n").Replace("}", "\n}");
            }
            writer += "\n------------------------\n\r";
            System.IO.File.AppendAllText(path,  DateTime.Now + "\n" + writer);
        }

        public class BasicAuthenticationAttribute : ActionFilterAttribute
        {
            private static readonly string AuthorizationHeader = "Authorization";
            private static readonly string BasicHeader = "Basic ";
            private static readonly string Username = "mentor";
            private static readonly string Password = "JWEcQnnP4o";
            private static readonly char[] Separator = ":".ToCharArray();

            public override void OnActionExecuting(ActionExecutingContext filterContext)
            {
                try
                {
                    if (!Authenticated(filterContext.HttpContext.Request))
                        filterContext.Result = new HttpUnauthorizedResult();

                    base.OnActionExecuting(filterContext);

                    /* Test */
                    /*
                    JsonResult jr = new JsonResult();
                    jr.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
                    var authHeader = filterContext.HttpContext.Request.Headers[AuthorizationHeader];
                    jr.Data = "auth: " + authHeader;
                    if (String.IsNullOrEmpty(authHeader) == false && authHeader.StartsWith(BasicHeader, StringComparison.InvariantCultureIgnoreCase))
                    {
                        string[] credentials = Encoding.ASCII.GetString(Convert.FromBase64String(authHeader.Substring(BasicHeader.Length))).Split(Separator);
                        jr.Data += ";" + credentials[0] + ";" + credentials[1];
                    }
                    filterContext.Result = jr;
                    */
                }
                catch(Exception ex)
                {
                    /* Test */
                    /*
                    JsonResult jr = new JsonResult();
                    jr.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
                    jr.Data = ex.ToString();
                    filterContext.Result = jr;
                    */
                    filterContext.Result = new HttpUnauthorizedResult();
                }
            }

            private bool Authenticated(HttpRequestBase httpRequestBase)
            {
                bool authenticated = false;

                var authHeader = httpRequestBase.Headers[AuthorizationHeader];
                if (String.IsNullOrEmpty(authHeader) == false && authHeader.StartsWith(BasicHeader, StringComparison.InvariantCultureIgnoreCase))
                {
                    string[] credentials = Encoding.ASCII.GetString(Convert.FromBase64String(authHeader.Substring(BasicHeader.Length))).Split(Separator);

                    if (credentials.Length == 2 && credentials[0] == Username && credentials[1] == Password)
                    {
                        authenticated = true;
                    }
                }

                return authenticated;
            }
        }
    }

    public static class HttpRequestExtensions
    {
        /// <summary>
        /// Dump the raw http request to a string. 
        /// </summary>
        /// <param name="request">The <see cref="HttpRequest"/> that should be dumped.       </param>
        /// <returns>The raw HTTP request.</returns>
        public static string ToRaw(this HttpRequestBase request)
        {
            StringWriter writer = new StringWriter();

            WriteStartLine(request, writer);
            WriteHeaders(request, writer);
            WriteBody(request, writer);

            return writer.ToString();
        }

        private static void WriteStartLine(HttpRequestBase request, StringWriter writer)
        {
            const string SPACE = " ";

            writer.Write(request.HttpMethod);
            writer.Write(SPACE + request.Url);
            writer.WriteLine(SPACE + request.ServerVariables["SERVER_PROTOCOL"]);
        }

        private static void WriteHeaders(HttpRequestBase request, StringWriter writer)
        {
            foreach (string key in request.Headers.AllKeys)
            {
                writer.WriteLine(string.Format("{0}: {1}", key, request.Headers[key]));
            }

            writer.WriteLine();
        }

        private static void WriteBody(HttpRequestBase request, StringWriter writer)
        {
            StreamReader reader = new StreamReader(request.InputStream);

            try
            {
                string body = reader.ReadToEnd();
                writer.WriteLine(body);
            }
            finally
            {
                reader.BaseStream.Position = 0;
            }
        }
    }
}
